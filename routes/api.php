<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('bunyikan', [UserController::class, 'bunyikan']);
Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware'    => 'auth:sanctum'],function(){
    Route::get('profil', [UserController::class, 'profil']);
    Route::post('save_target', [UserController::class, 'save_target']);
    Route::post('save_actual', [UserController::class, 'save_actual']);
    Route::post('save_total_pemilih', [UserController::class, 'save_total_pemilih']);
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
