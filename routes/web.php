<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GiziController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\IndikatorGiziController;
use App\Http\Controllers\IndikatorSenitifController;
use App\Http\Controllers\Auth\LogoutController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cache-clear', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/optimize-clear', function() {
    $exitCode = Artisan::call('optimize:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Cache facade value cleared</h1>';
}); 

Route::group(['middleware' => 'auth'], function() {
    /**
    * Logout Route
    */
    Route::get('/logout-perform', [LogoutController::class, 'perform'])->name('logout.perform');
 });
Route::group(['prefix' => 'gzkecamatan/{id?}','middleware'    => 'auth'],function(){
    Route::get('/',[GiziController::class, 'index']);
    Route::get('/view',[GiziController::class, 'view_data']);
    Route::get('/getdata',[GiziController::class, 'get_data']);
    Route::get('/getdatarecord',[GiziController::class, 'get_tipe_record']);
    Route::get('/delete',[GiziController::class, 'delete']);
    Route::get('/switch_to',[GiziController::class, 'switch_to']);
    Route::get('/create',[GiziController::class, 'create']);
    Route::get('/modal',[GiziController::class, 'modal']);
    Route::post('/',[GiziController::class, 'store']);
    Route::post('/import',[GiziController::class, 'store_import']);
});
Route::group(['prefix' => 'indikator-gizi','middleware'    => 'auth'],function(){
    Route::get('/',[IndikatorGiziController::class, 'index']);
    Route::get('/view',[IndikatorGiziController::class, 'view_data']);
    Route::get('/getdata',[IndikatorGiziController::class, 'get_data']);
    Route::get('/getdatarecord',[IndikatorGiziController::class, 'get_tipe_record']);
    Route::get('/delete',[IndikatorGiziController::class, 'delete']);
    Route::get('/switch_to',[IndikatorGiziController::class, 'switch_to']);
    Route::get('/create',[IndikatorGiziController::class, 'create']);
    Route::get('/modal',[IndikatorGiziController::class, 'modal']);
    Route::post('/',[IndikatorGiziController::class, 'store']);
    Route::post('/import',[IndikatorGiziController::class, 'store_import']);
});
Route::group(['prefix' => 'indikator-sensitif','middleware'    => 'auth'],function(){
    Route::get('/',[IndikatorSenitifController::class, 'index']);
    Route::get('/view',[IndikatorSenitifController::class, 'view_data']);
    Route::get('/getdata',[IndikatorSenitifController::class, 'get_data']);
    Route::get('/getdatarecord',[IndikatorSenitifController::class, 'get_tipe_record']);
    Route::get('/delete',[IndikatorSenitifController::class, 'delete']);
    Route::get('/switch_to',[IndikatorSenitifController::class, 'switch_to']);
    Route::get('/create',[IndikatorSenitifController::class, 'create']);
    Route::get('/modal',[IndikatorSenitifController::class, 'modal']);
    Route::post('/',[IndikatorSenitifController::class, 'store']);
    Route::post('/import',[IndikatorSenitifController::class, 'store_import']);
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [App\Http\Controllers\ProfileController::class, 'index']);
Route::get('/getkecamatan', [App\Http\Controllers\ProfileController::class, 'get_kecamatan']);
Route::group(['prefix' => 'stunting/{id?}'],function(){
    Route::get('/',[ProfileController::class, 'index_detail']);
    Route::get('/getdata',[ProfileController::class, 'get_data_kelurahan']);
    Route::get('/getdataall',[ProfileController::class, 'get_data_kelurahan_all']);
});
Route::group(['prefix' => 'intervensi/{id?}'],function(){
    Route::get('/',[ProfileController::class, 'index_detail_intervensi']);
    Route::get('/getdata',[ProfileController::class, 'get_data_kelurahan_intervensi']);
    Route::get('/getdataall',[ProfileController::class, 'get_data_kelurahan_all_intervensi']);
});
Route::group(['prefix' => 'intervensi-sensitif/{id?}'],function(){
    Route::get('/',[ProfileController::class, 'index_detail_sensitif']);
    Route::get('/getdata',[ProfileController::class, 'get_data_kelurahan_sensitif']);
    Route::get('/getdataall',[ProfileController::class, 'get_data_kelurahan_all_sensitif']);
});