/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50733 (5.7.33)
 Source Host           : localhost:3306
 Source Schema         : db_dinkes

 Target Server Type    : MySQL
 Target Server Version : 50733 (5.7.33)
 File Encoding         : 65001

 Date: 21/11/2023 16:58:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for m_indikator
-- ----------------------------
DROP TABLE IF EXISTS `m_indikator`;
CREATE TABLE `m_indikator`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indikator` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tipe_id` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_indikator
-- ----------------------------
INSERT INTO `m_indikator` VALUES (1, 'BUMIL KEK', 'Persentase ibu hamil Kurang Energi Kronik (KEK) yang mendapatkan tambahan asupan gizi.', 2);
INSERT INTO `m_indikator` VALUES (2, 'BUMIL TTD', '	Persentase ibu hamil yang mengonsumsi Tablet Tambah Darah (TTD) minimal 90 tablet selama masa kehamilan.', 2);
INSERT INTO `m_indikator` VALUES (3, 'REMATRI TTD', 'Persentase remaja putri yang mengonsumsi Tablet Tambah Darah (TTD).', 2);
INSERT INTO `m_indikator` VALUES (4, 'ASI EKSLUSIF', 'Persentase bayi usia kurang dari 6 bulan mendapat Air Susu Ibu (ASI) eksklusif.', 2);
INSERT INTO `m_indikator` VALUES (5, 'MP-ASI', 'Persentase anak usia 6-23 bulan yang mendapat Makanan Pendamping Air Susu Ibu (MP-ASI).', 2);
INSERT INTO `m_indikator` VALUES (6, 'TATALAKSANA GIBUR', 'Persentase anak berusia di bawah lima tahun (balita) gizi buruk yang mendapat pelayanan tata laksana gizi buruk.', 2);
INSERT INTO `m_indikator` VALUES (7, 'BALITA DIPANTAU TUMBANG', '	Persentase anak berusia di bawah lima tahun (balita) yang dipantau pertumbuhan dan perkembangannya.', 2);
INSERT INTO `m_indikator` VALUES (8, 'GZ KURANG DAPAT PMT', '	Persentase anak berusia di bawah lima tahun (balita) gizi kurang yang mendapat tambahan asupan gizi.', 2);
INSERT INTO `m_indikator` VALUES (9, 'IMUNISASI BALITA', 'Persentase anak berusia di bawah lima tahun (balita) yang memperoleh imunisasi dasar lengkap.', 2);
INSERT INTO `m_indikator` VALUES (10, 'KB pasca persalinan', 'Persentase pelayanan Keluarga Berencana (KB) pasca persalinan.', 3);
INSERT INTO `m_indikator` VALUES (11, 'kehamilan tidak diinginkan', 'Persentase kehamilan yang tidak diinginkan (Rencana dan Tidak direncanakan)', 3);
INSERT INTO `m_indikator` VALUES (12, 'PUS memperoleh pemeriksaan', 'Cakupan calon Pasangan Usia Subur (PUS) yang memperoleh pemeriksaan kesehatan sebagai bagian dari pelayanan nikah', 3);
INSERT INTO `m_indikator` VALUES (13, 'PAMRT', 'Persentase rumah tangga yang mendapatkan akses air minum layak di kabupaten/kota lokasi prioritas', 3);
INSERT INTO `m_indikator` VALUES (14, 'Limbah Cair', 'Persentase rumah tangga yang mendapatkan akses sanitasi (air limbah domestik) layak di kabupaten/kota lokasi prioritas', 3);
INSERT INTO `m_indikator` VALUES (15, 'Cakupan BPI JKN', 'Cakupan penerima bantuan iuran (PBI) Jaminan Kesehatan Nasional', 3);
INSERT INTO `m_indikator` VALUES (16, 'keluarga berisiko Stunting', 'Cakupan keluarga berisiko Stunting yang memperoleh pendampingan', 3);
INSERT INTO `m_indikator` VALUES (17, 'bantuan tunai bersyarat', 'Jumlah keluarga miskin dan rentan yang memperoleh bantuan tunai bersyarat', 3);
INSERT INTO `m_indikator` VALUES (18, 'Jumlah Sasaran KAP', 'Persentase target sasaran yang memiliki pemahaman yang baik tentang Stunting di lokasi prioritas', 3);
INSERT INTO `m_indikator` VALUES (19, 'Bantuan Pangan', ' Jumlah keluarga miskin dan rentan yang menerima bantuan sosial pangan', 3);
INSERT INTO `m_indikator` VALUES (20, 'Persentase ODF', 'Persentase Desa/Kelurahan stop Buang Air Besar Sembarangan (BABS) atau Open Defecation Free (ODF)', 3);

-- ----------------------------
-- Table structure for m_kabupaten
-- ----------------------------
DROP TABLE IF EXISTS `m_kabupaten`;
CREATE TABLE `m_kabupaten`  (
  `id` char(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_prov` char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` tinytext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_jenis` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_kabupaten
-- ----------------------------
INSERT INTO `m_kabupaten` VALUES ('3672', '36', 'Kota Cilegon', 2);

-- ----------------------------
-- Table structure for m_kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `m_kecamatan`;
CREATE TABLE `m_kecamatan`  (
  `id` char(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kab` char(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` tinytext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `color` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_kecamatan
-- ----------------------------
INSERT INTO `m_kecamatan` VALUES ('367201', '3672', 'Cibeber', 'indigo');
INSERT INTO `m_kecamatan` VALUES ('367202', '3672', 'Cilegon', 'green');
INSERT INTO `m_kecamatan` VALUES ('367203', '3672', 'Pulomerak', 'warning');
INSERT INTO `m_kecamatan` VALUES ('367204', '3672', 'Ciwandan', 'purple');
INSERT INTO `m_kecamatan` VALUES ('367205', '3672', ' Jombang', 'indigo');
INSERT INTO `m_kecamatan` VALUES ('367206', '3672', 'Gerogol', 'green');
INSERT INTO `m_kecamatan` VALUES ('367207', '3672', 'Purwakarta', 'warning');
INSERT INTO `m_kecamatan` VALUES ('367208', '3672', 'Citangkil', 'purple');

-- ----------------------------
-- Table structure for m_kelurahan
-- ----------------------------
DROP TABLE IF EXISTS `m_kelurahan`;
CREATE TABLE `m_kelurahan`  (
  `id` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kec` char(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` tinytext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_jenis` int(11) NOT NULL,
  `total_tps` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_kelurahan
-- ----------------------------
INSERT INTO `m_kelurahan` VALUES ('3672011001', '367201', 'Cibeber', 3, 60);
INSERT INTO `m_kelurahan` VALUES ('3672011002', '367201', 'Kedaleman', 3, 30);
INSERT INTO `m_kelurahan` VALUES ('3672011003', '367201', 'Bulakan', 3, 15);
INSERT INTO `m_kelurahan` VALUES ('3672011004', '367201', 'Cikerai', 3, 12);
INSERT INTO `m_kelurahan` VALUES ('3672011005', '367201', 'Karang Asem', 3, 28);
INSERT INTO `m_kelurahan` VALUES ('3672011006', '367201', 'Kalitimbang', 3, 24);
INSERT INTO `m_kelurahan` VALUES ('3672021001', '367202', 'Bagendung', 3, 12);
INSERT INTO `m_kelurahan` VALUES ('3672021002', '367202', 'Ciwedus', 3, 36);
INSERT INTO `m_kelurahan` VALUES ('3672021003', '367202', 'Bendungan', 3, 25);
INSERT INTO `m_kelurahan` VALUES ('3672021004', '367202', 'Ketileng', 3, 17);
INSERT INTO `m_kelurahan` VALUES ('3672021005', '367202', 'Ciwaduk', 3, 35);
INSERT INTO `m_kelurahan` VALUES ('3672031001', '367203', 'Tamansari', 3, 45);
INSERT INTO `m_kelurahan` VALUES ('3672031002', '367203', 'Lebakgede', 3, 44);
INSERT INTO `m_kelurahan` VALUES ('3672031003', '367203', 'Mekarsari', 3, 39);
INSERT INTO `m_kelurahan` VALUES ('3672031004', '367203', 'Suralaya', 3, 29);
INSERT INTO `m_kelurahan` VALUES ('3672041001', '367204', 'Banjar Negara', 3, 20);
INSERT INTO `m_kelurahan` VALUES ('3672041002', '367204', 'Tegal Ratu', 3, 29);
INSERT INTO `m_kelurahan` VALUES ('3672041003', '367204', 'Kubangsari', 3, 19);
INSERT INTO `m_kelurahan` VALUES ('3672041004', '367204', 'Gunung Sugih', 3, 19);
INSERT INTO `m_kelurahan` VALUES ('3672041005', '367204', 'Kepuh', 3, 22);
INSERT INTO `m_kelurahan` VALUES ('3672041006', '367204', 'Randakari', 3, 24);
INSERT INTO `m_kelurahan` VALUES ('3672051001', '367205', 'Sukmajaya', 3, 33);
INSERT INTO `m_kelurahan` VALUES ('3672051002', '367205', 'Jombang Wetan', 3, 64);
INSERT INTO `m_kelurahan` VALUES ('3672051003', '367205', 'Masigit', 3, 41);
INSERT INTO `m_kelurahan` VALUES ('3672051004', '367205', 'Panggung Rawi', 3, 31);
INSERT INTO `m_kelurahan` VALUES ('3672051005', '367205', 'Gedong Dalem', 3, 20);
INSERT INTO `m_kelurahan` VALUES ('3672061001', '367206', 'Kotasari', 3, 22);
INSERT INTO `m_kelurahan` VALUES ('3672061002', '367206', 'Grogol', 3, 13);
INSERT INTO `m_kelurahan` VALUES ('3672061003', '367206', 'Rawa Arum', 3, 44);
INSERT INTO `m_kelurahan` VALUES ('3672061004', '367206', 'Gerem', 3, 42);
INSERT INTO `m_kelurahan` VALUES ('3672071001', '367207', 'Ramanuju', 3, 8);
INSERT INTO `m_kelurahan` VALUES ('3672071002', '367207', 'Kotabumi', 3, 29);
INSERT INTO `m_kelurahan` VALUES ('3672071003', '367207', 'Kebon Dalem', 3, 40);
INSERT INTO `m_kelurahan` VALUES ('3672071004', '367207', 'Purwakarta', 3, 18);
INSERT INTO `m_kelurahan` VALUES ('3672071005', '367207', 'Tegal Bunder', 3, 14);
INSERT INTO `m_kelurahan` VALUES ('3672071006', '367207', 'Pabean', 3, 11);
INSERT INTO `m_kelurahan` VALUES ('3672081001', '367208', 'Warnasari', 3, 35);
INSERT INTO `m_kelurahan` VALUES ('3672081002', '367208', 'Dringo', 3, 23);
INSERT INTO `m_kelurahan` VALUES ('3672081003', '367208', 'Lebak Denok', 3, 21);
INSERT INTO `m_kelurahan` VALUES ('3672081004', '367208', 'Taman Baru', 3, 22);
INSERT INTO `m_kelurahan` VALUES ('3672081005', '367208', 'Kebonsari', 3, 33);
INSERT INTO `m_kelurahan` VALUES ('3672081006', '367208', 'Samangraya', 3, 27);
INSERT INTO `m_kelurahan` VALUES ('3672081007', '367208', 'Citangkil', 3, 48);

-- ----------------------------
-- Table structure for m_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `m_provinsi`;
CREATE TABLE `m_provinsi`  (
  `id` char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` tinytext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_provinsi
-- ----------------------------
INSERT INTO `m_provinsi` VALUES ('11', 'Aceh');
INSERT INTO `m_provinsi` VALUES ('12', 'Sumatera Utara');
INSERT INTO `m_provinsi` VALUES ('13', 'Sumatera Barat');
INSERT INTO `m_provinsi` VALUES ('14', 'Riau');
INSERT INTO `m_provinsi` VALUES ('15', 'Jambi');
INSERT INTO `m_provinsi` VALUES ('16', 'Sumatera Selatan');
INSERT INTO `m_provinsi` VALUES ('17', 'Bengkulu');
INSERT INTO `m_provinsi` VALUES ('18', 'Lampung');
INSERT INTO `m_provinsi` VALUES ('19', 'Kepulauan Bangka Belitung');
INSERT INTO `m_provinsi` VALUES ('21', 'Kepulauan Riau');
INSERT INTO `m_provinsi` VALUES ('31', 'Dki Jakarta');
INSERT INTO `m_provinsi` VALUES ('32', 'Jawa Barat');
INSERT INTO `m_provinsi` VALUES ('33', 'Jawa Tengah');
INSERT INTO `m_provinsi` VALUES ('34', 'Di Yogyakarta');
INSERT INTO `m_provinsi` VALUES ('35', 'Jawa Timur');
INSERT INTO `m_provinsi` VALUES ('36', 'Banten');
INSERT INTO `m_provinsi` VALUES ('51', 'Bali');
INSERT INTO `m_provinsi` VALUES ('52', 'Nusa Tenggara Barat');
INSERT INTO `m_provinsi` VALUES ('53', 'Nusa Tenggara Timur');
INSERT INTO `m_provinsi` VALUES ('61', 'Kalimantan Barat');
INSERT INTO `m_provinsi` VALUES ('62', 'Kalimantan Tengah');
INSERT INTO `m_provinsi` VALUES ('63', 'Kalimantan Selatan');
INSERT INTO `m_provinsi` VALUES ('64', 'Kalimantan Timur');
INSERT INTO `m_provinsi` VALUES ('65', 'Kalimantan Utara');
INSERT INTO `m_provinsi` VALUES ('71', 'Sulawesi Utara');
INSERT INTO `m_provinsi` VALUES ('72', 'Sulawesi Tengah');
INSERT INTO `m_provinsi` VALUES ('73', 'Sulawesi Selatan');
INSERT INTO `m_provinsi` VALUES ('74', 'Sulawesi Tenggara');
INSERT INTO `m_provinsi` VALUES ('75', 'Gorontalo');
INSERT INTO `m_provinsi` VALUES ('76', 'Sulawesi Barat');
INSERT INTO `m_provinsi` VALUES ('81', 'Maluku');
INSERT INTO `m_provinsi` VALUES ('82', 'Maluku Utara');
INSERT INTO `m_provinsi` VALUES ('92', 'Papua');
INSERT INTO `m_provinsi` VALUES ('91', 'Papua Barat');

-- ----------------------------
-- Table structure for m_role
-- ----------------------------
DROP TABLE IF EXISTS `m_role`;
CREATE TABLE `m_role`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_role
-- ----------------------------
INSERT INTO `m_role` VALUES (1, 'Administrator', 'Administrator');
INSERT INTO `m_role` VALUES (2, 'Admin Caleg', 'Admin Caleg');
INSERT INTO `m_role` VALUES (3, 'Calon Legislatif', 'Caleg ');
INSERT INTO `m_role` VALUES (4, 'Pengawas Provinsi', 'Pengawas Provinsi');
INSERT INTO `m_role` VALUES (5, 'Pengawas Kota / Kab', 'Pengawas Kota / Kab');
INSERT INTO `m_role` VALUES (6, 'Pengawas Kecamatan', 'Pengawas Kecamatan');
INSERT INTO `m_role` VALUES (7, 'Pengawas Kelurahan', 'Pengawas Kelurahan');
INSERT INTO `m_role` VALUES (8, 'Pengaws TPS', 'Pengaws TPS');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (2, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
INSERT INTO `personal_access_tokens` VALUES (1, 'App\\Models\\User', 5, 'MyApp', 'be01d32565ebf93c8d303e25ad75b43030a4737bf28181d8c4fa28c222f142ed', '[\"*\"]', NULL, '2023-10-27 17:23:33', '2023-10-27 17:23:33');
INSERT INTO `personal_access_tokens` VALUES (2, 'App\\Models\\User', 5, 'MyApp', '0b9b7918c08948f5502a0b517c4f37dcff267e104b0d6ba9367baf81fda29d87', '[\"*\"]', NULL, '2023-10-27 17:26:51', '2023-10-27 17:26:51');
INSERT INTO `personal_access_tokens` VALUES (3, 'App\\Models\\User', 39, 'MyApp', 'b37728e6e40344b0eb6db79017469d98402d3701472094122c74ddb34bde4dfd', '[\"*\"]', '2023-10-31 10:42:16', '2023-10-31 10:27:45', '2023-10-31 10:42:16');
INSERT INTO `personal_access_tokens` VALUES (4, 'App\\Models\\User', 40, 'MyApp', 'dc9ccbd8593109c573f7bd44754f8e7e3c97b789081db693c84d905cea56fdc0', '[\"*\"]', '2023-10-31 11:34:06', '2023-10-31 10:46:09', '2023-10-31 11:34:06');

-- ----------------------------
-- Table structure for t_gizi
-- ----------------------------
DROP TABLE IF EXISTS `t_gizi`;
CREATE TABLE `t_gizi`  (
  `id` int(10) NOT NULL,
  `kelurahan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keluarahan_id` int(10) NULL DEFAULT NULL,
  `SKL23` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Laki-laki usia 0-23 bulan',
  `SKP23` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Perempuan usia 0-23 bulan',
  `SKL59` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Laki-laki usia 0-23 bulan',
  `SKP59` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Perempuan usia 0-59 bulan',
  `KRL23` int(8) NULL DEFAULT NULL COMMENT 'Kurang Laki-laki usia 0-23 bulan',
  `KRP23` int(8) NULL DEFAULT NULL COMMENT 'Kurang Perempuan usia 0-23 bulan',
  `KRL59` int(8) NULL DEFAULT NULL COMMENT 'Kurang Laki-laki usia 0-59 bulan',
  `KRP59` int(8) NULL DEFAULT NULL COMMENT '\'Kurang Perempuan usia 0-59 bulan\'',
  `BBNL23` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Laki-laki usia 0-23 bulan\'',
  `BBNP23` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Perempuan usia 0-23 bulan\'',
  `BBNL59` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Laki-laki usia 0-59 bulan\'',
  `BBNP59` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Perempuan usia 0-593 bulan\'',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_gizi
-- ----------------------------

-- ----------------------------
-- Table structure for t_gizi_spesifikasi
-- ----------------------------
DROP TABLE IF EXISTS `t_gizi_spesifikasi`;
CREATE TABLE `t_gizi_spesifikasi`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kecamatan_id` int(10) NULL DEFAULT NULL,
  `kelurahan_id` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelurahan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sas_jan` int(5) NULL DEFAULT NULL,
  `sas_feb` int(5) NULL DEFAULT NULL,
  `sas_mar` int(5) NULL DEFAULT NULL,
  `sas_apr` int(5) NULL DEFAULT NULL,
  `sas_mei` int(5) NULL DEFAULT NULL,
  `sas_jun` int(5) NULL DEFAULT NULL,
  `sas_jul` int(5) NULL DEFAULT NULL,
  `sas_agu` int(5) NULL DEFAULT NULL,
  `sas_sep` int(5) NULL DEFAULT NULL,
  `sas_okt` int(5) NULL DEFAULT NULL,
  `sas_nov` int(5) NULL DEFAULT NULL,
  `sas_des` int(5) NULL DEFAULT NULL,
  `cap_jan` decimal(5, 2) NULL DEFAULT NULL,
  `cap_feb` decimal(5, 2) NULL DEFAULT NULL,
  `cap_mar` decimal(5, 2) NULL DEFAULT NULL,
  `cap_apr` decimal(5, 2) NULL DEFAULT NULL,
  `cap_mei` decimal(5, 2) NULL DEFAULT NULL,
  `cap_jun` decimal(5, 2) NULL DEFAULT NULL,
  `cap_jul` decimal(5, 2) NULL DEFAULT NULL,
  `cap_agu` decimal(5, 2) NULL DEFAULT NULL,
  `cap_sep` decimal(5, 2) NULL DEFAULT NULL,
  `cap_okt` decimal(5, 2) NULL DEFAULT NULL,
  `cap_nov` decimal(5, 2) NULL DEFAULT NULL,
  `cap_des` decimal(5, 2) NULL DEFAULT NULL,
  `sas_tw1` int(5) NULL DEFAULT NULL,
  `cap_tw1` int(5) NULL DEFAULT NULL,
  `sas_tw2` int(5) NULL DEFAULT NULL,
  `cap_tw2` int(5) NULL DEFAULT NULL,
  `sas_tw3` int(5) NULL DEFAULT NULL,
  `sas_tw4` int(5) NULL DEFAULT NULL,
  `cap_tw3` int(5) NULL DEFAULT NULL,
  `cap_tw4` int(5) NULL DEFAULT NULL,
  `indikator_id` int(5) NULL DEFAULT NULL,
  `tahun` int(5) NULL DEFAULT NULL,
  `kecamatan` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `t_header_id` int(10) NULL DEFAULT NULL,
  `nomor` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah` int(10) NULL DEFAULT NULL,
  `odf` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 528 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_gizi_spesifikasi
-- ----------------------------
INSERT INTO `t_gizi_spesifikasi` VALUES (1, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cibeber', 2, '3672013672011003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (2, 367201, '3672011004', 'Cikerai', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cibeber', 2, '3672013672011004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (3, 367201, '3672011006', 'Kalitimbang', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cibeber', 2, '3672013672011006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (4, 367201, '3672011005', 'Karang Asem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cibeber', 2, '3672013672011005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (5, 367201, '3672011001', 'Cibeber', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cibeber', 2, '3672013672011001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (6, 367201, '3672011002', 'Kedaleman', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cibeber', 2, '3672013672011002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (7, 367202, '3672021005', 'Ciwaduk', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cilegon', 2, '3672023672021005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (8, 367202, '3672021002', 'Ciwedus', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cilegon', 2, '3672023672021002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (9, 367202, '3672021003', 'Bendungan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cilegon', 2, '3672023672021003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (10, 367202, '3672021004', 'Ketileng', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cilegon', 2, '3672023672021004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (11, 367202, '3672021001', 'Bagendung', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Cilegon', 2, '3672023672021001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (12, 367203, '3672031004', 'Suralaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Pulomerak', 2, '3672033672031004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (13, 367204, '3672041003', 'Kubangsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Ciwandan', 2, '3672043672041003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (14, 367204, '3672041002', 'Tegal Ratu', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Ciwandan', 2, '3672043672041002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (15, 367204, '3672041006', 'Randakari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Ciwandan', 2, '3672043672041006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (16, 367204, '3672041005', 'Kepuh', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Ciwandan', 2, '3672043672041005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (17, 367204, '3672041004', 'Gunung Sugih', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Ciwandan', 2, '3672043672041004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (18, 367205, '3672051004', 'Panggung Rawi', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, ' Jombang', 2, '3672053672051004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (19, 367205, '3672051002', 'Jombang Wetan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, ' Jombang', 2, '3672053672051002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (20, 367205, '3672051005', 'Gedong Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, ' Jombang', 2, '3672053672051005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (21, 367205, '3672051001', 'Sukmajaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, ' Jombang', 2, '3672053672051001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (22, 367205, '3672051003', 'Masigit', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, ' Jombang', 2, '3672053672051003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (23, 367206, '3672061004', 'Gerem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Gerogol', 2, '3672063672061004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (24, 367206, '3672061003', 'Rawa Arum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Gerogol', 2, '3672063672061003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (25, 367206, '3672061001', 'Kotasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Gerogol', 2, '3672063672061001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (26, 367206, '3672061002', 'Grogol', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Gerogol', 2, '3672063672061002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (27, 367207, '3672071006', 'Pabean', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Purwakarta', 2, '3672073672071006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (28, 367207, '3672071005', 'Tegal Bunder', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Purwakarta', 2, '3672073672071005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (29, 367207, '3672071004', 'Purwakarta', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Purwakarta', 2, '3672073672071004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (30, 367207, '3672071003', 'Kebon Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Purwakarta', 2, '3672073672071003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (31, 367207, '3672071001', 'Ramanuju', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Purwakarta', 2, '3672073672071001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (32, 367208, '3672081007', 'Citangkil', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Citangkil', 2, '3672083672081007', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (33, 367208, '3672081005', 'Kebonsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Citangkil', 2, '3672083672081005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (34, 367208, '3672081004', 'Taman Baru', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Citangkil', 2, '3672083672081004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (35, 367208, '3672081001', 'Warnasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Citangkil', 2, '3672083672081001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (36, 367208, '3672081003', 'Lebak Denok', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2023, 'Citangkil', 2, '3672083672081003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (37, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cibeber', 2, '3672013672011003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (38, 367201, '3672011004', 'Cikerai', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cibeber', 2, '3672013672011004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (39, 367201, '3672011006', 'Kalitimbang', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cibeber', 2, '3672013672011006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (40, 367201, '3672011005', 'Karang Asem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cibeber', 2, '3672013672011005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (41, 367201, '3672011001', 'Cibeber', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cibeber', 2, '3672013672011001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (42, 367201, '3672011002', 'Kedaleman', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cibeber', 2, '3672013672011002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (43, 367202, '3672021005', 'Ciwaduk', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cilegon', 2, '3672023672021005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (44, 367202, '3672021002', 'Ciwedus', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cilegon', 2, '3672023672021002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (45, 367202, '3672021003', 'Bendungan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cilegon', 2, '3672023672021003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (46, 367202, '3672021004', 'Ketileng', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cilegon', 2, '3672023672021004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (47, 367202, '3672021001', 'Bagendung', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Cilegon', 2, '3672023672021001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (48, 367203, '3672031004', 'Suralaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Pulomerak', 2, '3672033672031004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (49, 367204, '3672041003', 'Kubangsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Ciwandan', 2, '3672043672041003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (50, 367204, '3672041002', 'Tegal Ratu', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Ciwandan', 2, '3672043672041002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (51, 367204, '3672041006', 'Randakari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Ciwandan', 2, '3672043672041006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (52, 367204, '3672041005', 'Kepuh', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Ciwandan', 2, '3672043672041005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (53, 367204, '3672041004', 'Gunung Sugih', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Ciwandan', 2, '3672043672041004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (54, 367205, '3672051004', 'Panggung Rawi', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, ' Jombang', 2, '3672053672051004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (55, 367205, '3672051002', 'Jombang Wetan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, ' Jombang', 2, '3672053672051002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (56, 367205, '3672051005', 'Gedong Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, ' Jombang', 2, '3672053672051005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (57, 367205, '3672051001', 'Sukmajaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, ' Jombang', 2, '3672053672051001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (58, 367205, '3672051003', 'Masigit', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, ' Jombang', 2, '3672053672051003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (59, 367206, '3672061004', 'Gerem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Gerogol', 2, '3672063672061004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (60, 367206, '3672061003', 'Rawa Arum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Gerogol', 2, '3672063672061003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (61, 367206, '3672061001', 'Kotasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Gerogol', 2, '3672063672061001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (62, 367206, '3672061002', 'Grogol', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Gerogol', 2, '3672063672061002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (63, 367207, '3672071006', 'Pabean', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Purwakarta', 2, '3672073672071006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (64, 367207, '3672071005', 'Tegal Bunder', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Purwakarta', 2, '3672073672071005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (65, 367207, '3672071004', 'Purwakarta', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Purwakarta', 2, '3672073672071004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (66, 367207, '3672071003', 'Kebon Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Purwakarta', 2, '3672073672071003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (67, 367207, '3672071001', 'Ramanuju', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Purwakarta', 2, '3672073672071001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (68, 367208, '3672081007', 'Citangkil', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Citangkil', 2, '3672083672081007', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (69, 367208, '3672081005', 'Kebonsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Citangkil', 2, '3672083672081005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (70, 367208, '3672081004', 'Taman Baru', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Citangkil', 2, '3672083672081004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (71, 367208, '3672081001', 'Warnasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Citangkil', 2, '3672083672081001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (72, 367208, '3672081003', 'Lebak Denok', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 11, 2023, 'Citangkil', 2, '3672083672081003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (73, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cibeber', 2, '3672013672011003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (74, 367201, '3672011004', 'Cikerai', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cibeber', 2, '3672013672011004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (75, 367201, '3672011006', 'Kalitimbang', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cibeber', 2, '3672013672011006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (76, 367201, '3672011005', 'Karang Asem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cibeber', 2, '3672013672011005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (77, 367201, '3672011001', 'Cibeber', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cibeber', 2, '3672013672011001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (78, 367201, '3672011002', 'Kedaleman', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cibeber', 2, '3672013672011002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (79, 367202, '3672021005', 'Ciwaduk', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cilegon', 2, '3672023672021005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (80, 367202, '3672021002', 'Ciwedus', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cilegon', 2, '3672023672021002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (81, 367202, '3672021003', 'Bendungan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cilegon', 2, '3672023672021003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (82, 367202, '3672021004', 'Ketileng', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cilegon', 2, '3672023672021004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (83, 367202, '3672021001', 'Bagendung', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Cilegon', 2, '3672023672021001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (84, 367203, '3672031004', 'Suralaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Pulomerak', 2, '3672033672031004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (85, 367204, '3672041003', 'Kubangsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Ciwandan', 2, '3672043672041003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (86, 367204, '3672041002', 'Tegal Ratu', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Ciwandan', 2, '3672043672041002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (87, 367204, '3672041006', 'Randakari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Ciwandan', 2, '3672043672041006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (88, 367204, '3672041005', 'Kepuh', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Ciwandan', 2, '3672043672041005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (89, 367204, '3672041004', 'Gunung Sugih', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Ciwandan', 2, '3672043672041004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (90, 367205, '3672051004', 'Panggung Rawi', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, ' Jombang', 2, '3672053672051004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (91, 367205, '3672051002', 'Jombang Wetan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, ' Jombang', 2, '3672053672051002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (92, 367205, '3672051005', 'Gedong Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, ' Jombang', 2, '3672053672051005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (93, 367205, '3672051001', 'Sukmajaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, ' Jombang', 2, '3672053672051001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (94, 367205, '3672051003', 'Masigit', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, ' Jombang', 2, '3672053672051003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (95, 367206, '3672061004', 'Gerem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Gerogol', 2, '3672063672061004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (96, 367206, '3672061003', 'Rawa Arum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Gerogol', 2, '3672063672061003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (97, 367206, '3672061001', 'Kotasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Gerogol', 2, '3672063672061001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (98, 367206, '3672061002', 'Grogol', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Gerogol', 2, '3672063672061002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (99, 367207, '3672071006', 'Pabean', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Purwakarta', 2, '3672073672071006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (100, 367207, '3672071005', 'Tegal Bunder', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Purwakarta', 2, '3672073672071005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (101, 367207, '3672071004', 'Purwakarta', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Purwakarta', 2, '3672073672071004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (102, 367207, '3672071003', 'Kebon Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Purwakarta', 2, '3672073672071003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (103, 367207, '3672071001', 'Ramanuju', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Purwakarta', 2, '3672073672071001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (104, 367208, '3672081007', 'Citangkil', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Citangkil', 2, '3672083672081007', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (105, 367208, '3672081005', 'Kebonsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Citangkil', 2, '3672083672081005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (106, 367208, '3672081004', 'Taman Baru', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Citangkil', 2, '3672083672081004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (107, 367208, '3672081001', 'Warnasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Citangkil', 2, '3672083672081001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (108, 367208, '3672081003', 'Lebak Denok', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 12, 2023, 'Citangkil', 2, '3672083672081003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (109, 367201, '3672011003', 'Bulakan', 1488, 1491, 1488, 1494, 1494, 1497, 1494, 1500, 1500, 0, 0, 0, 85.00, 85.00, 85.00, 85.00, 85.00, 86.00, 85.00, 86.00, 86.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cibeber', 2, '3672013672011003', 1738, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (110, 367201, '3672011004', 'Cikerai', 985, 988, 988, 991, 991, 993, 991, 997, 997, 0, 0, 0, 80.00, 80.00, 80.00, 80.00, 80.00, 80.00, 80.00, 81.00, 81.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cibeber', 2, '3672013672011004', 1227, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (111, 367201, '3672011006', 'Kalitimbang', 2109, 2115, 2109, 2118, 2118, 2119, 2118, 2119, 2119, 0, 0, 0, 88.00, 88.00, 88.00, 88.00, 88.00, 88.00, 88.00, 88.00, 88.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cibeber', 2, '3672013672011006', 2384, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (112, 367201, '3672011005', 'Karang Asem', 2554, 2558, 2559, 2563, 2563, 2568, 2563, 2577, 2582, 0, 0, 0, 73.00, 73.00, 73.00, 73.00, 73.00, 73.00, 73.00, 74.00, 74.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cibeber', 2, '3672013672011005', 3481, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (113, 367201, '3672011001', 'Cibeber', 5716, 5724, 5716, 5724, 5724, 5726, 5724, 5728, 5728, 0, 0, 0, 89.00, 89.00, 89.00, 89.00, 89.00, 89.00, 89.00, 89.00, 89.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cibeber', 2, '3672013672011001', 6420, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (114, 367201, '3672011002', 'Kedaleman', 2662, 2667, 2667, 2672, 2672, 2675, 2672, 2691, 2691, 0, 0, 0, 85.00, 85.00, 85.00, 85.00, 85.00, 85.00, 85.00, 86.00, 86.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cibeber', 2, '3672013672011002', 3121, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (115, 367202, '3672021005', 'Ciwaduk', 337, 556, 616, 936, 1141, 1376, 1141, 1572, 1730, 0, 0, 0, 8.00, 14.00, 15.00, 24.00, 29.00, 35.00, 29.00, 40.00, 44.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cilegon', 2, '3672023672021005', 3866, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (116, 367202, '3672021002', 'Ciwedus', 268, 533, 578, 706, 806, 906, 806, 1175, 1287, 0, 0, 0, 6.00, 13.00, 14.00, 17.00, 20.00, 22.00, 20.00, 29.00, 32.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cilegon', 2, '3672023672021002', 3972, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (117, 367202, '3672021003', 'Bendungan', 334, 610, 610, 763, 933, 1083, 933, 1425, 1627, 0, 0, 0, 10.00, 19.00, 19.00, 24.00, 29.00, 34.00, 29.00, 45.00, 52.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cilegon', 2, '3672023672021003', 3127, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (118, 367202, '3672021004', 'Ketileng', 200, 446, 446, 902, 1092, 1267, 1092, 1653, 1653, 0, 0, 0, 8.00, 19.00, 19.00, 38.00, 46.00, 54.00, 46.00, 71.00, 71.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cilegon', 2, '3672023672021004', 2325, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (119, 367202, '3672021001', 'Bagendung', 227, 446, 446, 695, 797, 986, 797, 1891, 2184, 0, 0, 0, 10.00, 21.00, 21.00, 32.00, 37.00, 46.00, 37.00, 89.00, 103.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Cilegon', 2, '3672023672021001', 2113, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (120, 367203, '3672031004', 'Suralaya', 1965, 1965, 1965, 1965, 1966, 1968, 1966, 1974, 1977, 0, 0, 0, 86.00, 86.00, 86.00, 86.00, 86.00, 86.00, 86.00, 87.00, 87.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Pulomerak', 2, '3672033672031004', 2268, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (121, 367204, '3672041003', 'Kubangsari', 1965, 1980, 1965, 1965, 1965, 1965, 1965, 1965, 1965, 0, 0, 0, 83.00, 83.00, 83.00, 83.00, 83.00, 83.00, 83.00, 83.00, 83.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Ciwandan', 2, '3672043672041003', 2365, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (122, 367204, '3672041002', 'Tegal Ratu', 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 3150, 0, 0, 0, 92.00, 92.00, 92.00, 92.00, 92.00, 92.00, 92.00, 92.00, 92.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Ciwandan', 2, '3672043672041002', 3411, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (123, 367204, '3672041006', 'Randakari', 2250, 2250, 2250, 2250, 2250, 2250, 2250, 2250, 2250, 0, 0, 0, 80.00, 80.00, 80.00, 80.00, 80.00, 80.00, 80.00, 80.00, 80.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Ciwandan', 2, '3672043672041006', 2783, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (124, 367204, '3672041005', 'Kepuh', 2125, 2135, 2125, 2125, 2125, 2125, 2125, 2125, 2125, 0, 0, 0, 77.00, 77.00, 77.00, 77.00, 77.00, 77.00, 77.00, 77.00, 77.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Ciwandan', 2, '3672043672041005', 2752, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (125, 367204, '3672041004', 'Gunung Sugih', 2015, 2050, 2015, 2015, 2015, 2015, 2015, 2015, 2015, 0, 0, 0, 88.00, 90.00, 88.00, 88.00, 88.00, 88.00, 88.00, 88.00, 88.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Ciwandan', 2, '3672043672041004', 2269, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (126, 367205, '3672051004', 'Panggung Rawi', 3199, 3199, 2604, 2604, 2611, 2611, 2611, 2611, 2626, 0, 0, 0, 97.00, 97.00, 79.00, 79.00, 79.00, 79.00, 79.00, 79.00, 80.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, ' Jombang', 2, '3672053672051004', 3270, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (127, 367205, '3672051002', 'Jombang Wetan', 6930, 6930, 4343, 4343, 4346, 4346, 4346, 4346, 4361, 0, 0, 0, 98.00, 98.00, 62.00, 62.00, 62.00, 62.00, 62.00, 62.00, 62.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, ' Jombang', 2, '3672053672051002', 7001, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (128, 367205, '3672051005', 'Gedong Dalem', 2426, 2426, 1917, 1917, 1925, 1925, 1925, 1925, 1940, 0, 0, 0, 96.00, 96.00, 76.00, 76.00, 76.00, 76.00, 76.00, 76.00, 77.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, ' Jombang', 2, '3672053672051005', 2515, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (129, 367205, '3672051001', 'Sukmajaya', 3846, 3846, 2555, 2555, 2564, 2564, 2564, 2564, 2579, 0, 0, 0, 96.00, 96.00, 63.00, 63.00, 64.00, 64.00, 64.00, 64.00, 64.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, ' Jombang', 2, '3672053672051001', 4004, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (130, 367205, '3672051003', 'Masigit', 4590, 4590, 3242, 3242, 3256, 3256, 3256, 3256, 3271, 0, 0, 0, 97.00, 97.00, 68.00, 68.00, 69.00, 69.00, 69.00, 69.00, 69.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, ' Jombang', 2, '3672053672051003', 4708, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (131, 367206, '3672061004', 'Gerem', 4270, 4270, 4270, 4270, 4295, 4295, 4295, 4295, 4295, 0, 0, 0, 88.00, 88.00, 88.00, 88.00, 89.00, 89.00, 89.00, 89.00, 89.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Gerogol', 2, '3672063672061004', 4823, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (132, 367206, '3672061003', 'Rawa Arum', 4565, 4565, 4565, 4565, 4598, 4598, 4598, 4598, 4598, 0, 0, 0, 93.00, 93.00, 93.00, 93.00, 94.00, 94.00, 94.00, 94.00, 94.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Gerogol', 2, '3672063672061003', 4885, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (133, 367206, '3672061001', 'Kotasari', 2535, 2535, 2535, 2535, 2615, 2615, 2615, 2615, 2615, 0, 0, 0, 91.00, 91.00, 91.00, 91.00, 94.00, 94.00, 94.00, 94.00, 94.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Gerogol', 2, '3672063672061001', 2779, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (134, 367206, '3672061002', 'Grogol', 1345, 1345, 1345, 1345, 1365, 1365, 1365, 1365, 1365, 0, 0, 0, 88.00, 88.00, 88.00, 88.00, 89.00, 89.00, 89.00, 89.00, 89.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Gerogol', 2, '3672063672061002', 1528, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (135, 367207, '3672071006', 'Pabean', 89, 218, 242, 298, 367, 438, 367, 438, 518, 0, 0, 0, 7.00, 17.00, 19.00, 24.00, 30.00, 35.00, 30.00, 35.00, 42.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Purwakarta', 2, '3672073672071006', 1218, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (136, 367207, '3672071005', 'Tegal Bunder', 122, 253, 265, 279, 370, 449, 370, 449, 525, 0, 0, 0, 7.00, 14.00, 15.00, 16.00, 21.00, 26.00, 21.00, 26.00, 30.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Purwakarta', 2, '3672073672071005', 1703, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (137, 367207, '3672071004', 'Purwakarta', 94, 353, 373, 391, 475, 582, 475, 582, 680, 0, 0, 0, 4.00, 16.00, 17.00, 17.00, 21.00, 26.00, 21.00, 26.00, 31.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Purwakarta', 2, '3672073672071004', 2181, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (138, 367207, '3672071003', 'Kebon Dalem', 213, 378, 392, 415, 559, 718, 559, 718, 809, 0, 0, 0, 4.00, 7.00, 8.00, 8.00, 11.00, 15.00, 11.00, 15.00, 17.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Purwakarta', 2, '3672073672071003', 4748, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (139, 367207, '3672071001', 'Ramanuju', 33, 93, 112, 132, 201, 252, 201, 252, 314, 0, 0, 0, 4.00, 13.00, 15.00, 18.00, 28.00, 35.00, 28.00, 35.00, 44.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Purwakarta', 2, '3672073672071001', 712, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (140, 367208, '3672081007', 'Citangkil', 5107, 5107, 5107, 5107, 5107, 5107, 5107, 5107, 5107, 0, 0, 0, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Citangkil', 2, '3672083672081007', 5147, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (141, 367208, '3672081005', 'Kebonsari', 3895, 3895, 3895, 3895, 3895, 3895, 3895, 3895, 3895, 0, 0, 0, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Citangkil', 2, '3672083672081005', 3915, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (142, 367208, '3672081004', 'Taman Baru', 2703, 2703, 2703, 2703, 2703, 2703, 2703, 2703, 2703, 0, 0, 0, 97.00, 97.00, 97.00, 97.00, 97.00, 97.00, 97.00, 97.00, 97.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Citangkil', 2, '3672083672081004', 2773, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (143, 367208, '3672081001', 'Warnasari', 0, 1480, 2491, 29, 2515, 2630, 2515, 2886, 2998, 0, 0, 0, 0.00, 36.00, 61.00, 0.00, 62.00, 65.00, 62.00, 71.00, 74.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Citangkil', 2, '3672083672081001', 4021, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (144, 367208, '3672081003', 'Lebak Denok', 0, 0, 351, 24, 388, 487, 388, 844, 1352, 0, 0, 0, 0.00, 0.00, 11.00, 0.00, 12.00, 15.00, 12.00, 27.00, 44.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 13, 2023, 'Citangkil', 2, '3672083672081003', 3071, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (145, 367206, '3672061001', 'Kotasari', 1955, 1955, 2535, 1955, 2165, 2165, 2165, 2165, 2165, 0, 0, 0, 70.00, 70.00, 91.00, 70.00, 77.00, 77.00, 77.00, 77.00, 77.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Gerogol', 2, '3672063672061001', 2779, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (146, 367206, '3672061002', 'Grogol', 965, 965, 1345, 965, 996, 996, 996, 996, 996, 0, 0, 0, 63.00, 63.00, 88.00, 63.00, 65.00, 65.00, 65.00, 65.00, 65.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Gerogol', 2, '3672063672061002', 1528, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (147, 367207, '3672071006', 'Pabean', 189, 218, 234, 277, 371, 441, 371, 441, 518, 0, 0, 0, 15.00, 17.00, 19.00, 22.00, 30.00, 36.00, 30.00, 36.00, 42.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Purwakarta', 2, '3672073672071006', 1218, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (148, 367207, '3672071005', 'Tegal Bunder', 184, 251, 274, 274, 390, 451, 390, 451, 524, 0, 0, 0, 10.00, 14.00, 16.00, 16.00, 22.00, 26.00, 22.00, 26.00, 30.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Purwakarta', 2, '3672073672071005', 1703, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (149, 367207, '3672071004', 'Purwakarta', 155, 348, 362, 362, 490, 598, 490, 598, 678, 0, 0, 0, 7.00, 15.00, 16.00, 16.00, 22.00, 27.00, 22.00, 27.00, 31.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Purwakarta', 2, '3672073672071004', 2181, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (150, 367207, '3672071003', 'Kebon Dalem', 312, 378, 412, 412, 574, 721, 574, 721, 808, 0, 0, 0, 6.00, 7.00, 8.00, 8.00, 12.00, 15.00, 12.00, 15.00, 17.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Purwakarta', 2, '3672073672071003', 4748, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (151, 367207, '3672071001', 'Ramanuju', 35, 93, 112, 112, 220, 251, 220, 251, 311, 0, 0, 0, 4.00, 13.00, 15.00, 15.00, 30.00, 35.00, 30.00, 35.00, 43.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Purwakarta', 2, '3672073672071001', 712, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (152, 367208, '3672081007', 'Citangkil', 5100, 5100, 5100, 5100, 5100, 5100, 5100, 5100, 5100, 0, 0, 0, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 99.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Citangkil', 2, '3672083672081007', 5147, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (153, 367208, '3672081005', 'Kebonsari', 3855, 3855, 3855, 3855, 3855, 3855, 3855, 3855, 3855, 0, 0, 0, 98.00, 98.00, 98.00, 98.00, 98.00, 98.00, 98.00, 98.00, 98.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Citangkil', 2, '3672083672081005', 3915, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (154, 367208, '3672081004', 'Taman Baru', 2689, 2689, 2689, 2689, 2689, 2689, 2689, 2689, 2689, 0, 0, 0, 96.00, 96.00, 96.00, 96.00, 96.00, 96.00, 96.00, 96.00, 96.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Citangkil', 2, '3672083672081004', 2773, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (155, 367208, '3672081001', 'Warnasari', 0, 1480, 2491, 29, 2515, 2630, 2515, 2889, 2998, 0, 0, 0, 0.00, 36.00, 61.00, 0.00, 62.00, 65.00, 62.00, 71.00, 74.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Citangkil', 2, '3672083672081001', 4021, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (156, 367208, '3672081003', 'Lebak Denok', 0, 0, 356, 24, 388, 487, 388, 854, 1355, 0, 0, 0, 0.00, 0.00, 11.00, 0.00, 12.00, 15.00, 12.00, 27.00, 44.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 14, 2023, 'Citangkil', 2, '3672083672081003', 3071, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (157, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cibeber', 2, '3672013672011003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (158, 367201, '3672011004', 'Cikerai', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cibeber', 2, '3672013672011004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (159, 367201, '3672011006', 'Kalitimbang', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cibeber', 2, '3672013672011006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (160, 367201, '3672011005', 'Karang Asem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cibeber', 2, '3672013672011005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (161, 367201, '3672011001', 'Cibeber', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cibeber', 2, '3672013672011001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (162, 367201, '3672011002', 'Kedaleman', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cibeber', 2, '3672013672011002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (163, 367202, '3672021005', 'Ciwaduk', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cilegon', 2, '3672023672021005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (164, 367202, '3672021002', 'Ciwedus', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cilegon', 2, '3672023672021002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (165, 367202, '3672021003', 'Bendungan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cilegon', 2, '3672023672021003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (166, 367202, '3672021004', 'Ketileng', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cilegon', 2, '3672023672021004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (167, 367202, '3672021001', 'Bagendung', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Cilegon', 2, '3672023672021001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (168, 367203, '3672031004', 'Suralaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Pulomerak', 2, '3672033672031004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (169, 367204, '3672041003', 'Kubangsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Ciwandan', 2, '3672043672041003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (170, 367204, '3672041002', 'Tegal Ratu', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Ciwandan', 2, '3672043672041002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (171, 367204, '3672041006', 'Randakari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Ciwandan', 2, '3672043672041006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (172, 367204, '3672041005', 'Kepuh', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Ciwandan', 2, '3672043672041005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (173, 367204, '3672041004', 'Gunung Sugih', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Ciwandan', 2, '3672043672041004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (174, 367205, '3672051004', 'Panggung Rawi', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, ' Jombang', 2, '3672053672051004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (175, 367205, '3672051002', 'Jombang Wetan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, ' Jombang', 2, '3672053672051002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (176, 367205, '3672051005', 'Gedong Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, ' Jombang', 2, '3672053672051005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (177, 367205, '3672051001', 'Sukmajaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, ' Jombang', 2, '3672053672051001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (178, 367205, '3672051003', 'Masigit', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, ' Jombang', 2, '3672053672051003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (179, 367206, '3672061004', 'Gerem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Gerogol', 2, '3672063672061004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (180, 367206, '3672061003', 'Rawa Arum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Gerogol', 2, '3672063672061003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (181, 367206, '3672061001', 'Kotasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Gerogol', 2, '3672063672061001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (182, 367206, '3672061002', 'Grogol', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Gerogol', 2, '3672063672061002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (183, 367207, '3672071006', 'Pabean', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Purwakarta', 2, '3672073672071006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (184, 367207, '3672071005', 'Tegal Bunder', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Purwakarta', 2, '3672073672071005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (185, 367207, '3672071004', 'Purwakarta', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Purwakarta', 2, '3672073672071004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (186, 367207, '3672071003', 'Kebon Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Purwakarta', 2, '3672073672071003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (187, 367207, '3672071001', 'Ramanuju', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Purwakarta', 2, '3672073672071001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (188, 367208, '3672081007', 'Citangkil', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Citangkil', 2, '3672083672081007', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (189, 367208, '3672081005', 'Kebonsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Citangkil', 2, '3672083672081005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (190, 367208, '3672081004', 'Taman Baru', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Citangkil', 2, '3672083672081004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (191, 367208, '3672081001', 'Warnasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Citangkil', 2, '3672083672081001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (192, 367208, '3672081003', 'Lebak Denok', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 16, 2023, 'Citangkil', 2, '3672083672081003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (193, 367201, '3672011003', 'Bulakan', 990, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 300.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cibeber', 2, '3672013672011003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (194, 367201, '3672011004', 'Cikerai', 799, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 282.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cibeber', 2, '3672013672011004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (195, 367201, '3672011006', 'Kalitimbang', 991, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 230.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cibeber', 2, '3672013672011006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (196, 367201, '3672011005', 'Karang Asem', 1525, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 424.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cibeber', 2, '3672013672011005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (197, 367201, '3672011001', 'Cibeber', 922, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 165.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cibeber', 2, '3672013672011001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (198, 367201, '3672011002', 'Kedaleman', 941, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 264.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cibeber', 2, '3672013672011002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (199, 367202, '3672021005', 'Ciwaduk', 730, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 144.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cilegon', 2, '3672023672021005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (200, 367202, '3672021002', 'Ciwedus', 900, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 262.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cilegon', 2, '3672023672021002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (201, 367202, '3672021003', 'Bendungan', 918, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 273.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cilegon', 2, '3672023672021003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (202, 367202, '3672021004', 'Ketileng', 797, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 192.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cilegon', 2, '3672023672021004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (203, 367202, '3672021001', 'Bagendung', 1017, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 272.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Cilegon', 2, '3672023672021001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (204, 367203, '3672031004', 'Suralaya', 1050, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 216.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Pulomerak', 2, '3672033672031004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (205, 367204, '3672041003', 'Kubangsari', 1300, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 316.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Ciwandan', 2, '3672043672041003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (206, 367204, '3672041002', 'Tegal Ratu', 1572, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 314.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Ciwandan', 2, '3672043672041002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (207, 367204, '3672041006', 'Randakari', 1549, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 338.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Ciwandan', 2, '3672043672041006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (208, 367204, '3672041005', 'Kepuh', 1691, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 348.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Ciwandan', 2, '3672043672041005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (209, 367204, '3672041004', 'Gunung Sugih', 1070, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 164.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Ciwandan', 2, '3672043672041004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (210, 367205, '3672051004', 'Panggung Rawi', 1070, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 211.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, ' Jombang', 2, '3672053672051004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (211, 367205, '3672051002', 'Jombang Wetan', 1871, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 326.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, ' Jombang', 2, '3672053672051002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (212, 367205, '3672051005', 'Gedong Dalem', 904, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 213.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, ' Jombang', 2, '3672053672051005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (213, 367205, '3672051001', 'Sukmajaya', 1172, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 201.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, ' Jombang', 2, '3672053672051001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (214, 367205, '3672051003', 'Masigit', 1902, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 380.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, ' Jombang', 2, '3672053672051003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (215, 367206, '3672061004', 'Gerem', 1667, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 462.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Gerogol', 2, '3672063672061004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (216, 367206, '3672061003', 'Rawa Arum', 1560, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 318.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Gerogol', 2, '3672063672061003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (217, 367206, '3672061001', 'Kotasari', 484, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 111.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Gerogol', 2, '3672063672061001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (218, 367206, '3672061002', 'Grogol', 896, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 254.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Gerogol', 2, '3672063672061002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (219, 367207, '3672071006', 'Pabean', 702, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 178.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Purwakarta', 2, '3672073672071006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (220, 367207, '3672071005', 'Tegal Bunder', 1033, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 263.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Purwakarta', 2, '3672073672071005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (221, 367207, '3672071004', 'Purwakarta', 931, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 176.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Purwakarta', 2, '3672073672071004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (222, 367207, '3672071003', 'Kebon Dalem', 929, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 162.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Purwakarta', 2, '3672073672071003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (223, 367207, '3672071001', 'Ramanuju', 173, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 26.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Purwakarta', 2, '3672073672071001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (224, 367208, '3672081007', 'Citangkil', 1433, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 316.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Citangkil', 2, '3672083672081007', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (225, 367208, '3672081005', 'Kebonsari', 1416, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 341.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Citangkil', 2, '3672083672081005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (226, 367208, '3672081004', 'Taman Baru', 937, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 237.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Citangkil', 2, '3672083672081004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (227, 367208, '3672081001', 'Warnasari', 1052, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 198.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Citangkil', 2, '3672083672081001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (228, 367208, '3672081003', 'Lebak Denok', 1160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 241.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 17, 2023, 'Citangkil', 2, '3672083672081003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (229, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cibeber', 2, '3672013672011003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (230, 367201, '3672011004', 'Cikerai', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cibeber', 2, '3672013672011004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (231, 367201, '3672011006', 'Kalitimbang', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cibeber', 2, '3672013672011006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (232, 367201, '3672011005', 'Karang Asem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cibeber', 2, '3672013672011005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (233, 367201, '3672011001', 'Cibeber', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cibeber', 2, '3672013672011001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (234, 367201, '3672011002', 'Kedaleman', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cibeber', 2, '3672013672011002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (235, 367202, '3672021005', 'Ciwaduk', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cilegon', 2, '3672023672021005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (236, 367202, '3672021002', 'Ciwedus', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cilegon', 2, '3672023672021002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (237, 367202, '3672021003', 'Bendungan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cilegon', 2, '3672023672021003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (238, 367202, '3672021004', 'Ketileng', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cilegon', 2, '3672023672021004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (239, 367202, '3672021001', 'Bagendung', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Cilegon', 2, '3672023672021001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (240, 367203, '3672031004', 'Suralaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Pulomerak', 2, '3672033672031004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (241, 367204, '3672041003', 'Kubangsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Ciwandan', 2, '3672043672041003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (242, 367204, '3672041002', 'Tegal Ratu', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Ciwandan', 2, '3672043672041002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (243, 367204, '3672041006', 'Randakari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Ciwandan', 2, '3672043672041006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (244, 367204, '3672041005', 'Kepuh', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Ciwandan', 2, '3672043672041005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (245, 367204, '3672041004', 'Gunung Sugih', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Ciwandan', 2, '3672043672041004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (246, 367205, '3672051004', 'Panggung Rawi', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, ' Jombang', 2, '3672053672051004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (247, 367205, '3672051002', 'Jombang Wetan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, ' Jombang', 2, '3672053672051002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (248, 367205, '3672051005', 'Gedong Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, ' Jombang', 2, '3672053672051005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (249, 367205, '3672051001', 'Sukmajaya', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, ' Jombang', 2, '3672053672051001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (250, 367205, '3672051003', 'Masigit', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, ' Jombang', 2, '3672053672051003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (251, 367206, '3672061004', 'Gerem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Gerogol', 2, '3672063672061004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (252, 367206, '3672061003', 'Rawa Arum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Gerogol', 2, '3672063672061003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (253, 367206, '3672061001', 'Kotasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Gerogol', 2, '3672063672061001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (254, 367206, '3672061002', 'Grogol', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Gerogol', 2, '3672063672061002', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (255, 367207, '3672071006', 'Pabean', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Purwakarta', 2, '3672073672071006', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (256, 367207, '3672071005', 'Tegal Bunder', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Purwakarta', 2, '3672073672071005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (257, 367207, '3672071004', 'Purwakarta', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Purwakarta', 2, '3672073672071004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (258, 367207, '3672071003', 'Kebon Dalem', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Purwakarta', 2, '3672073672071003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (259, 367207, '3672071001', 'Ramanuju', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Purwakarta', 2, '3672073672071001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (260, 367208, '3672081007', 'Citangkil', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Citangkil', 2, '3672083672081007', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (261, 367208, '3672081005', 'Kebonsari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Citangkil', 2, '3672083672081005', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (262, 367208, '3672081004', 'Taman Baru', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Citangkil', 2, '3672083672081004', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (263, 367208, '3672081001', 'Warnasari', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Citangkil', 2, '3672083672081001', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (264, 367208, '3672081003', 'Lebak Denok', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 18, 2023, 'Citangkil', 2, '3672083672081003', 0, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (265, 367201, '3672011003', 'Bulakan', 391, 391, 391, 391, 391, 391, 390, 382, 382, 373, 373, 0, 39.00, 39.00, 39.00, 39.00, 39.00, 39.00, 39.00, 38.00, 38.00, 37.00, 37.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cibeber', 2, '3672013672011003', 990, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (266, 367201, '3672011004', 'Cikerai', 366, 366, 366, 366, 366, 366, 365, 359, 359, 338, 338, 0, 45.00, 45.00, 45.00, 45.00, 45.00, 45.00, 45.00, 44.00, 44.00, 42.00, 42.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cibeber', 2, '3672013672011004', 799, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (267, 367201, '3672011006', 'Kalitimbang', 294, 294, 294, 293, 293, 293, 292, 290, 290, 274, 274, 0, 29.00, 29.00, 29.00, 29.00, 29.00, 29.00, 29.00, 29.00, 29.00, 27.00, 27.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cibeber', 2, '3672013672011006', 991, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (268, 367201, '3672011005', 'Karang Asem', 613, 613, 613, 610, 610, 610, 602, 589, 589, 558, 558, 0, 40.00, 40.00, 40.00, 40.00, 40.00, 40.00, 39.00, 38.00, 38.00, 36.00, 36.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cibeber', 2, '3672013672011005', 1525, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (269, 367201, '3672011001', 'Cibeber', 184, 184, 184, 182, 182, 182, 189, 176, 176, 158, 158, 0, 19.00, 19.00, 19.00, 19.00, 19.00, 19.00, 20.00, 19.00, 19.00, 17.00, 17.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cibeber', 2, '3672013672011001', 922, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (270, 367201, '3672011002', 'Kedaleman', 280, 280, 280, 281, 281, 281, 271, 267, 267, 252, 252, 0, 29.00, 29.00, 29.00, 29.00, 29.00, 29.00, 28.00, 28.00, 28.00, 26.00, 26.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cibeber', 2, '3672013672011002', 941, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (271, 367202, '3672021005', 'Ciwaduk', 183, 183, 183, 176, 176, 176, 180, 171, 171, 168, 168, 0, 25.00, 25.00, 25.00, 24.00, 24.00, 24.00, 24.00, 23.00, 23.00, 23.00, 23.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cilegon', 2, '3672023672021005', 730, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (272, 367202, '3672021002', 'Ciwedus', 214, 214, 214, 217, 217, 217, 210, 200, 200, 192, 192, 0, 23.00, 23.00, 23.00, 24.00, 24.00, 24.00, 23.00, 22.00, 22.00, 21.00, 21.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cilegon', 2, '3672023672021002', 900, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (273, 367202, '3672021003', 'Bendungan', 317, 317, 317, 322, 322, 322, 310, 306, 306, 290, 290, 0, 34.00, 34.00, 34.00, 35.00, 35.00, 35.00, 33.00, 33.00, 33.00, 31.00, 31.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cilegon', 2, '3672023672021003', 918, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (274, 367202, '3672021004', 'Ketileng', 276, 276, 276, 275, 275, 275, 265, 265, 265, 248, 248, 0, 34.00, 34.00, 34.00, 34.00, 34.00, 34.00, 33.00, 33.00, 33.00, 31.00, 31.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cilegon', 2, '3672023672021004', 797, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (275, 367202, '3672021001', 'Bagendung', 442, 442, 442, 449, 449, 449, 435, 435, 435, 415, 415, 0, 43.00, 43.00, 43.00, 44.00, 44.00, 44.00, 42.00, 42.00, 42.00, 40.00, 40.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Cilegon', 2, '3672023672021001', 1017, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (276, 367203, '3672031004', 'Suralaya', 385, 385, 385, 388, 388, 388, 368, 368, 368, 353, 353, 0, 36.00, 36.00, 36.00, 36.00, 36.00, 36.00, 35.00, 35.00, 35.00, 33.00, 33.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Pulomerak', 2, '3672033672031004', 1050, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (277, 367204, '3672041003', 'Kubangsari', 517, 517, 517, 514, 514, 514, 500, 500, 500, 454, 454, 0, 39.00, 39.00, 39.00, 39.00, 39.00, 39.00, 38.00, 38.00, 38.00, 34.00, 34.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Ciwandan', 2, '3672043672041003', 1300, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (278, 367204, '3672041002', 'Tegal Ratu', 430, 430, 430, 433, 433, 433, 412, 412, 412, 379, 379, 0, 27.00, 27.00, 27.00, 27.00, 27.00, 27.00, 26.00, 26.00, 26.00, 24.00, 24.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Ciwandan', 2, '3672043672041002', 1572, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (279, 367204, '3672041006', 'Randakari', 494, 494, 494, 494, 494, 494, 463, 463, 463, 436, 436, 0, 31.00, 31.00, 31.00, 31.00, 31.00, 31.00, 29.00, 29.00, 29.00, 28.00, 28.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Ciwandan', 2, '3672043672041006', 1549, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (280, 367204, '3672041005', 'Kepuh', 696, 696, 696, 702, 702, 702, 672, 672, 672, 620, 620, 0, 41.00, 41.00, 41.00, 41.00, 41.00, 41.00, 39.00, 39.00, 39.00, 36.00, 36.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Ciwandan', 2, '3672043672041005', 1691, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (281, 367204, '3672041004', 'Gunung Sugih', 251, 251, 251, 254, 254, 254, 340, 340, 340, 230, 230, 0, 23.00, 23.00, 23.00, 23.00, 23.00, 23.00, 31.00, 31.00, 31.00, 21.00, 21.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Ciwandan', 2, '3672043672041004', 1070, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (282, 367205, '3672051004', 'Panggung Rawi', 215, 215, 215, 212, 212, 212, 200, 200, 200, 191, 191, 0, 20.00, 20.00, 20.00, 19.00, 19.00, 19.00, 18.00, 18.00, 18.00, 17.00, 17.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, ' Jombang', 2, '3672053672051004', 1070, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (283, 367205, '3672051002', 'Jombang Wetan', 433, 433, 433, 430, 430, 430, 412, 412, 412, 390, 390, 0, 23.00, 23.00, 23.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 20.00, 20.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, ' Jombang', 2, '3672053672051002', 1871, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (284, 367205, '3672051005', 'Gedong Dalem', 327, 327, 327, 331, 331, 331, 301, 301, 301, 293, 293, 0, 36.00, 36.00, 36.00, 36.00, 36.00, 36.00, 33.00, 33.00, 33.00, 32.00, 32.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, ' Jombang', 2, '3672053672051005', 904, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (285, 367205, '3672051001', 'Sukmajaya', 279, 279, 279, 282, 282, 282, 262, 262, 262, 248, 248, 0, 23.00, 23.00, 23.00, 24.00, 24.00, 24.00, 22.00, 22.00, 22.00, 21.00, 21.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, ' Jombang', 2, '3672053672051001', 1172, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (286, 367205, '3672051003', 'Masigit', 431, 431, 431, 428, 428, 428, 402, 402, 402, 391, 391, 0, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 21.00, 21.00, 21.00, 20.00, 20.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, ' Jombang', 2, '3672053672051003', 1902, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (287, 367206, '3672061004', 'Gerem', 587, 587, 587, 589, 589, 589, 570, 570, 570, 540, 540, 0, 35.00, 35.00, 35.00, 35.00, 35.00, 35.00, 34.00, 34.00, 34.00, 32.00, 32.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Gerogol', 2, '3672063672061004', 1667, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (288, 367206, '3672061003', 'Rawa Arum', 301, 301, 301, 301, 301, 301, 285, 285, 285, 265, 265, 0, 19.00, 19.00, 19.00, 19.00, 19.00, 19.00, 18.00, 18.00, 18.00, 16.00, 16.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Gerogol', 2, '3672063672061003', 1560, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (289, 367206, '3672061001', 'Kotasari', 150, 150, 150, 151, 151, 151, 138, 138, 138, 127, 127, 0, 30.00, 30.00, 30.00, 31.00, 31.00, 31.00, 28.00, 28.00, 28.00, 26.00, 26.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Gerogol', 2, '3672063672061001', 484, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (290, 367206, '3672061002', 'Grogol', 358, 358, 358, 357, 357, 357, 338, 338, 338, 315, 315, 0, 39.00, 39.00, 39.00, 39.00, 39.00, 39.00, 37.00, 37.00, 37.00, 35.00, 35.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Gerogol', 2, '3672063672061002', 896, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (291, 367207, '3672071006', 'Pabean', 257, 257, 257, 261, 261, 261, 239, 239, 239, 230, 230, 0, 36.00, 36.00, 36.00, 37.00, 37.00, 37.00, 34.00, 34.00, 34.00, 32.00, 32.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Purwakarta', 2, '3672073672071006', 702, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (292, 367207, '3672071005', 'Tegal Bunder', 420, 420, 420, 422, 422, 422, 404, 404, 404, 385, 385, 0, 40.00, 40.00, 40.00, 40.00, 40.00, 40.00, 39.00, 39.00, 39.00, 37.00, 37.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Purwakarta', 2, '3672073672071005', 1033, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (293, 367207, '3672071004', 'Purwakarta', 319, 319, 319, 317, 317, 317, 310, 310, 310, 292, 292, 0, 34.00, 34.00, 34.00, 34.00, 34.00, 34.00, 33.00, 33.00, 33.00, 31.00, 31.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Purwakarta', 2, '3672073672071004', 931, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (294, 367207, '3672071003', 'Kebon Dalem', 277, 277, 277, 275, 275, 275, 262, 262, 262, 248, 248, 0, 29.00, 29.00, 29.00, 29.00, 29.00, 29.00, 28.00, 28.00, 28.00, 26.00, 26.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Purwakarta', 2, '3672073672071003', 929, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (295, 367207, '3672071001', 'Ramanuju', 43, 43, 43, 43, 43, 43, 42, 42, 42, 38, 38, 0, 24.00, 24.00, 24.00, 24.00, 24.00, 24.00, 24.00, 24.00, 24.00, 21.00, 21.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Purwakarta', 2, '3672073672071001', 173, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (296, 367208, '3672081007', 'Citangkil', 340, 340, 340, 341, 341, 341, 324, 324, 324, 304, 304, 0, 23.00, 23.00, 23.00, 23.00, 23.00, 23.00, 22.00, 22.00, 22.00, 21.00, 21.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Citangkil', 2, '3672083672081007', 1433, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (297, 367208, '3672081005', 'Kebonsari', 319, 319, 319, 318, 318, 318, 311, 311, 311, 290, 290, 0, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 21.00, 21.00, 21.00, 20.00, 20.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Citangkil', 2, '3672083672081005', 1416, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (298, 367208, '3672081004', 'Taman Baru', 322, 322, 322, 318, 318, 318, 314, 314, 314, 295, 295, 0, 34.00, 34.00, 34.00, 33.00, 33.00, 33.00, 33.00, 33.00, 33.00, 31.00, 31.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Citangkil', 2, '3672083672081004', 937, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (299, 367208, '3672081001', 'Warnasari', 273, 273, 273, 278, 278, 278, 261, 261, 261, 243, 243, 0, 25.00, 25.00, 25.00, 26.00, 26.00, 26.00, 24.00, 24.00, 24.00, 23.00, 23.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Citangkil', 2, '3672083672081001', 1052, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (300, 367208, '3672081003', 'Lebak Denok', 432, 432, 432, 437, 437, 437, 416, 416, 416, 394, 394, 0, 37.00, 37.00, 37.00, 37.00, 37.00, 37.00, 35.00, 35.00, 35.00, 33.00, 33.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 19, 2023, 'Citangkil', 2, '3672083672081003', 1160, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (301, 367202, '3672021001', 'BAGENDUNG', 2, 2, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 2.00, 2.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 0.00, 0.00, 0.00, 7, 7, 9, 9, 9, 0, 9, 0, 1, 2023, 'Cilegon', 3, '3672023672021001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (302, 367202, '3672021003', 'BENDUNGAN', 2, 3, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 2.00, 3.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 0.00, 0.00, 0.00, 10, 9, 12, 12, 12, 0, 12, 0, 1, 2023, 'Cilegon', 3, '3672023672021003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (303, 367201, '3672011003', 'BULAKAN', 3, 3, 5, 5, 5, 5, 5, 5, 5, 0, 0, 0, 3.00, 3.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 5.00, 0.00, 0.00, 0.00, 11, 11, 15, 15, 15, 0, 15, 0, 1, 2023, 'Cibeber', 3, '3672013672011003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (304, 367201, '3672011001', 'CIBEBER', 0, 3, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0.00, 3.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 0.00, 0.00, 0.00, 10, 7, 12, 12, 12, 0, 12, 0, 1, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (305, 367201, '3672011004', 'CIKERAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Cibeber', 3, '3672013672011004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (306, 367208, '3672081007', 'CITANGKIL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Citangkil', 3, '3672083672081007', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (307, 367202, '3672021005', 'CIWADUK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Cilegon', 3, '3672023672021005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (308, 367202, '3672021002', 'CIWEDUS', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Cilegon', 3, '3672023672021002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (309, 367205, '3672051005', 'GEDONG DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, ' Jombang', 3, '3672053672051005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (310, 367206, '3672061004', 'GEREM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Gerogol', 3, '3672063672061004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (311, 367205, '3672051002', 'JOMBANG WETAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, ' Jombang', 3, '3672053672051002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (312, 367201, '3672011006', 'KALITIMBANG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Cibeber', 3, '3672013672011006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (313, 367207, '3672071003', 'KEBON DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Purwakarta', 3, '3672073672071003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (314, 367208, '3672081005', 'KEBONSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Citangkil', 3, '3672083672081005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (315, 367201, '3672011002', 'KEDALEMAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Cibeber', 3, '3672013672011002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (316, 367204, '3672041005', 'KEPUH', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Ciwandan', 3, '3672043672041005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (317, 367202, '3672021004', 'KETILENG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Cilegon', 3, '3672023672021004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (318, 367206, '3672061001', 'KOTASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Gerogol', 3, '3672063672061001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (319, 367204, '3672041003', 'KUBANGSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Ciwandan', 3, '3672043672041003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (320, 367205, '3672051003', 'MASIGIT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, ' Jombang', 3, '3672053672051003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (321, 367207, '3672071006', 'PABEAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Purwakarta', 3, '3672073672071006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (322, 367205, '3672051004', 'PANGGUNG RAWI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, ' Jombang', 3, '3672053672051004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (323, 367207, '3672071004', 'PURWAKARTA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (324, 367207, '3672071001', 'RAMANUJU', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Purwakarta', 3, '3672073672071001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (325, 367204, '3672041006', 'RANDAKARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Ciwandan', 3, '3672043672041006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (326, 367206, '3672061003', 'RAWA ARUM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Gerogol', 3, '3672063672061003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (327, 367208, '3672081006', 'SAMANGRAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Citangkil', 3, '3672083672081006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (328, 367205, '3672051001', 'SUKMAJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, ' Jombang', 3, '3672053672051001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (329, 367203, '3672031004', 'SURALAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Pulomerak', 3, '3672033672031004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (330, 367207, '3672071005', 'TEGAL BUNDER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Purwakarta', 3, '3672073672071005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (331, 367208, '3672081001', 'WARNASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2023, 'Citangkil', 3, '3672083672081001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (332, 367204, '3672041005', 'KEPUH', 19, 27, 27, 55, 65, 76, 87, 99, 110, 0, 0, 0, 19.00, 27.00, 27.00, 55.00, 65.00, 76.00, 76.00, 88.00, 99.00, 0.00, 0.00, 0.00, 81, 73, 196, 196, 296, 0, 263, 0, 2, 2023, 'Ciwandan', 3, '3672043672041005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (333, 367204, '3672041006', 'RANDAKARI', 18, 37, 37, 71, 88, 104, 123, 136, 150, 0, 0, 0, 18.00, 37.00, 37.00, 71.00, 88.00, 104.00, 115.00, 128.00, 142.00, 0.00, 0.00, 0.00, 111, 92, 263, 263, 409, 0, 385, 0, 2, 2023, 'Ciwandan', 3, '3672043672041006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (334, 367204, '3672041003', 'KUBANGSARI', 22, 25, 25, 52, 65, 85, 95, 103, 115, 0, 0, 0, 22.00, 25.00, 25.00, 52.00, 65.00, 85.00, 91.00, 99.00, 111.00, 0.00, 0.00, 0.00, 75, 72, 202, 202, 313, 0, 301, 0, 2, 2023, 'Ciwandan', 3, '3672043672041003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (335, 367203, '3672031004', 'SURALAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Pulomerak', 3, '3672033672031004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (336, 367207, '3672071001', 'RAMANUJU', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Purwakarta', 3, '3672073672071001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (337, 367207, '3672071003', 'KEBON DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Purwakarta', 3, '3672073672071003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (338, 367207, '3672071004', 'PURWAKARTA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (339, 367207, '3672071005', 'TEGAL BUNDER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Purwakarta', 3, '3672073672071005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (340, 367207, '3672071006', 'PABEAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Purwakarta', 3, '3672073672071006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (341, 367206, '3672061003', 'RAWA ARUM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Gerogol', 3, '3672063672061003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (342, 367206, '3672061004', 'GEREM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Gerogol', 3, '3672063672061004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (343, 367206, '3672061001', 'KOTASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Gerogol', 3, '3672063672061001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (344, 367202, '3672021001', 'BAGENDUNG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cilegon', 3, '3672023672021001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (345, 367202, '3672021002', 'CIWEDUS', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cilegon', 3, '3672023672021002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (346, 367202, '3672021003', 'BENDUNGAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cilegon', 3, '3672023672021003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (347, 367202, '3672021005', 'CIWADUK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cilegon', 3, '3672023672021005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (348, 367202, '3672021004', 'KETILENG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cilegon', 3, '3672023672021004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (349, 367205, '3672051003', 'MASIGIT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, ' Jombang', 3, '3672053672051003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (350, 367205, '3672051004', 'PANGGUNG RAWI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, ' Jombang', 3, '3672053672051004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (351, 367205, '3672051005', 'GEDONG DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, ' Jombang', 3, '3672053672051005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (352, 367205, '3672051001', 'SUKMAJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, ' Jombang', 3, '3672053672051001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (353, 367205, '3672051002', 'JOMBANG WETAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, ' Jombang', 3, '3672053672051002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (354, 367201, '3672011001', 'CIBEBER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (355, 367201, '3672011002', 'KEDALEMAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cibeber', 3, '3672013672011002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (356, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cibeber', 3, '3672013672011003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (357, 367201, '3672011004', 'CIKERAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cibeber', 3, '3672013672011004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (358, 367201, '3672011006', 'KALITIMBANG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Cibeber', 3, '3672013672011006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (359, 367208, '3672081007', 'CITANGKIL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Citangkil', 3, '3672083672081007', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (360, 367208, '3672081005', 'KEBONSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Citangkil', 3, '3672083672081005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (361, 367208, '3672081001', 'WARNASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Citangkil', 3, '3672083672081001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (362, 367208, '3672081006', 'SAMANGRAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2023, 'Citangkil', 3, '3672083672081006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (363, 367207, '3672071004', 'PURWAKARTA', 0, 0, 2384, 0, 0, 2384, 0, 0, 2384, 0, 0, 0, 0.00, 0.00, 999.99, 0.00, 0.00, 999.99, 0.00, 0.00, 999.99, 0.00, 0.00, 0.00, 2384, 1806, 2384, 1906, 0, NULL, 0, NULL, 3, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (364, 367206, '3672061002', 'GROGOL', 0, 0, 2998, 0, 0, 2998, 0, 0, 2998, 0, 0, 0, 0.00, 0.00, 999.99, 0.00, 0.00, 999.99, 0.00, 0.00, 999.99, 0.00, 0.00, 0.00, 2998, 1684, 2998, 1684, 0, NULL, 0, NULL, 3, 2023, 'Gerogol', 3, '3672063672061002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (365, 367201, '3672011001', 'CIBEBER', 0, 0, 5216, 0, 0, 5216, 0, 0, 5216, 0, 0, 0, 0.00, 0.00, 999.99, 0.00, 0.00, 999.99, 0.00, 0.00, 999.99, 0.00, 0.00, 0.00, 5216, 5124, 5216, 5124, 0, NULL, 0, NULL, 3, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (366, 367204, NULL, NULL, 0, 47, 0, 0, 0, 0, 0, 171, 0, 0, 0, 0, 0.00, 19.00, 0.00, 0.00, 0.00, 0.00, 0.00, 131.00, 0.00, 0.00, 0.00, 0.00, 47, 19, 171, 131, 0, NULL, 0, NULL, 4, 2023, 'Ciwandan', 3, '367204', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (367, 367208, NULL, NULL, 0, 15, 0, 0, 0, 0, 0, 126, 0, 0, 0, 0, 0.00, 15.00, 0.00, 0.00, 0.00, 0.00, 0.00, 110.00, 0.00, 0.00, 0.00, 0.00, 15, 15, 126, 110, 0, NULL, 0, NULL, 4, 2023, 'Citangkil', 3, '367208', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (368, 321401, NULL, NULL, 0, 543, 0, 0, 0, 0, 0, 543, 0, 0, 0, 0, 0.00, 290.00, 0.00, 0.00, 0.00, 0.00, 0.00, 290.00, 0.00, 0.00, 0.00, 0.00, 543, 290, 543, 290, 0, NULL, 0, NULL, 4, 2023, 'Purwakarta', 3, '321401', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (369, 331109, NULL, NULL, 0, 42, 0, 0, 0, 0, 0, 33, 0, 0, 0, 0, 0.00, 24.00, 0.00, 0.00, 0.00, 0.00, 0.00, 16.00, 0.00, 0.00, 0.00, 0.00, 42, 24, 33, 16, 0, NULL, 0, NULL, 4, 2023, 'Grogol', 3, '331109', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (370, 367202, NULL, NULL, 0, 324, 0, 0, 0, 0, 0, 856, 0, 0, 0, 0, 0.00, 166.00, 0.00, 0.00, 0.00, 0.00, 0.00, 280.00, 0.00, 0.00, 0.00, 0.00, 324, 166, 856, 280, 0, NULL, 0, NULL, 4, 2023, 'Cilegon', 3, '367202', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (371, 350901, NULL, NULL, 0, 493, 0, 0, 0, 0, 0, 101, 0, 0, 0, 0, 0.00, 333.00, 0.00, 0.00, 0.00, 0.00, 0.00, 74.00, 0.00, 0.00, 0.00, 0.00, 493, 333, 101, 74, 0, NULL, 0, NULL, 4, 2023, 'Jombang', 3, '350901', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (372, 320303, NULL, NULL, 0, 382, 0, 0, 0, 0, 0, 332, 0, 0, 0, 0, 0.00, 325.00, 0.00, 0.00, 0.00, 0.00, 0.00, 288.00, 0.00, 0.00, 0.00, 0.00, 382, 325, 332, 288, 0, NULL, 0, NULL, 4, 2023, 'Cibeber', 3, '320303', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (373, 367204, '3672041005', 'KEPUH', 145, 203, 201, 190, 197, 174, 174, 104, 174, 0, 0, 0, 145.00, 203.00, 201.00, 190.00, 197.00, 174.00, 174.00, 104.00, 174.00, 0.00, 0.00, 0.00, 607, 549, 561, 561, 452, 0, 452, 0, 5, 2023, 'Ciwandan', 3, '3672043672041005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (374, 367204, '3672041006', 'RANDAKARI', 127, 170, 155, 175, 177, 148, 148, 161, 148, 0, 0, 0, 127.00, 170.00, 155.00, 175.00, 177.00, 148.00, 148.00, 161.00, 148.00, 0.00, 0.00, 0.00, 495, 452, 500, 500, 457, 0, 457, 0, 5, 2023, 'Ciwandan', 3, '3672043672041006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (375, 367204, '3672041003', 'KUBANGSARI', 94, 172, 186, 188, 186, 170, 170, 176, 170, 0, 0, 0, 94.00, 172.00, 186.00, 188.00, 186.00, 170.00, 170.00, 176.00, 170.00, 0.00, 0.00, 0.00, 530, 452, 544, 544, 516, 0, 516, 0, 5, 2023, 'Ciwandan', 3, '3672043672041003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (376, 367208, '3672081007', 'CITANGKIL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Citangkil', 3, '3672083672081007', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (377, 367208, '3672081005', 'KEBONSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Citangkil', 3, '3672083672081005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (378, 367203, '3672031004', 'SURALAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Pulomerak', 3, '3672033672031004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (379, 367207, '3672071001', 'RAMANUJU', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Purwakarta', 3, '3672073672071001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (380, 367207, '3672071003', 'KEBON DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Purwakarta', 3, '3672073672071003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (381, 367207, '3672071004', 'PURWAKARTA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (382, 367207, '3672071005', 'TEGAL BUNDER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Purwakarta', 3, '3672073672071005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (383, 367207, '3672071006', 'PABEAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Purwakarta', 3, '3672073672071006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (384, 367206, '3672061003', 'RAWA ARUM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Gerogol', 3, '3672063672061003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (385, 367206, '3672061004', 'GEREM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Gerogol', 3, '3672063672061004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (386, 367206, '3672061001', 'KOTASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Gerogol', 3, '3672063672061001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (387, 367202, '3672021001', 'BAGENDUNG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cilegon', 3, '3672023672021001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (388, 367202, '3672021002', 'CIWEDUS', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cilegon', 3, '3672023672021002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (389, 367202, '3672021003', 'BENDUNGAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cilegon', 3, '3672023672021003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (390, 367202, '3672021005', 'CIWADUK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cilegon', 3, '3672023672021005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (391, 367202, '3672021004', 'KETILENG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cilegon', 3, '3672023672021004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (392, 367205, '3672051003', 'MASIGIT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, ' Jombang', 3, '3672053672051003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (393, 367205, '3672051004', 'PANGGUNG RAWI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, ' Jombang', 3, '3672053672051004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (394, 367205, '3672051005', 'GEDONG DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, ' Jombang', 3, '3672053672051005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (395, 367205, '3672051001', 'SUKMAJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, ' Jombang', 3, '3672053672051001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (396, 367205, '3672051002', 'JOMBANG WETAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, ' Jombang', 3, '3672053672051002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (397, 367201, '3672011001', 'CIBEBER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (398, 367201, '3672011002', 'KEDALEMAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cibeber', 3, '3672013672011002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (399, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cibeber', 3, '3672013672011003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (400, 367201, '3672011004', 'CIKERAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cibeber', 3, '3672013672011004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (401, 367201, '3672011006', 'KALITIMBANG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Cibeber', 3, '3672013672011006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (402, 367208, '3672081001', 'WARNASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Citangkil', 3, '3672083672081001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (403, 367208, '3672081006', 'SAMANGRAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 5, 2023, 'Citangkil', 3, '3672083672081006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (404, 367204, '3672041005', 'KEPUH', 19, 27, 27, 55, 65, 76, 87, 99, 110, 0, 0, 0, 19.00, 27.00, 27.00, 55.00, 65.00, 76.00, 76.00, 88.00, 99.00, 0.00, 0.00, 0.00, 81, 73, 196, 196, 296, 0, 263, 0, 6, 2023, 'Ciwandan', 3, '3672043672041005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (405, 367204, '3672041006', 'RANDAKARI', 18, 37, 37, 71, 88, 104, 123, 136, 150, 0, 0, 0, 18.00, 37.00, 37.00, 71.00, 88.00, 104.00, 115.00, 128.00, 142.00, 0.00, 0.00, 0.00, 111, 92, 263, 263, 409, 0, 385, 0, 6, 2023, 'Ciwandan', 3, '3672043672041006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (406, 367204, '3672041003', 'KUBANGSARI', 22, 25, 25, 52, 65, 85, 95, 103, 115, 0, 0, 0, 22.00, 25.00, 25.00, 52.00, 65.00, 85.00, 91.00, 99.00, 111.00, 0.00, 0.00, 0.00, 75, 72, 202, 202, 313, 0, 301, 0, 6, 2023, 'Ciwandan', 3, '3672043672041003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (407, 367208, '3672081007', 'CITANGKIL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Citangkil', 3, '3672083672081007', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (408, 367208, '3672081005', 'KEBONSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Citangkil', 3, '3672083672081005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (409, 367203, '3672031004', 'SURALAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Pulomerak', 3, '3672033672031004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (410, 367207, '3672071001', 'RAMANUJU', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Purwakarta', 3, '3672073672071001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (411, 367207, '3672071003', 'KEBON DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Purwakarta', 3, '3672073672071003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (412, 367207, '3672071004', 'PURWAKARTA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (413, 367207, '3672071005', 'TEGAL BUNDER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Purwakarta', 3, '3672073672071005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (414, 367207, '3672071006', 'PABEAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Purwakarta', 3, '3672073672071006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (415, 367206, '3672061003', 'RAWA ARUM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Gerogol', 3, '3672063672061003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (416, 367206, '3672061004', 'GEREM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Gerogol', 3, '3672063672061004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (417, 367206, '3672061001', 'KOTASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Gerogol', 3, '3672063672061001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (418, 367202, '3672021001', 'BAGENDUNG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cilegon', 3, '3672023672021001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (419, 367202, '3672021002', 'CIWEDUS', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cilegon', 3, '3672023672021002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (420, 367202, '3672021003', 'BENDUNGAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cilegon', 3, '3672023672021003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (421, 367202, '3672021005', 'CIWADUK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cilegon', 3, '3672023672021005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (422, 367202, '3672021004', 'KETILENG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cilegon', 3, '3672023672021004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (423, 367205, '3672051003', 'MASIGIT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, ' Jombang', 3, '3672053672051003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (424, 367205, '3672051004', 'PANGGUNG RAWI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, ' Jombang', 3, '3672053672051004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (425, 367205, '3672051005', 'GEDONG DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, ' Jombang', 3, '3672053672051005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (426, 367205, '3672051001', 'SUKMAJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, ' Jombang', 3, '3672053672051001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (427, 367205, '3672051002', 'JOMBANG WETAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, ' Jombang', 3, '3672053672051002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (428, 367201, '3672011001', 'CIBEBER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (429, 367201, '3672011002', 'KEDALEMAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cibeber', 3, '3672013672011002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (430, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cibeber', 3, '3672013672011003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (431, 367201, '3672011004', 'CIKERAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cibeber', 3, '3672013672011004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (432, 367201, '3672011006', 'KALITIMBANG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Cibeber', 3, '3672013672011006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (433, 367208, '3672081001', 'WARNASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Citangkil', 3, '3672083672081001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (434, 367208, '3672081006', 'SAMANGRAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 6, 2023, 'Citangkil', 3, '3672083672081006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (435, 367204, '3672041005', 'KEPUH', 736, 736, 736, 736, 736, 721, 708, 703, 700, 0, 0, 0, 592.00, 647.00, 647.00, 697.00, 703.00, 696.00, 693.00, 691.00, 687.00, 0.00, 0.00, 0.00, 2208, 1886, 2193, 2096, 2111, 0, 2071, 0, 7, 2023, 'Ciwandan', 3, '3672043672041005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (436, 367204, '3672041006', 'RANDAKARI', 774, 774, 774, 774, 774, 673, 670, 665, 658, 0, 0, 0, 579.00, 614.00, 614.00, 606.00, 661.00, 645.00, 656.00, 651.00, 645.00, 0.00, 0.00, 0.00, 2322, 1807, 2221, 1912, 1993, 0, 1952, 0, 7, 2023, 'Ciwandan', 3, '3672043672041006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (437, 367204, '3672041003', 'KUBANGSARI', 660, 660, 660, 660, 660, 610, 600, 617, 617, 0, 0, 0, 343.00, 575.00, 575.00, 596.00, 595.00, 561.00, 590.00, 606.00, 596.00, 0.00, 0.00, 0.00, 1980, 1493, 1930, 1752, 1834, 0, 1792, 0, 7, 2023, 'Ciwandan', 3, '3672043672041003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (438, 367208, '3672081007', 'CITANGKIL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Citangkil', 3, '3672083672081007', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (439, 367208, '3672081005', 'KEBONSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Citangkil', 3, '3672083672081005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (440, 367203, '3672031004', 'SURALAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Pulomerak', 3, '3672033672031004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (441, 367207, '3672071001', 'RAMANUJU', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Purwakarta', 3, '3672073672071001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (442, 367207, '3672071003', 'KEBON DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Purwakarta', 3, '3672073672071003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (443, 367207, '3672071004', 'PURWAKARTA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (444, 367207, '3672071005', 'TEGAL BUNDER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Purwakarta', 3, '3672073672071005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (445, 367207, '3672071006', 'PABEAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Purwakarta', 3, '3672073672071006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (446, 367206, '3672061003', 'RAWA ARUM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Gerogol', 3, '3672063672061003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (447, 367206, '3672061004', 'GEREM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Gerogol', 3, '3672063672061004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (448, 367206, '3672061001', 'KOTASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Gerogol', 3, '3672063672061001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (449, 367202, '3672021001', 'BAGENDUNG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cilegon', 3, '3672023672021001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (450, 367202, '3672021002', 'CIWEDUS', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cilegon', 3, '3672023672021002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (451, 367202, '3672021003', 'BENDUNGAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cilegon', 3, '3672023672021003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (452, 367202, '3672021005', 'CIWADUK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cilegon', 3, '3672023672021005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (453, 367202, '3672021004', 'KETILENG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cilegon', 3, '3672023672021004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (454, 367205, '3672051003', 'MASIGIT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, ' Jombang', 3, '3672053672051003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (455, 367205, '3672051004', 'PANGGUNG RAWI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, ' Jombang', 3, '3672053672051004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (456, 367205, '3672051005', 'GEDONG DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, ' Jombang', 3, '3672053672051005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (457, 367205, '3672051001', 'SUKMAJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, ' Jombang', 3, '3672053672051001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (458, 367205, '3672051002', 'JOMBANG WETAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, ' Jombang', 3, '3672053672051002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (459, 367201, '3672011001', 'CIBEBER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (460, 367201, '3672011002', 'KEDALEMAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cibeber', 3, '3672013672011002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (461, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cibeber', 3, '3672013672011003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (462, 367201, '3672011004', 'CIKERAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cibeber', 3, '3672013672011004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (463, 367201, '3672011006', 'KALITIMBANG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Cibeber', 3, '3672013672011006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (464, 367208, '3672081001', 'WARNASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Citangkil', 3, '3672083672081001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (465, 367208, '3672081006', 'SAMANGRAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 7, 2023, 'Citangkil', 3, '3672083672081006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (466, 367204, '3672041005', 'KEPUH', 3, 3, 3, 3, 3, 3, 3, 3, 7, 0, 0, 0, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 3.00, 7.00, 0.00, 0.00, 0.00, 9, 9, 9, 9, 13, 0, 13, 0, 8, 2023, 'Ciwandan', 3, '3672043672041005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (467, 367204, '3672041006', 'RANDAKARI', 3, 4, 4, 4, 4, 4, 4, 4, 2, 0, 0, 0, 3.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 4.00, 2.00, 0.00, 0.00, 0.00, 12, 11, 12, 12, 10, 0, 10, 0, 8, 2023, 'Ciwandan', 3, '3672043672041006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (468, 367204, '3672041003', 'KUBANGSARI', 3, 6, 6, 6, 6, 6, 6, 6, 9, 0, 0, 0, 3.00, 6.00, 6.00, 6.00, 6.00, 6.00, 6.00, 6.00, 9.00, 0.00, 0.00, 0.00, 18, 15, 18, 18, 21, 0, 21, 0, 8, 2023, 'Ciwandan', 3, '3672043672041003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (469, 367208, '3672081007', 'CITANGKIL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Citangkil', 3, '3672083672081007', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (470, 367208, '3672081005', 'KEBONSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Citangkil', 3, '3672083672081005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (471, 367203, '3672031004', 'SURALAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Pulomerak', 3, '3672033672031004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (472, 367207, '3672071001', 'RAMANUJU', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Purwakarta', 3, '3672073672071001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (473, 367207, '3672071003', 'KEBON DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Purwakarta', 3, '3672073672071003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (474, 367207, '3672071004', 'PURWAKARTA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (475, 367207, '3672071005', 'TEGAL BUNDER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Purwakarta', 3, '3672073672071005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (476, 367207, '3672071006', 'PABEAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Purwakarta', 3, '3672073672071006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (477, 367206, '3672061003', 'RAWA ARUM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Gerogol', 3, '3672063672061003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (478, 367206, '3672061004', 'GEREM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Gerogol', 3, '3672063672061004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (479, 367206, '3672061001', 'KOTASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Gerogol', 3, '3672063672061001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (480, 367202, '3672021001', 'BAGENDUNG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cilegon', 3, '3672023672021001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (481, 367202, '3672021002', 'CIWEDUS', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cilegon', 3, '3672023672021002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (482, 367202, '3672021003', 'BENDUNGAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cilegon', 3, '3672023672021003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (483, 367202, '3672021005', 'CIWADUK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cilegon', 3, '3672023672021005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (484, 367202, '3672021004', 'KETILENG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cilegon', 3, '3672023672021004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (485, 367205, '3672051003', 'MASIGIT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, ' Jombang', 3, '3672053672051003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (486, 367205, '3672051004', 'PANGGUNG RAWI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, ' Jombang', 3, '3672053672051004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (487, 367205, '3672051005', 'GEDONG DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, ' Jombang', 3, '3672053672051005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (488, 367205, '3672051001', 'SUKMAJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, ' Jombang', 3, '3672053672051001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (489, 367205, '3672051002', 'JOMBANG WETAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, ' Jombang', 3, '3672053672051002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (490, 367201, '3672011001', 'CIBEBER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (491, 367201, '3672011002', 'KEDALEMAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cibeber', 3, '3672013672011002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (492, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cibeber', 3, '3672013672011003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (493, 367201, '3672011004', 'CIKERAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cibeber', 3, '3672013672011004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (494, 367201, '3672011006', 'KALITIMBANG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Cibeber', 3, '3672013672011006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (495, 367208, '3672081001', 'WARNASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Citangkil', 3, '3672083672081001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (496, 367208, '3672081006', 'SAMANGRAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 8, 2023, 'Citangkil', 3, '3672083672081006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (497, 367204, '3672041005', 'KEPUH', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Ciwandan', 3, '3672043672041005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (498, 367204, '3672041006', 'RANDAKARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Ciwandan', 3, '3672043672041006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (499, 367204, '3672041003', 'KUBANGSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Ciwandan', 3, '3672043672041003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (500, 367208, '3672081007', 'CITANGKIL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Citangkil', 3, '3672083672081007', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (501, 367208, '3672081005', 'KEBONSARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Citangkil', 3, '3672083672081005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (502, 367203, '3672031004', 'SURALAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Pulomerak', 3, '3672033672031004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (503, 367207, '3672071001', 'RAMANUJU', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Purwakarta', 3, '3672073672071001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (504, 367207, '3672071003', 'KEBON DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Purwakarta', 3, '3672073672071003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (505, 367207, '3672071004', 'PURWAKARTA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Purwakarta', 3, '3672073672071004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (506, 367207, '3672071005', 'TEGAL BUNDER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Purwakarta', 3, '3672073672071005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (507, 367207, '3672071006', 'PABEAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Purwakarta', 3, '3672073672071006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (508, 367206, '3672061003', 'RAWA ARUM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Gerogol', 3, '3672063672061003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (509, 367206, '3672061004', 'GEREM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Gerogol', 3, '3672063672061004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (510, 367206, '3672061001', 'KOTASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Gerogol', 3, '3672063672061001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (511, 367202, '3672021001', 'BAGENDUNG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cilegon', 3, '3672023672021001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (512, 367202, '3672021002', 'CIWEDUS', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cilegon', 3, '3672023672021002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (513, 367202, '3672021003', 'BENDUNGAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cilegon', 3, '3672023672021003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (514, 367202, '3672021005', 'CIWADUK', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cilegon', 3, '3672023672021005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (515, 367202, '3672021004', 'KETILENG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cilegon', 3, '3672023672021004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (516, 367205, '3672051003', 'MASIGIT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, ' Jombang', 3, '3672053672051003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (517, 367205, '3672051004', 'PANGGUNG RAWI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, ' Jombang', 3, '3672053672051004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (518, 367205, '3672051005', 'GEDONG DALEM', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, ' Jombang', 3, '3672053672051005', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (519, 367205, '3672051001', 'SUKMAJAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, ' Jombang', 3, '3672053672051001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (520, 367205, '3672051002', 'JOMBANG WETAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, ' Jombang', 3, '3672053672051002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (521, 367201, '3672011001', 'CIBEBER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cibeber', 3, '3672013672011001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (522, 367201, '3672011002', 'KEDALEMAN', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cibeber', 3, '3672013672011002', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (523, 367201, '3672011003', 'Bulakan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cibeber', 3, '3672013672011003', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (524, 367201, '3672011004', 'CIKERAI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cibeber', 3, '3672013672011004', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (525, 367201, '3672011006', 'KALITIMBANG', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Cibeber', 3, '3672013672011006', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (526, 367208, '3672081001', 'WARNASARI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Citangkil', 3, '3672083672081001', NULL, NULL);
INSERT INTO `t_gizi_spesifikasi` VALUES (527, 367208, '3672081006', 'SAMANGRAYA', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 9, 2023, 'Citangkil', 3, '3672083672081006', NULL, NULL);

-- ----------------------------
-- Table structure for t_gizi_warga
-- ----------------------------
DROP TABLE IF EXISTS `t_gizi_warga`;
CREATE TABLE `t_gizi_warga`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kelurahan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelurahan_id` bigint(30) NULL DEFAULT NULL,
  `kecamatan_id` bigint(30) NULL DEFAULT NULL,
  `SKL23` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Laki-laki usia 0-23 bulan',
  `SKP23` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Perempuan usia 0-23 bulan',
  `SKL59` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Laki-laki usia 0-23 bulan',
  `SKP59` int(8) NULL DEFAULT NULL COMMENT 'Sangat Kurang Perempuan usia 0-59 bulan',
  `KRL23` int(8) NULL DEFAULT NULL COMMENT 'Kurang Laki-laki usia 0-23 bulan',
  `KRP23` int(8) NULL DEFAULT NULL COMMENT 'Kurang Perempuan usia 0-23 bulan',
  `KRL59` int(8) NULL DEFAULT NULL COMMENT 'Kurang Laki-laki usia 0-59 bulan',
  `KRP59` int(8) NULL DEFAULT NULL COMMENT '\'Kurang Perempuan usia 0-59 bulan\'',
  `BBNL23` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Laki-laki usia 0-23 bulan\'',
  `BBNP23` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Perempuan usia 0-23 bulan\'',
  `BBNL59` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Laki-laki usia 0-59 bulan\'',
  `BBNP59` int(8) NULL DEFAULT NULL COMMENT '\'Berat Badan Normal Perempuan usia 0-593 bulan\'',
  `RSL23` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Lebih Laki-laki usia 0-23 bulan\'',
  `RSP23` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Lebih Perempuan usia 0-23 bulan\'',
  `RSL59` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Lebih Laki-laki usia 0-59 bulan\'',
  `RSP59` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Lebih Perempuan usia 0-593 bulan\'',
  `SPL23` int(8) NULL DEFAULT NULL COMMENT '\'Sangat Pendek Laki-laki usia 0-23 bulan\'',
  `SPP23` int(8) NULL DEFAULT NULL COMMENT '\'Sangat Pendek Perempuan usia 0-23 bulan\'',
  `SPL59` int(8) NULL DEFAULT NULL COMMENT '\'Sangat Pendek Laki-laki usia 0-59 bulan\'',
  `SPP59` int(8) NULL DEFAULT NULL COMMENT '\'Sangat Pendek Perempuan usia 0-593 bulan\'',
  `PDL23` int(8) NULL DEFAULT NULL COMMENT '\'Pendek Laki-laki usia 0-23 bulan\'',
  `PDP23` int(8) NULL DEFAULT NULL COMMENT '\'Pendek Perempuan usia 0-23 bulan\'',
  `PDL59` int(8) NULL DEFAULT NULL COMMENT '\'Pendek Laki-laki usia 0-59 bulan\'',
  `PDP59` int(8) NULL DEFAULT NULL COMMENT '\'Pendek Perempuan usia 0-593 bulan\'',
  `NRL23` int(8) NULL DEFAULT NULL COMMENT '\'Normal Laki-laki usia 0-23 bulan\'',
  `NRP23` int(8) NULL DEFAULT NULL COMMENT '\'Normal Perempuan usia 0-23 bulan\'',
  `NRL59` int(8) NULL DEFAULT NULL COMMENT '\'Normal Laki-laki usia 0-59 bulan\'',
  `NRP59` int(8) NULL DEFAULT NULL COMMENT '\'Normal Perempuan usia 0-593 bulan\'',
  `TGL23` int(8) NULL DEFAULT NULL COMMENT '\'Tinggi Laki-laki usia 0-23 bulan\'',
  `TGP23` int(8) NULL DEFAULT NULL COMMENT '\'Tinggi Perempuan usia 0-23 bulan\'',
  `TGL59` int(8) NULL DEFAULT NULL COMMENT '\'Tinggi Laki-laki usia 0-59 bulan\'',
  `TGP59` int(8) NULL DEFAULT NULL COMMENT '\'Tinggi Perempuan usia 0-593 bulan\'',
  `GBL23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Buruk Laki-laki usia 0-23 bulan\'',
  `GBP23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Buruk Perempuan usia 0-23 bulan\'',
  `GBL59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Buruk Laki-laki usia 0-59 bulan\'',
  `GBP59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Buruk Perempuan usia 0-593 bulan\'',
  `GKL23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Kurang Laki-laki usia 0-23 bulan\'',
  `GKP23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Kurang Perempuan usia 0-23 bulan\'',
  `GKL59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Kurang Laki-laki usia 0-59 bulan\'',
  `GKP59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Kurang Perempuan usia 0-593 bulan\'',
  `GNL23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Normal Laki-laki usia 0-23 bulan\'',
  `GNP23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Normal Perempuan usia 0-23 bulan\'',
  `GNL59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Normal Laki-laki usia 0-59 bulan\'',
  `GNP59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Normal Perempuan usia 0-593 bulan\'',
  `RGLL23` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Gizi Lebih Laki-laki usia 0-23 bulan\'',
  `RGLP23` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Gizi Lebih Perempuan usia 0-23 bulan\'',
  `RGLL59` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Gizi Lebih Laki-laki usia 0-59 bulan\'',
  `RGLP59` int(8) NULL DEFAULT NULL COMMENT '\'Resiko Gizi Lebih Perempuan usia 0-593 bulan\'',
  `GLL23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Lebih Laki-laki usia 0-23 bulan\'',
  `GLP23` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Lebih Perempuan usia 0-23 bulan\'',
  `GLL59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Lebih Laki-laki usia 0-59 bulan\'',
  `GLP59` int(8) NULL DEFAULT NULL COMMENT '\'Gizi Lebih Perempuan usia 0-593 bulan\'',
  `OBL23` int(8) NULL DEFAULT NULL COMMENT '\'Obesitas Laki-laki usia 0-23 bulan\'',
  `OBP23` int(8) NULL DEFAULT NULL COMMENT '\'Obesitas Perempuan usia 0-23 bulan\'',
  `OBL59` int(8) NULL DEFAULT NULL COMMENT '\'Obesitas Laki-laki usia 0-59 bulan\'',
  `OBP59` int(8) NULL DEFAULT NULL COMMENT '\'Obesitas Perempuan usia 0-593 bulan\'',
  `bulan` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun` int(5) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `keyid` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_gizi_warga
-- ----------------------------
INSERT INTO `t_gizi_warga` VALUES (1, 'BULAKAN', 3672011003, 367201, 2, 0, 3, 4, 2, 2, 5, 6, 88, 86, 244, 217, 4, 3, 5, 3, 2, 0, 5, 2, 1, 4, 5, 10, 93, 87, 247, 218, 0, 0, 0, 0, 0, 0, 1, 1, 5, 0, 6, 3, 87, 82, 238, 208, 4, 9, 12, 17, 0, 0, 0, 1, 0, 0, 0, 0, NULL, 2023, '2023-11-17 19:51:59', 4);
INSERT INTO `t_gizi_warga` VALUES (2, 'CIKERAI', 3672011004, 367201, 0, 0, 4, 1, 2, 3, 10, 12, 79, 56, 214, 174, 7, 4, 10, 6, 1, 1, 5, 3, 0, 1, 7, 8, 87, 61, 226, 182, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 4, 1, 77, 53, 217, 178, 7, 7, 11, 9, 3, 3, 5, 5, 0, 0, 0, 0, NULL, 2023, '2023-11-17 19:51:59', 4);
INSERT INTO `t_gizi_warga` VALUES (3, 'KALITIMBANG', 3672011006, 367201, 0, 0, 2, 0, 2, 3, 6, 8, 95, 74, 270, 211, 2, 1, 3, 2, 0, 0, 2, 0, 3, 1, 6, 5, 95, 77, 271, 215, 1, 0, 2, 1, 0, 0, 2, 0, 1, 2, 4, 5, 92, 74, 264, 206, 6, 2, 10, 9, 0, 0, 1, 1, 0, 0, 0, 0, NULL, 2023, '2023-11-17 19:51:59', 4);
INSERT INTO `t_gizi_warga` VALUES (4, 'CIBEBER', 3672011001, 367201, 0, 0, 0, 3, 2, 1, 4, 5, 132, 125, 555, 481, 4, 8, 12, 16, 0, 0, 1, 1, 2, 2, 6, 7, 136, 130, 564, 494, 0, 2, 0, 3, 0, 0, 0, 1, 0, 0, 2, 3, 126, 124, 536, 469, 10, 7, 27, 21, 2, 3, 4, 8, 0, 0, 2, 2, NULL, 2023, '2023-11-17 19:51:59', 4);
INSERT INTO `t_gizi_warga` VALUES (5, 'KEDALEMAN', 3672011002, 367201, 2, 1, 6, 8, 3, 1, 11, 9, 127, 122, 341, 316, 4, 4, 11, 10, 2, 1, 8, 5, 6, 1, 11, 11, 128, 125, 350, 326, 0, 1, 0, 1, 2, 1, 2, 2, 1, 1, 7, 10, 122, 111, 338, 305, 10, 11, 13, 16, 1, 3, 7, 6, 0, 1, 2, 4, NULL, 2023, '2023-11-17 19:51:59', 4);
INSERT INTO `t_gizi_warga` VALUES (6, 'JOMBANG WETAN', 3672051002, 367205, 1, 2, 4, 6, 2, 3, 19, 16, 191, 169, 550, 458, 4, 4, 23, 10, 2, 1, 8, 4, 10, 5, 17, 14, 183, 172, 566, 471, 3, 0, 5, 1, 0, 0, 2, 2, 4, 3, 23, 13, 177, 155, 539, 443, 13, 14, 18, 23, 4, 6, 7, 8, 0, 0, 6, 1, NULL, 2023, '2023-11-20 15:47:36', 4);
INSERT INTO `t_gizi_warga` VALUES (7, 'MASIGIT', 3672051003, 367205, 3, 2, 5, 2, 12, 11, 28, 32, 171, 161, 531, 453, 12, 14, 25, 26, 2, 1, 4, 2, 13, 8, 27, 20, 182, 174, 554, 483, 1, 5, 4, 8, 1, 1, 1, 2, 6, 2, 15, 14, 178, 166, 548, 462, 8, 17, 14, 27, 4, 2, 8, 4, 1, 0, 3, 4, NULL, 2023, '2023-11-20 15:47:36', 4);
INSERT INTO `t_gizi_warga` VALUES (8, 'PANGGUNG RAWI', 3672051004, 367205, 2, 1, 3, 1, 3, 4, 8, 11, 119, 101, 318, 266, 11, 5, 26, 21, 1, 1, 1, 1, 0, 3, 1, 4, 131, 106, 345, 292, 3, 1, 8, 2, 1, 0, 3, 0, 5, 2, 10, 15, 114, 99, 311, 254, 9, 9, 18, 22, 6, 1, 11, 6, 0, 0, 2, 2, NULL, 2023, '2023-11-20 15:47:36', 4);
INSERT INTO `t_gizi_warga` VALUES (9, 'GEDONG DALEM', 3672051005, 367205, 0, 1, 0, 1, 0, 1, 2, 3, 162, 113, 416, 314, 3, 1, 9, 2, 0, 1, 0, 1, 0, 0, 0, 0, 165, 115, 427, 319, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 162, 110, 420, 313, 2, 5, 5, 6, 0, 0, 0, 0, 0, 0, 1, 0, NULL, 2023, '2023-11-20 15:47:36', 4);
INSERT INTO `t_gizi_warga` VALUES (10, 'SUKMAJAYA', 3672051001, 367205, 1, 0, 2, 0, 3, 1, 5, 4, 141, 127, 458, 429, 2, 6, 4, 8, 1, 0, 1, 0, 2, 1, 4, 4, 144, 132, 464, 436, 0, 1, 0, 1, 0, 0, 1, 1, 3, 0, 4, 1, 138, 125, 452, 424, 3, 9, 8, 13, 2, 0, 3, 2, 1, 0, 1, 0, NULL, 2023, '2023-11-20 15:47:37', 4);

-- ----------------------------
-- Table structure for t_header
-- ----------------------------
DROP TABLE IF EXISTS `t_header`;
CREATE TABLE `t_header`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `waktu` datetime NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `active` int(5) NULL DEFAULT NULL,
  `tipe_id` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_header
-- ----------------------------
INSERT INTO `t_header` VALUES (1, '2023-11-20 21:06:43', '2023-11-20', 1, 2);
INSERT INTO `t_header` VALUES (2, '2023-11-21 08:33:30', '2023-11-21', 1, 2);
INSERT INTO `t_header` VALUES (3, '2023-11-21 11:04:36', '2023-11-21', 1, 3);

-- ----------------------------
-- Table structure for triwulan
-- ----------------------------
DROP TABLE IF EXISTS `triwulan`;
CREATE TABLE `triwulan`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `triwulan` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keyid` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of triwulan
-- ----------------------------
INSERT INTO `triwulan` VALUES (1, '01', 1);
INSERT INTO `triwulan` VALUES (2, '02', 1);
INSERT INTO `triwulan` VALUES (3, '03', 1);
INSERT INTO `triwulan` VALUES (4, '04', 2);
INSERT INTO `triwulan` VALUES (5, '05', 2);
INSERT INTO `triwulan` VALUES (6, '06', 2);
INSERT INTO `triwulan` VALUES (7, '07', 3);
INSERT INTO `triwulan` VALUES (8, '08', 3);
INSERT INTO `triwulan` VALUES (9, '09', 3);
INSERT INTO `triwulan` VALUES (10, '10', 4);
INSERT INTO `triwulan` VALUES (11, '11', 4);
INSERT INTO `triwulan` VALUES (12, '12', 4);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role_id` int(5) NULL DEFAULT NULL,
  `wilayah_id` int(5) NULL DEFAULT NULL,
  `provinsi_id` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kota_id` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kecamatan_id` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kelurahan_id` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tps_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` int(5) NULL DEFAULT NULL,
  `caleg_id` int(5) NULL DEFAULT NULL,
  `tipe_caleg_id` int(5) NULL DEFAULT NULL,
  `no_tps` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$9KPhLjg2Sv/dS6kkNEGiKOzxL32qZh9qWMy0.1/rieMt5V0LV0JOO', NULL, NULL, NULL, '12345678', 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, 'YunanTo Didamba', '3602150810900222@gmail.com', NULL, '$2y$10$TyNSSzOc515McnVvWWpG0.9ihVZ3kx225z1z1GKXpKJOwXPDjUIF.', NULL, '2023-10-26 11:25:02', '2023-10-26 11:30:38', '3602150810900222', 3, 15, '15', '0', '0', '0', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (3, 'Ntol Fakih', '3602150810900002@gmail.com', NULL, '$2y$10$e73fF.IOKTKadhNLhStrNOR4.3.vcspQK9T0.0AefcGb827.EvWNq', NULL, '2023-10-26 11:28:00', '2023-10-30 09:21:23', '3602150810900002', 3, 3672, '36', '3672', '0', '0', '0', 1, 2, 2, NULL);
INSERT INTO `users` VALUES (4, 'YunanTo Didamba', '3602150810900001@gmail.com', NULL, '$2y$10$2MveOc5xlG6wqNkM4mwYHumdHNK6TzgwMy4WipWC8QsWVQkLBEfSe', NULL, '2023-10-26 11:29:31', '2023-10-30 09:20:49', '3602150810900001', 3, 3672, '36', '3672', '0', '0', '0', 1, 1, 2, NULL);
INSERT INTO `users` VALUES (5, 'Ntol Fakih', '36021508109000028@gmail.com', NULL, '$2y$10$N9j90OANi9nf4WPjYP0NWOqx5cV/l17IhtbDyynUjXUkyPaMDDwfu', NULL, '2023-10-26 11:29:48', '2023-10-26 11:30:48', '36021508109000028', 3, 12, '12', '0', '0', '0', '0', 1, 7, 1, NULL);
INSERT INTO `users` VALUES (6, 'Ntol Fakih', '360215081090000213@gmail.com', NULL, '$2y$10$gbvHHOSgOyb3u5XmmGcY4OHdCxwpkLX7VVBB/S8sSPJht/r2JufoG', NULL, '2023-10-26 11:30:45', '2023-10-26 11:30:45', '360215081090000213', 3, 367204, '36', '3672', '367204', '0', '0', 1, 6, 3, NULL);
INSERT INTO `users` VALUES (14, 'dsfdsfsf', '32434324324@gmail.com', NULL, '$2y$10$BM6JLt7deQQzvXcBOlYguuxWS165tHyVV.S/7VquUOjbTY81fe6yC', NULL, '2023-10-26 17:29:19', '2023-10-26 17:29:19', '32434324324', 5, 15, '15', '1508', '0', '0', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (15, 'Truk Tronton', '36021508103434002@gmail.com', NULL, '$2y$10$ZPfOsKVINFZGAS9jvVKXS.1jXFGrpVBsjsIH60MKbRDakOF19TJvO', NULL, '2023-10-26 17:29:50', '2023-10-26 17:29:50', '36021508103434002', 5, 15, '15', '1504', '0', '0', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (16, 'Truk Tronton', '36021503434900002@gmail.com', NULL, '$2y$10$aPl2dPCdIAEHGWsZD/9EV.nztxJOl.uLEzxEGIwMlZe6C6gX.cv7W', NULL, '2023-10-26 17:30:07', '2023-10-26 17:30:07', '36021503434900002', 5, 15, '15', '1572', '0', '0', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (17, 'Gugum', '66666666666@gmail.com', NULL, '$2y$10$FFcjZQxA85VBl58ZeW.CZuB9QfnFdXUibHtq4.mSFIgAPdvCsvGVq', NULL, '2023-10-26 17:31:00', '2023-10-26 17:31:00', '66666666666', 5, 15, '15', '1501', '0', '0', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (18, 'Truk Tronton', '32434324324333@gmail.com', NULL, '$2y$10$n5T2w1CJJPEEORt9exOMzebBpSUnYHQ1Af35FA.XybNvrvxx0cUiq', NULL, '2023-10-26 17:35:16', '2023-10-26 17:35:16', '32434324324333', 5, 15, '15', '1503', '0', '0', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (19, 'rtrttrtrtrt', '33443433434443@gmail.com', NULL, '$2y$10$y8TQpMrywHoLQOqYl3I.XO/LKXDksqzxX3UPLw2XWsKxoozx84nS2', NULL, '2023-10-27 11:33:25', '2023-10-27 11:33:25', '33443433434443', 6, 36, '36', '0', '360209', '0', '0', 1, 4, 1, NULL);
INSERT INTO `users` VALUES (20, 'Truk Tronton', '34344343434@gmail.com', NULL, '$2y$10$3ldszgh7fW1o2K2Wij5a5uGtXN.Dtxpag68QPyoDd4wZ9XBRTE32K', NULL, '2023-10-27 14:25:45', '2023-10-27 14:25:45', '34344343434', 7, 15, '15', '1502', '150201', '0', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (21, 'erereerer', '4326565656@gmail.com', NULL, '$2y$10$jvfhc7TrPPnR3iJQQGAB0uSGYBTD/q6IXPRGDp4nhoq3r/yZqLNzK', NULL, '2023-10-27 14:27:25', '2023-10-27 14:27:25', '4326565656', 7, 15, '15', '1502', '150201', '1502012024', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (22, 'Truk Tronton', '45454546464@gmail.com', NULL, '$2y$10$m8GTdrdFLCh8G8mHmfWxWOw/.ex9jaDSy502TSUFIDJihPBrb8pH.', NULL, '2023-10-27 14:31:00', '2023-10-27 14:31:00', '45454546464', 7, 15, '15', '1504', '150407', '1504071004', '0', 1, 8, 1, NULL);
INSERT INTO `users` VALUES (24, 'Gugum', '367859999999999@gmail.com', NULL, '$2y$10$jQTN738Lacw4xB03gdstje6DGPXIM21wl3dN0JVRekYXNX8W/cmGu', NULL, '2023-10-29 17:54:52', '2023-10-29 17:54:52', '367859999999999', 5, 36, '36', '3602', '0', '0', '0', 1, 3, 1, NULL);
INSERT INTO `users` VALUES (25, 'Atut', '34667777777777@gmail.com', NULL, '$2y$10$qvYjcodCUxeAkUAN3trPCOhA.Q58/6fH1HYxtdK.F5h0BBAaJSfx2', NULL, '2023-10-29 17:59:10', '2023-10-29 17:59:10', '34667777777777', 3, 3673, '36', '3673', '0', '0', '0', 1, 9, 2, NULL);
INSERT INTO `users` VALUES (26, 'Gugum', '360215081666002@gmail.com', NULL, '$2y$10$uy/Q3HLT/MuHxYvySOtVa.Nl8BAVnFIvAk.JpxuXQ2Ywr.wu.6C1G', NULL, '2023-10-29 18:00:34', '2023-10-29 18:00:34', '360215081666002', 6, 3673, '36', '3673', '367305', '0', '0', 1, 9, 2, NULL);
INSERT INTO `users` VALUES (27, 'Asep mabrur', '3602150810900003@gmail.com', NULL, '$2y$10$7RPd8wQNbHK.7b7rhSuFf./MqmROv3sdRqElTPSpa5C79JHcUHJgq', NULL, '2023-10-30 10:48:17', '2023-10-30 10:48:17', '3602150810900003', 3, 36, '36', '0', '0', '0', '0', 1, 3, 1, NULL);
INSERT INTO `users` VALUES (30, 'Truk Tronton', '360215081091111002@gmail.com', NULL, '$2y$10$edIbgq5YqvpakV4T4BfR7ed.pKEDlSg3uzPBha01qY8i86M2vOcGe', NULL, '2023-10-31 09:49:15', '2023-10-31 09:49:15', '360215081091111002', 7, 3672, '36', '3672', '367201', '2147483647', '0', 1, 2, 2, NULL);
INSERT INTO `users` VALUES (38, 'Truk Tronton', '3333332150810900002@gmail.com', NULL, '$2y$10$9fl.wKA536LuYidFCoyj7edE0mYsW6/1QzhRjGPbvZR0Nf9vUDAlq', NULL, '2023-10-31 10:00:38', '2023-10-31 10:00:38', '3333332150810900002', 7, 3672, '36', '3672', '367201', '3672011003', '0', 1, 2, 2, NULL);
INSERT INTO `users` VALUES (40, 'Bagus Sanjaya', '367300000000111@gmail.com', NULL, '$2y$10$Ty.ba8j/IpV2ip.vr6tgJe4bw99Egsb58exNnxU87LjpyIjtBH2pO', NULL, '2023-10-31 10:45:46', '2023-10-31 10:45:46', '367300000000111', 8, 3672, '36', '3672', '367201', '3672011003', '236720110031', 1, 2, 2, 'TPS1');

-- ----------------------------
-- View structure for view_kecamatan
-- ----------------------------
DROP VIEW IF EXISTS `view_kecamatan`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_kecamatan` AS SELECT
	m_kecamatan.id, 
	m_kecamatan.nama, 
	UPPER(m_kecamatan.nama) as nama_upper
FROM
	m_kecamatan ;

-- ----------------------------
-- View structure for view_kelurahan
-- ----------------------------
DROP VIEW IF EXISTS `view_kelurahan`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_kelurahan` AS SELECT
	m_kelurahan.id, 
	m_kelurahan.id_kec,
	m_kelurahan.nama, 
	UPPER(m_kelurahan.nama) as nama_upper
FROM
	m_kelurahan ;

-- ----------------------------
-- View structure for view_spesifikasi
-- ----------------------------
DROP VIEW IF EXISTS `view_spesifikasi`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_spesifikasi` AS SELECT
	
	t_gizi_spesifikasi.nomor AS nomorurut, 
	t_gizi_spesifikasi.kecamatan_id, 
	t_gizi_spesifikasi.kelurahan_id, 
	t_gizi_spesifikasi.kelurahan, 
	FORMAT(t_gizi_spesifikasi.odf,0) as odf, 
	
	t_gizi_spesifikasi.sas_jan as sas_jan, 
	t_gizi_spesifikasi.sas_feb as sas_feb, 
	t_gizi_spesifikasi.sas_mar as sas_mar, 
	t_gizi_spesifikasi.sas_apr as sas_apr, 
	t_gizi_spesifikasi.sas_mei as sas_mei, 
	t_gizi_spesifikasi.sas_jun as sas_jun, 
	t_gizi_spesifikasi.sas_jul as sas_jul, 
	t_gizi_spesifikasi.sas_agu as sas_agu, 
	t_gizi_spesifikasi.sas_sep as sas_sep, 
	t_gizi_spesifikasi.sas_okt as sas_okt, 
	t_gizi_spesifikasi.sas_nov as sas_nov,
	t_gizi_spesifikasi.sas_des as sas_des,
	ROUND(t_gizi_spesifikasi.cap_jan) as num_cap_jan, 
	ROUND(t_gizi_spesifikasi.cap_feb) as num_cap_feb, 
	ROUND(t_gizi_spesifikasi.cap_mar) as num_cap_mar, 
	ROUND(t_gizi_spesifikasi.cap_apr) as num_cap_apr, 
	ROUND(t_gizi_spesifikasi.cap_mei) as num_cap_mei, 
	ROUND(t_gizi_spesifikasi.cap_jun) as num_cap_jun, 
	ROUND(t_gizi_spesifikasi.cap_jul) as num_cap_jul, 
	ROUND(t_gizi_spesifikasi.cap_agu) as num_cap_agu, 
	ROUND(t_gizi_spesifikasi.cap_sep) as num_cap_sep, 
	ROUND(t_gizi_spesifikasi.cap_okt) as num_cap_okt, 
	ROUND(t_gizi_spesifikasi.cap_nov) as num_cap_nov,
	ROUND(t_gizi_spesifikasi.cap_des) as num_cap_des,
	t_gizi_spesifikasi.cap_jan, 
	t_gizi_spesifikasi.cap_feb, 
	t_gizi_spesifikasi.cap_mar, 
	t_gizi_spesifikasi.cap_apr, 
	t_gizi_spesifikasi.cap_mei, 
	t_gizi_spesifikasi.cap_jun, 
	t_gizi_spesifikasi.cap_jul, 
	t_gizi_spesifikasi.cap_agu, 
	t_gizi_spesifikasi.cap_sep, 
	t_gizi_spesifikasi.cap_okt, 
	t_gizi_spesifikasi.cap_nov, 
	t_gizi_spesifikasi.cap_des, 
	t_gizi_spesifikasi.sas_tw1, 
	t_gizi_spesifikasi.cap_tw1, 
	t_gizi_spesifikasi.sas_tw2, 
	t_gizi_spesifikasi.cap_tw2, 
	t_gizi_spesifikasi.sas_tw3, 
	t_gizi_spesifikasi.sas_tw4, 
	t_gizi_spesifikasi.cap_tw3, 
	t_gizi_spesifikasi.cap_tw4, 
	ROUND(t_gizi_spesifikasi.jumlah) as jumlah, 
	t_gizi_spesifikasi.indikator_id, 
	t_gizi_spesifikasi.tahun, 
	t_gizi_spesifikasi.t_header_id, 
	t_gizi_spesifikasi.kecamatan as nama_kecamatan
FROM
	t_gizi_spesifikasi ;

SET FOREIGN_KEY_CHECKS = 1;
