<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Models\Caleg;
use App\Models\ViewCaleg;
use App\Models\Kecamatan;
use App\Models\Kota;
use App\Models\Kelurahan;
use App\Models\ViewKelurahan;
use App\Models\DaerahHide;
use App\Models\ViewTps;
use App\Models\ViewTipeCaleg;
use App\Models\Tps;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(request $request)
    {
        if(Auth::user()->role_id==1){
            if($request->bulan>0){
                $bulan =$request->bulan;
                
              }else{
                $bulan=triwulan_aktif();
              }
              if($request->tahun!=""){
                  $tahun=$request->tahun;
              }else{
                  $tahun=date('Y');
              }
            return view('home_admin',compact('tahun','bulan'));
        }else{
            if(Auth::user()->tipe_caleg_id==1){
                $data=ViewCaleg::where('id',Auth::user()->caleg_id)->firstorfail();
                $nama=$data->provinsi;
                return view('home',compact('nama'));
            }else{
                
                $data=ViewCaleg::where('id',Auth::user()->caleg_id)->firstorfail();
                $nama=$data->kota;
                return view('home_kecamatan',compact('nama'));
            }
        }
        
    }
}
