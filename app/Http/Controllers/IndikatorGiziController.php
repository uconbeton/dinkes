<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Imports\IndikatorGiziImport;
use App\Models\Kecamatan;
use App\Models\SpesifikasiGizi;
use App\Models\HeaderSpesifikasi;
use App\Models\VwSpesifikasi;
use App\Models\Gizi;
use App\Models\User;

class IndikatorGiziController extends Controller
{
    
    public function index(request $request)
    {
        error_reporting(0);
        $template='top';
        if($request->act>0){
            $act=$request->act;
        }else{
            $act=max_header_id(3);
        }
        // dd($act);
        if($request->tahun!=""){
            $tahun=$request->tahun;
        }else{
            $tahun=date('Y');
        }
        return view('indikator-gizi.index',compact('template','act','tahun'));
    }
    public function modal(request $request)
    {
        error_reporting(0);
        $template='top';
        $data=Caleg::find($request->id);
        $id=$request->id;
        if($id==0){
            $disabled='';
        }else{
            $disabled='readonly';
        }
        return view('gizi.modal',compact('template','data','disabled','id'));
    }
    public function view_data(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        
        $data=ViewCaleg::where('id',$id)->first();
        
        if($id==0){
            $disabled='';
        }else{
            $disabled='readonly';
        }
        return view('gizi.view_data',compact('template','data','disabled','id'));
    }
   

    public function get_data(request $request)
    {
        
        $query = VwSpesifikasi::query();
        
        if($request->tahun!=""){
            $data = $query->where('tahun',$request->tahun);
        }else{
            $data = $query->where('tahun',date('Y'));
        }
        $data = $query->where('t_header_id',$request->act)->where('indikator_id',$request->indikator_id)->orderBy('nomorurut','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            
            ->addColumn('kecamatannya', function ($row) {
                if($row->indikator_id==3 || $row->indikator_id==4 ){
                    
                        return $row->nama_kecamatan;
                    
                }else{
                    if(max_id_kelurahan($row->kecamatan_id)==$row->kelurahan_id){
                        return $row->nama_kecamatan;
                    }else{
                        return "";
                    }
                }
                
                
            })
            
            ->rawColumns(['kecamatannya'])
            ->make(true);
    }
    public function get_tipe_record(request $request)
    {
        $query = Gizi::query();
        if($request->bulan!=""){
            $data = $query->where('bulan',$request->bulan);
        }else{
            $data = $query->where('bulan',date('m'));
        }
        if($request->tahun!=""){
            $data = $query->where('tahun',$request->tahun);
        }else{
            $data = $query->where('tahun',date('Y'));
        }
        $data = $query->where('kecamatan_id',$ide)->orderBy('kelurahan','Asc')->get();
    }
    public function get_kecamatan(request $request)
    {
        $data=Kecamatan::where('id_kab',$request->id_kota)->whereNotin('id',get_delete_daerah(2))->orderBy('nama','Asc')->get();
        $tampil='<option value="">.:: Pilih</option>';
        if($request->id>0){
            $id=$request->id;
        }else{
            $id=0;
        }
        foreach($data as $o){
            if($id==$o->id){
                $sel='selected';
            }else{
                $sel='';
            }
            $tampil.='<option value="'.$o->id.'"  '.$sel.'>- '.$o->nama.'</option>';
        }
        return $tampil;
    }
    public function get_kelurahan(request $request)
    {
        $data=Kelurahan::where('id_kota',$request->id_kota)->orderBy('nama','Asc')->get();
        $tampil='<option value="">.:: Pilih</option>';
        if($request->id>0){
            $id=$request->id;
        }else{
            $id=0;
        }
        foreach($data as $o){
            if($id==$o->id){
                $sel='selected';
            }else{
                $sel='';
            }
            $tampil.='<option value="'.$o->id.'"  '.$sel.'>- '.$o->nama.'</option>';
        }
        return $tampil;
    }

    public function delete(request $request){
        $emp=Caleg::where('id',$request->id)->update(['active'=>0]);
         

    }
    public function switch_to(request $request)
    {
        $data=User::where('username',Auth::user()->username)->update(['role_id'=>$request->role_id]);
    }

    public function get_tipe_caleg(request $request)
    {
        error_reporting(0);
        $data = ViewTipeCaleg::orderBy('id','Asc')->get();
        $success=[];

        foreach($data as $o){
           $sub['id']=$o->id;
           $sub['tipe_caleg']=$o->tipe_caleg;
           $sub['total']=$o->total;
           $success[]=$sub;
        }
        return response()->json($success, 200);
    }
    
    
   
    public function store(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['nama_caleg']= 'required';
        $messages['nama_gizi.required']= 'Harap lengkapi nama caleg';
        if($request->id=='0'){
            $rules['nik']= 'required|numeric|unique:m_caleg';
            $messages['nik.required']= 'Harap lengkapi nama caleg';
            $messages['nik.numeric']= 'Harap lengkapi nama caleg';
            $messages['nik.unique']= 'NIK Sudah terdaftar';
        }
        $rules['tipe_caleg_id']= 'required|numeric';
        $messages['tipe_caleg_id.required']= 'Harap Pilih Tipe Caleg';
        $messages['tipe_caleg_id.numeric']= 'Harap Pilih Tipe Caleg';

        if($request->tipe_caleg_id==1){
            

            $rules['provinsi_id']= 'required|numeric';
            $messages['provinsi_id.required']= 'Harap Pilih Provinsi';
            $messages['provinsi_id.numeric']= 'Harap Pilih Provinsi';
        }
        if($request->tipe_caleg_id==2){
            

            $rules['provinsi_id']= 'required|numeric';
            $messages['provinsi_id.required']= 'Harap Pilih Provinsi';
            $messages['provinsi_id.numeric']= 'Harap Pilih Provinsi';

            $rules['kota_id']= 'required|numeric';
            $messages['kota_id.required']= 'Harap Pilih Kota / Kabupaten';
            $messages['kota_id.numeric']= 'Harap Pilih Kota / Kabupaten';
        }
        if($request->tipe_caleg_id==3){
            

            $rules['provinsi_id']= 'required|numeric';
            $messages['provinsi_id.required']= 'Harap Pilih Provinsi';
            $messages['provinsi_id.numeric']= 'Harap Pilih Provinsi';

            $rules['kota_id']= 'required|numeric';
            $messages['kota_id.required']= 'Harap Pilih Kota / Kabupaten';
            $messages['kota_id.numeric']= 'Harap Pilih Kota / Kabupaten';

            $rules['kecamatan_id']= 'required|numeric';
            $messages['kecamatan_id.required']= 'Harap Pilih Kecamatan';
            $messages['kecamatan_id.numeric']= 'Harap Pilih Kecamatan';
        }
        
        if($request->file!=""){
            $rules['file']= 'required|mimes:jpg,jpeg,png,gif';
            $messages['file.required']= 'Upload foto caleg dengan format (jpg,png,jpeg,gif)';
            $messages['file.mimes']= 'Upload foto caleg dengan format (jpg,png,jpeg,gif)';
        }
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{

            if($request->tipe_caleg_id==1){
                $kota_id=0;
                $kecamatan_id=0;
                $wilayah_id=$request->provinsi_id;
                
            }
            if($request->tipe_caleg_id==2){
                $kota_id=$request->kota_id;
                $kecamatan_id=0;
                $wilayah_id=$request->kota_id;
                
            }
            if($request->tipe_caleg_id==3){
                $kota_id=$request->kota_id;
                $kecamatan_id=$request->kecamatan_id;
                $wilayah_id=$request->kecamatan_id;
                
            }
            if($request->id=='0'){
                
                

                $data=Caleg::UpdateOrcreate([
                    'nik'=>$request->nik,
                ],[
                    'tipe_caleg_id'=>$request->tipe_caleg_id,
                    'nama'=>$request->nama_caleg,
                    'nik'=>$request->nik,
                    'provinsi_id'=>$request->provinsi_id,
                    'kecamatan_id'=>$kecamatan_id,
                    'wilayah_id'=>$wilayah_id,
                    'kota_id'=>$kota_id,
                    
                    'active'=>1,
                ]);
                $user=User::UpdateOrcreate([
                    'username'=>$request->nik,
                ],[
                    'caleg_id'=>$data->id,
                    'tipe_caleg_id'=>$request->tipe_caleg_id,
                    'password'=>Hash::make('@admin123'),
                    'name'=>$request->nama_caleg,
                    'email'=>$request->nik.'@gmail.com',
                    'provinsi_id'=>$request->provinsi_id,
                    'kecamatan_id'=>$kecamatan_id,
                    'wilayah_id'=>$wilayah_id,
                    'kota_id'=>$kota_id,
                    'kelurahan_id'=>0,
                    'tps_id'=>0,
                    'role_id'=>3,
                    
                    'active'=>1,
                ]);
                if($request->file!=""){
                    $thumbnail = $request->file('file');
                    $thumbnailFileName =$request->nik.'.'. $thumbnail->getClientOriginalExtension();
                    $filename =$thumbnailFileName;
                    $file = \Storage::disk('public_caleg');
                    
                    if($file->put($filename, file_get_contents($thumbnail))){
                        $data=Caleg::where('id',$data->id)->update([
                            'foto'=>$filename,
                        ]);
                    }
                    echo'@ok';
                }else{
                    echo'@ok';
                }
                
                
            }else{
                $mst=Caleg::find($request->id);
                $data=Caleg::UpdateOrcreate([
                    'id'=>$request->id,
                ],[
                    'tipe_caleg_id'=>$request->tipe_caleg_id,
                    'nama'=>$request->nama_caleg,
                    'provinsi_id'=>$request->provinsi_id,
                    'kecamatan_id'=>$kecamatan_id,
                    'wilayah_id'=>$wilayah_id,
                    'kota_id'=>$kota_id,
                ]);
                $user=User::UpdateOrcreate([
                    'username'=>$mst->nik,
                ],[
                    'tipe_caleg_id'=>$request->tipe_caleg_id,
                    'caleg_id'=>$request->id,
                    'name'=>$request->nama_caleg,
                    'email'=>$mst->nik.'@gmail.com',
                    'password'=>Hash::make('@admin123'),
                    'provinsi_id'=>$request->provinsi_id,
                    'kecamatan_id'=>$kecamatan_id,
                    'wilayah_id'=>$wilayah_id,
                    'kota_id'=>$kota_id,
                    'kelurahan_id'=>0,
                    'tps_id'=>0,
                    'role_id'=>3,
                    'active'=>1,
                ]);
                if($request->file!=""){
                    $thumbnail = $request->file('file');
                    $thumbnailFileName =$request->nik.'.'. $thumbnail->getClientOriginalExtension();
                    $filename =$thumbnailFileName;
                    $file = \Storage::disk('public_caleg');
                    
                    if($file->put($filename, file_get_contents($thumbnail))){
                        $data=Caleg::where('id',$data->id)->update([
                            'foto'=>$filename,
                        ]);
                    }
                    echo'@ok';
                }else{
                    echo'@ok';
                }
            }
           
        }
    }
    public function store_import(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        
        $rules['tahun']= 'required|numeric';
        $messages['tahun.required']= 'Harap Pilih Periode tahun';
        $messages['tahun.numeric']= 'Periode tahun tidak sesuai';

        $rules['file']= 'required|mimes:xls,xlsx';
        $messages['file.required']= 'Harap upload file intervensi Spesifik';
        $messages['file.mimes']= 'Harap upload file intervensi Spesifik dengan format (xls,xlsx)';

        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            $filess = $request->file('file');
            $nama_file = rand() . $filess->getClientOriginalName();
            $filess->move('public/file_excel', $nama_file);
            $exp=explode('.',$nama_file);
            

            $save=HeaderSpesifikasi::UpdateOrcreate([
               
                'tanggal'=>date('Y-m-d'),
                'active'=>1,
                'tipe_id'=>3
            ],[
                'waktu'=>date('Y-m-d H:i:s'),
            ]);
            $import = new IndikatorGiziImport($request->tahun,$save->id);
            // $import->onlySheets('Worksheet 1', 'Worksheet 3');
            Excel::import($import, public_path('public/file_excel/' . $nama_file));
            echo'@ok';
        }
    }
}
