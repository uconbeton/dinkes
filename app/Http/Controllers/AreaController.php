<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Models\Caleg;
use App\Models\ViewCaleg;
use App\Models\Kecamatan;
use App\Models\Kota;
use App\Models\Kelurahan;
use App\Models\ViewKelurahan;
use App\Models\DaerahHide;
use App\Models\ViewTps;
use App\Models\ViewTipeCaleg;
use App\Models\Tps;
use App\Models\User;

class AreaController extends Controller
{
    
    public function index_kota(request $request)
    {
        error_reporting(0);
        $template='top';
        
        return view('area.index_kota',compact('template'));
    }
    public function index_tps(request $request)
    {
        error_reporting(0);
        $template='top';
        $data=ViewKelurahan::where('id',$request->kelurahan_id)->first();
        return view('area.index_tps',compact('data'));
    }
    public function index_kecamatan(request $request)
    {
        error_reporting(0);
        $template='top';
        if($request->kota_id>0){
            $id=$request->kota_id;
        }else{
            if(Auth::user()->role_id==1 || Auth::user()->role_id==4 || Auth::user()->role_id==3){
                if(Auth::user()->tipe_caleg_id==1){
                    $data=Kota::where('id_prov',Auth::user()->provinsi_id)->whereNotin('id',get_delete_daerah(1))->orderBy('nama','Asc')->firstOrfail();
                    $id=$data->id;
                }else{
                    $id=Auth::user()->kota_id;
                }
                
            }else{
                $id=Auth::user()->kota_id;
            }
        }
        $mst=Kota::find($id);
        
        return view('area.index_kecamatan',compact('template','id','mst'));
    }
    public function index_kelurahan(request $request)
    {
        error_reporting(0);
        $template='top';
        // $success=array_kota();
        // return response()->json($success, 200);
        if($request->kecamatan_id>0){
            $kota_id=0;
            $id=$request->kecamatan_id;
        }else{
            if(Auth::user()->role_id==1 || Auth::user()->role_id==4 || Auth::user()->role_id==5 || Auth::user()->role_id==3){
                if(Auth::user()->tipe_caleg_id==1 || Auth::user()->tipe_caleg_id==2){
                    if(Auth::user()->tipe_caleg_id==1){
                        $kota=Kota::where('id_prov',Auth::user()->provinsi_id)->orderBy('nama','Asc')->firstOrfail();
                        $kota_id=$kota->id;
                        $data=Kecamatan::where('id_kab',$kota_id)->whereNotin('id',get_delete_daerah(2))->orderBy('nama','Asc')->firstOrfail();
                        $id=$data->id;
                        $disabledkota='';
                        $disabledkecamatan='';
                    }
                    if(Auth::user()->tipe_caleg_id==2){
                        $kota_id=Auth::user()->kota_id;
                        $data=Kecamatan::where('id_kab',$kota_id)->whereNotin('id',get_delete_daerah(2))->orderBy('nama','Asc')->firstOrfail();
                        $id=$data->id;
                        $disabledkota='disabled';
                        $disabledkecamatan='';
                    }
                    
                }else{
                    $kota_id=Auth::user()->kota_id;
                    $id=Auth::user()->kecamatan_id;
                    $disabledkota='disabled';
                    $disabledkecamatan='disabled';
                }
                
            }else{
                $kota_id=Auth::user()->kota_id;
                $id=Auth::user()->kecamatan_id;
                $disabledkota='disabled';
                $disabledkecamatan='disabled';
            }
        }
        $mst=Kecamatan::find($id);
        
        return view('area.index_kelurahan',compact('template','id','mst','disabledkota','disabledkecamatan'));
    }
    
    public function modal(request $request)
    {
        error_reporting(0);
        $template='top';
        $data=Kota::find($request->id);
        $id=$request->id;
        if($id==0){
            $disabled='';
        }else{
            $disabled='readonly';
        }
        return view('area.modal',compact('template','data','disabled','id'));
    }
    public function modal_kecamatan(request $request)
    {
        error_reporting(0);
        $template='top';
        $data=Kecamatan::find($request->id);
        $id=$request->id;
        if($id==0){
            $disabled='';
        }else{
            $disabled='readonly';
        }
        return view('area.modal_kecamatan',compact('template','data','disabled','id'));
    }
    public function modal_tps(request $request)
    {
        error_reporting(0);
        $template='top';
        $data=ViewTps::where('nomor',$request->nomor)->first();;
        $id=$request->id;
        if($id==0){
            $disabled='';
        }else{
            $disabled='readonly';
        }
        return view('area.modal_tps',compact('template','data','disabled','id'));
    }
    public function modal_kelurahan(request $request)
    {
        error_reporting(0);
        $template='top';
        $data=Kelurahan::find($request->id);
        $kecamatan_id=$data->id_kec;
        $ks=Kecamatan::find($data->id_kec);
        $kota_id=$ks->id_kab;
        $id=$request->id;
        if($id==0){
            $disabled='';
        }else{
            $disabled='readonly';
        }
        return view('area.modal_kelurahan',compact('template','data','disabled','id','kota_id','kecamatan_id'));
    }
    public function view_data(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        
        $data=ViewCaleg::where('id',$id)->first();
        
        if($id==0){
            $disabled='';
        }else{
            $disabled='readonly';
        }
        return view('caleg.view_data',compact('template','data','disabled','id'));
    }
   

    public function get_data(request $request)
    {
        error_reporting(0);
        $query = ViewTps::query();
        $data = $query->where('kode_kelurahan',$request->id)->orderBy('nom','Asc')->get();

        return Datatables::of($data)
        
            ->addColumn('foto', function ($row) {
                if($row->foto!=""){
                    $btn='<img src="'.url_plug().'/attach/foto_caleg/'.$row->foto.'" class="img-rounded height-30" />';
                }else{
                    $btn='<img src="'.url_plug().'/assets/img/user/user-1.jpg" class="img-rounded height-30" />';
                }
                
                return $btn;
            })
            ->addColumn('action', function ($row) {
                $btn='
                    <div class="btn-group">
                        <span class="btn btn-xs btn-primary" onclick="add_data('.$row->nomor.')" title="ubah data"><i class="fas fa-pencil-alt fa-fw"></i></span>
                    </div>
                ';
                return $btn;
            })
            
            ->rawColumns(['action','foto'])
            ->make(true);
    }
    public function index_isi(request $request)
    {
        $data=Kota::where('id_prov',Auth::user()->provinsi_id)->orderBy('nama','Asc')->get();
        $tampol='';
        foreach($data as $o){
            $tampol.='<h2>'.$o->nama.'</h2>';
            $camat=Kecamatan::where('id_kab',$o->id)->orderBy('nama','Asc')->get();
            foreach($camat as $s){
                $tampol.='<h3>&nbsp;&nbsp;&nbsp;'.$s->nama.'</h3>';
                $kelurahan=Kelurahan::where('id_kec',$s->id)->orderBy('nama','Asc')->get();
                $tampol.='<table width="100%" border="1">';
                foreach($kelurahan as $k){
                    $tampol.='<tr><td>'.$k->nama.'</td><td><input type="hidden" name="kelurahan_id[]" value="'.$k->id.'"></td><td><input type="text" value="'.$k->total_tps.'" name="total_tps[]" value="0"></td></tr>';
                }
                $tampol.='</table>';
            }
        }
        return view('area.isian',compact('tampol'));

    }
    public function get_kota(request $request)
    {
        $data=Kota::whereNotin('id',get_delete_daerah(1))->where('id_prov',Auth::user()->provinsi_id)->orderBy('nama','Asc')->get();
        $success=[];
        foreach($data as $no=>$o){
            $kec=Kecamatan::where('id_kab',$o->id)->count();
            $cek=User::where('kota_id',$o->id)->where('caleg_id',Auth::user()->caleg_id)->where('role_id',5)->count();
            if($cek>0){
                $mst=User::where('kota_id',$o->id)->where('caleg_id',Auth::user()->caleg_id)->where('role_id',5)->firstOrfail();
                $nik=$mst->username;
                $name=$mst->name;
                $users_id=$mst->id;
                
            }else{
                $nik="";
                $name="";
                $users_id="";
            }
            $sub['id']=$o->id;
            $sub['nama']=$o->nama;
            $sub['cek']=$cek;
            $sub['name']=$name;
            $sub['nik']=$nik;
            $sub['total_rencana']=count_rencana_tps(2,$o->id);
            $sub['total_actual']=count_actual_tps(2,$o->id);
            $sub['users_id']=$users_id;
            $sub['total_kecamatan']=$kec;
            $success[]=$sub;
        }

        return response()->json($success, 200);
    }
    public function get_kecamatan(request $request)
    {
        if($request->kota_id>0){
            $data=Kecamatan::whereNotin('id',get_delete_daerah(2))->where('id_kab',$request->kota_id)->orderBy('nama','Asc')->get();
        }else{
            $data=Kecamatan::whereNotin('id',get_delete_daerah(2))->where('id_kab',Auth::user()->kota_id)->orderBy('nama','Asc')->get();
        }
        
        $success=[];
        foreach($data as $no=>$o){
            $kec=Kelurahan::where('id_kec',$o->id)->count();
            $cek=User::where('kecamatan_id',$o->id)->where('caleg_id',Auth::user()->caleg_id)->where('role_id',6)->count();
            if($cek>0){
                $mst=User::where('kecamatan_id',$o->id)->where('caleg_id',Auth::user()->caleg_id)->where('role_id',6)->firstOrfail();
                $nik=$mst->username;
                $name=$mst->name;
                $users_id=$mst->id;
                
            }else{
                $nik="";
                $name="";
                $users_id="";
            }
            $sub['id']=$o->id;
            $sub['nama']=$o->nama;
            $sub['cek']=$cek;
            $sub['name']=$name;
            $sub['nik']=$nik;
            $sub['total_rencana']=count_rencana_tps(3,$o->id);
            $sub['total_actual']=count_actual_tps(3,$o->id);
            $sub['users_id']=$users_id;
            $sub['total_kelurahan']=$kec;
            $success[]=$sub;
        }

        return response()->json($success, 200);

    }
    public function get_kelurahan(request $request)
    {
        if($request->kecamatan_id>0){
            $data=Kelurahan::where('id_kec',$request->kecamatan_id)->orderBy('nama','Asc')->get();
        }else{
            $data=Kelurahan::where('id_kec',Auth::user()->kecamatan_id)->orderBy('nama','Asc')->get();
        }
        
        $success=[];
        foreach($data as $no=>$o){
            $kec=$o->total_tps;
            $cek=User::where('kelurahan_id',$o->id)->where('caleg_id',Auth::user()->caleg_id)->where('role_id',7)->count();
            $cektps=Tps::where('kode_kelurahan',$o->id)->where('caleg_id',Auth::user()->caleg_id)->count();
            if($cek>0){
                $mst=User::where('kelurahan_id',$o->id)->where('caleg_id',Auth::user()->caleg_id)->where('role_id',7)->firstOrfail();
                $nik=$mst->username;
                $name=$mst->name;
                $users_id=$mst->id;
                
            }else{
                $nik="";
                $name="";
                $users_id="";
            }
            if($cektps==$kec){
                $act=1;
            }else{
                $act=0;
            }
            $sub['id']=$o->id;
            $sub['nama']=$o->nama;
            $sub['cek']=$cek;
            $sub['name']=$name;
            $sub['nik']=$nik;
            $sub['users_id']=$users_id;
            $sub['total_tps']=$kec;
            $sub['total_rencana']=count_rencana_tps(4,$o->id);
            $sub['total_actual']=count_actual_tps(4,$o->id);
            $sub['act']=$act;
            $success[]=$sub;
        }

        return response()->json($success, 200);
    }

    public function delete(request $request){
        $emp=User::where('id',$request->id)->delete();
         

    }
    public function buat_tps(request $request){
        error_reporting(0);
        $kelurahan_id=$request->kelurahan_id;
        $emp=ViewKelurahan::where('id',$kelurahan_id)->where('total_tps','>',0)->first();
        // dd($emp);
        for($x=1;$x<=$emp->total_tps;$x++){
            $save=Tps::UpdateOrcreate([
                'nomor'=>Auth::user()->caleg_id.$kelurahan_id.$x,
            ],[
                'kelurahan_id'=>$kelurahan_id,
                'kode_kelurahan'=>$kelurahan_id,
                'caleg_id'=>Auth::user()->caleg_id,
                'no_tps'=>'TPS'.$x,
                'nom'=>$x,
                'tipe_caleg_id'=>Auth::user()->tipe_caleg_id,
                'kecamatan_id'=>$emp->kecamatan_id,
                'kota_id'=>$emp->kota_id,
                'provinsi_id'=>$emp->provinsi_id,
            ]);
           
        }
         

    }
    public function delete_daerah(request $request)
    {
        error_reporting(0);
        $sum=count((array) $request->id);
        if($sum>0){
            for($x=0;$x<$sum;$x++){
                $data=DaerahHide::UpdateOrcreate([
                    'caleg_id'=>Auth::user()->caleg_id,
                    'area_id'=>$request->id[$x],
                    'kategori_area_id'=>1,
                ],[
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
            }
            echo '@ok@';
        }else{
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">Pilih kota / kabupaten yang akan dihapus</div></div>';
        }
            

        
    }
    public function delete_daerah_kecamatan(request $request)
    {
        error_reporting(0);
        $sum=count((array) $request->id);
        if($sum>0){
            for($x=0;$x<$sum;$x++){
                $data=DaerahHide::UpdateOrcreate([
                    'caleg_id'=>Auth::user()->caleg_id,
                    'area_id'=>$request->id[$x],
                    'kategori_area_id'=>2,
                ],[
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
            }
            echo '@ok@';
        }else{
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">Pilih kecamatan yang akan dihapus</div></div>';
        }
            

        
    }
    public function get_tps(request $request)
    {
        $clg=Caleg::get();
        foreach($clg as $c){
            $data=ViewKelurahan::where('total_tps','>',0)->get();
            foreach($data as $o){
                for($x=1;$x<$o->total_tps;$x++){
                    $save=Tps::UpdateOrcreate([
                        'nomor'=>$c->id.$o->id.$x,
                    ],[
                        'kelurahan_id'=>$o->id,
                        'kode_kelurahan'=>$o->id,
                        'caleg_id'=>$c->id,
                        'no_tps'=>'TPS'.$x,
                        'nom'=>$x,
                        'tipe_caleg_id'=>$c->tipe_caleg_id,
                        'kecamatan_id'=>$o->kecamatan_id,
                        'kota_id'=>$o->kota_id,
                        'provinsi_id'=>$o->provinsi_id,
                        
                        'rencana'=>0,
                        'actual'=>0,
                    ]);
                }
            }
        }
    }

    public function get_tipe_caleg(request $request)
    {
        error_reporting(0);
        $data = ViewTipeCaleg::orderBy('id','Asc')->get();
        $success=[];

        foreach($data as $o){
           $sub['id']=$o->id;
           $sub['tipe_caleg']=$o->tipe_caleg;
           $sub['total']=$o->total;
           $success[]=$sub;
        }
        return response()->json($success, 200);
    }
    public function get_data_target(request $request)
    {
        error_reporting(0);
        $query = ViewTps::query();
        $rencana = $query->where('kode_kelurahan',$request->id)->sum('rencana');
        $actual = $query->where('kode_kelurahan',$request->id)->sum('actual');
        $total_pemilih = $query->where('kode_kelurahan',$request->id)->sum('total_pemilih');
        $persen=round(($actual/$total_pemilih)*100);
        $success=[];
        $success['rencana']=$rencana;
        $success['actual']=$actual;
        $success['total_pemilih']=$total_pemilih;
        $success['persen']=$persen.'%';

        return response()->json($success, 200);
    }
    
    
   
    public function store(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['name']= 'required';
        $messages['name.required']= 'Harap lengkapi nama pengawas';
       
        $rules['nik']= 'required|numeric';
        $messages['nik.required']= 'Harap lengkapi nik pengawas';
        $messages['nik.numeric']= 'Harap lengkapi nik pengawas';
        
        $rules['no_hp']= 'required|numeric';
        $messages['no_hp.required']= 'Harap lengkapi No telepon';
        $messages['no_hp.numeric']= 'Harap lengkapi No telepon';

        
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            $caleg_id=Auth::user()->caleg_id;
            $tipe_caleg_id=Auth::user()->tipe_caleg_id;
            $provinsi_id=Auth::user()->provinsi_id;
            $wilayah_id=Auth::user()->wilayah_id;
            
            if($request->role_id==5){
                $kecamatan_id=Auth::user()->kecamatan_id;
                $cek=User::where('username',$request->nik)->where('caleg_id',$caleg_id)->count();
                if($cek>0){
                    echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">Nik Sudah terdaftar</div></div>';
                }else{
                     $user=User::UpdateOrcreate([
                        'kota_id'=>$request->kota_id,
                        'caleg_id'=>$caleg_id,
                        'role_id'=>5,
                    ],[
                        
                        'tipe_caleg_id'=>$tipe_caleg_id,
                        'provinsi_id'=>$provinsi_id,
                        'password'=>Hash::make('@admin123'),
                        'name'=>$request->name,
                        'email'=>$request->nik.'@gmail.com',
                        'kecamatan_id'=>$kecamatan_id,
                        'wilayah_id'=>$wilayah_id,
                        'username'=>$request->nik,
                        
                        'kelurahan_id'=>0,
                        'tps_id'=>0,
                        
                        
                        'active'=>1,
                    ]);
                    echo '@ok';
                }
            }
            if($request->role_id==6){
                $kota_id=Auth::user()->kota_id;
                $cek=User::where('username',$request->nik)->where('caleg_id',$caleg_id)->count();
                if($cek>0){
                    echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">Nik Sudah terdaftar</div></div>';
                }else{
                     $user=User::UpdateOrcreate([
                        'kecamatan_id'=>$request->kecamatan_id,
                        'caleg_id'=>$caleg_id,
                        'role_id'=>6,
                    ],[
                        
                        'tipe_caleg_id'=>$tipe_caleg_id,
                        'provinsi_id'=>$provinsi_id,
                        'password'=>Hash::make('@admin123'),
                        'name'=>$request->name,
                        'email'=>$request->nik.'@gmail.com',
                        'kota_id'=>$kota_id,
                        'wilayah_id'=>$wilayah_id,
                        'username'=>$request->nik,
                        
                        'kelurahan_id'=>0,
                        'tps_id'=>0,
                        
                        
                        'active'=>1,
                    ]);
                    echo '@ok';
                }
            }
            if($request->role_id==7){
                $kota_id=Auth::user()->kota_id;
                $kel=ViewKelurahan::where('id',$request->kelurahan_id)->first();
                $cek=User::where('username',$request->nik)->where('caleg_id',$caleg_id)->count();
                if($cek>0){
                    echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">Nik Sudah terdaftar</div></div> '.$request->kelurahan_id;
                }else{
                     $user=User::UpdateOrcreate([
                        'kelurahan_id'=>$kel->id,
                        'caleg_id'=>$caleg_id,
                        'role_id'=>7,
                    ],[
                        'kecamatan_id'=>$kel->kecamatan_id,
                        'tipe_caleg_id'=>$tipe_caleg_id,
                        'provinsi_id'=>$kel->provinsi_id,
                        'password'=>Hash::make('@admin123'),
                        'name'=>$request->name,
                        'email'=>$request->nik.'@gmail.com',
                        'kota_id'=>$kel->kota_id,
                        'wilayah_id'=>$wilayah_id,
                        'username'=>$request->nik,
                        'tps_id'=>0,
                        
                        
                        'active'=>1,
                    ]);
                     $kelur=Kelurahan::UpdateOrcreate([
                        'id'=>$kel->id,
                     ],[
                        'total_tps'=>$request->total_tps,
                     ]);

                    // $emp=ViewKelurahan::where('id',$kel->id)->where('total_tps','>',0)->first();
                    // dd($emp);
                    $success=[];
                    if($request->total_tps>0){
                        for($x=1;$x<=$request->total_tps;$x++){
                            $nmor=Auth::user()->caleg_id.$kel->id.$x;
                            $sucess[]=$nmor;
                            $save=Tps::UpdateOrcreate([
                                'nomor'=>$nmor,
                            ],[
                                'kelurahan_id'=>$kel->id,
                                'kode_kelurahan'=>$kel->id,
                                'caleg_id'=>Auth::user()->caleg_id,
                                'no_tps'=>'TPS'.$x,
                                'nom'=>$x,
                                'tipe_caleg_id'=>Auth::user()->tipe_caleg_id,
                                'kecamatan_id'=>$kel->kecamatan_id,
                                'kota_id'=>$kel->kota_id,
                                'provinsi_id'=>$kel->provinsi_id,
                            ]);
                        
                        }
                    }
                    $del=Tps::where('nom','>',$request->total_tps)->where('kode_kelurahan',$kel->id)->where('caleg_id',Auth::user()->caleg_id)->delete();
                    echo '@ok';
                }
            }
            if($request->role_id==8){
                $mrt=ViewTps::where('nomor',$request->nomor)->first();
                $delee=User::where('tps_id',$request->nomor)->delete();
                $cek=User::where('username',$request->nik)->where('caleg_id',$caleg_id)->count();
                if($cek>0){
                    echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">Nik Sudah terdaftar</div></div>';
                }else{
                     $ttps=ViewTps::where('nomor',$request->nomor)->update(['nik'=>$request->nik,'nama_pengawas'=>$request->name,]);
                     $user=User::UpdateOrcreate([
                        'tps_id'=>$mrt->nomor,
                        'kelurahan_id'=>$mrt->kode_kelurahan,
                        'caleg_id'=>$caleg_id,
                        'role_id'=>8,
                    ],[
                        'kecamatan_id'=>$mrt->kecamatan_id,
                        'tipe_caleg_id'=>$mrt->tipe_caleg_id,
                        'provinsi_id'=>$provinsi_id,
                        'password'=>Hash::make('@admin123'),
                        'name'=>$request->name,
                        'email'=>$request->nik.'@gmail.com',
                        'kota_id'=>$mrt->kota_id,
                        'wilayah_id'=>$wilayah_id,
                        'username'=>$request->nik,
                        'no_tps'=>$mrt->no_tps,
                        
                        
                        'active'=>1,
                    ]);
                    echo '@ok';
                }
            }
                
           
        }
    }
    public function store_isian(request $request){
        error_reporting(0);
            $sum=count($request->kelurahan_id);
            for($x=0;$x<$sum;$x++){
                     $user=Kelurahan::UpdateOrcreate([
                        'id'=>$request->kelurahan_id[$x],
                    ],[
                        
                        'total_tps'=>$request->total_tps[$x],
                    ]);
            }
                    echo '@ok';
                
    }
}
