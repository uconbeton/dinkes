<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Models\Caleg;
use App\Models\ViewCaleg;
use App\Models\Kecamatan;
use App\Models\Kota;
use App\Models\Kelurahan;
use App\Models\ViewKelurahan;
use App\Models\Gizi;
use App\Models\ViewTps;
use App\Models\HeaderSpesifikasi;
use App\Models\SpesifikasiGizi;
use App\Models\Tps;
use App\Models\User;
use DB;

class ProfileController extends Controller
{
    
    public function index(request $request)
    {
        error_reporting(0);
        $template='top';
        if($request->bulan>0){
          $bulan =$request->bulan;
          
        }else{
          $bulan=triwulan_aktif();
        }
        if($request->tahun!=""){
            $tahun=$request->tahun;
        }else{
            $tahun=date('Y');
        }
        return view('profil.index',compact('tahun','bulan'));
    }
    public function index_detail(request $request ,$id)
    {
        error_reporting(0);
        $template='top';
        $data=Kecamatan::find($id);
          if($request->act>0){
               $act=$request->act;
          }else{
               $act=0;
          }
          if($request->bulan>0){
               $bulan =$request->bulan;
               
          }else{
               $bulan=triwulan_aktif();
          }
          if($request->tahun!=""){
               $tahun=$request->tahun;
          }else{
               $tahun=date('Y');
          }
        return view('profil.index_detail',compact('tahun','data','act','bulan'));
    }
    public function index_detail_intervensi(request $request ,$id)
    {
        error_reporting(0);
        $template='top';
        $data=Kecamatan::find($id);
          if($request->act>0){
               $act=$request->act;
          }else{
               $act=0;
          }
          if($request->bulan>0){
               $bulan =$request->bulan;
               
          }else{
               $bulan=triwulan_aktif();
          }
          if($request->tahun!=""){
               $tahun=$request->tahun;
          }else{
               $tahun=date('Y');
          }
        return view('intervensi.index_detail',compact('tahun','data','act','bulan'));
    }
    public function index_detail_sensitif(request $request ,$id)
    {
        error_reporting(0);
        $template='top';
        $data=Kecamatan::find($id);
          if($request->act>0){
               $act=$request->act;
          }else{
               $act=0;
          }
          if($request->bulan>0){
               $bulan =$request->bulan;
               
          }else{
               $bulan=triwulan_aktif();
          }
          if($request->tahun!=""){
               $tahun=$request->tahun;
          }else{
               $tahun=date('Y');
          }
        return view('sensitif.index_detail',compact('tahun','data','act','bulan'));
    }
    

    public function get_kecamatan(request $request)
    {
        error_reporting(0);
        $token=config('app.key');
        $data=Kecamatan::where('id_kab',3672)->orderBy('nama','Asc')->get();
        $success=[];
        foreach($data as $no=>$o){
           $sp=Gizi::where('kecamatan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('SPL59+SPP59'));
           $pd=Gizi::where('kecamatan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('PDL59+PDP59'));
           $nr=Gizi::where('kecamatan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('NRL59+NRP59'));
           $tg=Gizi::where('kecamatan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('TGL59+TGP59'));
           $sum=($sp+$pd);
           $sumall=($sp+$pd+$nr+$tg);
           if($sumall>0){
                $y=$sumall;
            }else{
                $y=0;
                
            }
           if($sum>0){
                $x=$sum;
                $total=ceil_persen(($x/$y)*100); 
           }else{
                $x=0;
                $total=0; 
           }
           if($nr>0){
                $xnr=$nr;
                $normal=ceil_persen(($xnr/$y)*100); 
           }else{
                $xnr=0;
                $normal=0; 
           }
           if($sp>0){
                $xsp=$sp;
                $sangatpendek=ceil_persen(($xsp/$y)*100); 
           }else{
                $xsp=0;
                $sangatpendek=0; 
           }
           if($pd>0){
                $xpd=$pd;
                $pendek=ceil_persen(($xpd/$y)*100); 
           }else{
                $xpd=0;
                $pendek=0; 
           }
           if($tg>0){
                $xtg=$tg;
                $tinggi=ceil_persen(($xtg/$y)*100); 
           }else{
                $xtg=0;
                $tinggi=0; 
           }
           $hasil=$pendek+$tinggi+$sangatpendek;
           if($hasil>0){
               $ttnormal=(100-($pendek+$tinggi+$sangatpendek));
           }else{
               $ttnormal=0;
               
           }
        //    $total=(($x/$y)*100);
            $sub['id']=$o->id;
            $sub['nama']=$o->nama;
            $sub['sangat_pendek']=$sangatpendek;
            $sub['pendek']= $pendek;
            $sub['normal']= $ttnormal;
            $sub['tinggi']= $tinggi;
            $sub['nama']=$o->nama;
            $sub['color']=$o->color;
            $sub['x']=$x;
            $sub['y']=$y;
            $sub['total']=$total;
            
            $success[]=$sub;
        }

        return response()->json($success, 200);
    }
    public function get_data_kelurahan(request $request,$id)
    {
          
        error_reporting(0);
        $token=config('app.key');
        $data=Kelurahan::where('id_kec',$id)->orderBy('nama','Asc')->get();
        $success=[];
        foreach($data as $no=>$o){
           $sp=Gizi::where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('SPL59+SPP59'));
           $pd=Gizi::where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('PDL59+PDP59'));
           $nr=Gizi::where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('NRL59+NRP59'));
           $tg=Gizi::where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('keyid',$request->bulan)->sum(\DB::raw('TGL59+TGP59'));
           $sum=($sp+$pd);
           $sumall=($sp+$pd+$nr+$tg);
           
           if($sumall>0){
                $y=$sumall;
            }else{
                $y=0;
                
            }
           if($sum>0){
                $x=$sum;
                $total=ceil(($x/$y)*100); 
           }else{
                $x=0;
                $total=0; 
           }
           if($nr>0){
                $xnr=$nr;
                $normal=ceil(($xnr/$y)*100); 
           }else{
                $xnr=0;
                $normal=0; 
           }
           if($sp>0){
                $xsp=$sp;
                $sangatpendek=ceil(($xsp/$y)*100); 
           }else{
                $xsp=0;
                $sangatpendek=0; 
           }
           if($pd>0){
                $xpd=$pd;
                $pendek=ceil(($xpd/$y)*100); 
           }else{
                $xpd=0;
                $pendek=0; 
           }
           if($tg>0){
                $xtg=$tg;
                $tinggi=ceil(($xtg/$y)*100); 
           }else{
                $xtg=0;
                $tinggi=0; 
           }
           $hasil=$pendek+$tinggi+$sangatpendek;
           if($hasil>0){
               $ttnormal=(100-($pendek+$tinggi+$sangatpendek));
           }else{
               $ttnormal=0;
               
           }
        //    $total=(($x/$y)*100);
            $sub['id']=$o->id;
            $sub['nama']=$o->nama;
            $sub['sangat_pendek']=$sangatpendek;
            $sub['pendek']=$pendek;
            $sub['normal']=$ttnormal;
            $sub['tinggi']=$tinggi;
            $sub['nama']=$o->nama;
            $sub['color']=$o->color;
            $sub['x']=$x;
            $sub['y']=$y;
            $sub['total']=$total;
            
            $success[]=$sub;
        }

        return response()->json($success, 200);
    }
    public function get_data_kelurahan_intervensi(request $request,$id)
    {
          
        error_reporting(0);
        $t_header_id=max_header_id(3);
        $mst=HeaderSpesifikasi::find($t_header_id);
        $tahun=date('Y',strtotime($mst->tanggal));
        $token=config('app.key');
        if($request->indikator_id==3 || $request->indikator_id==4 ){
          $tw1=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw1');
          $tw2=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw2');
          $tw3=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw3');
          $tw4=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw4');
          $tc1=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw1');
          $tc2=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw2');
          $tc3=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw3');
          $tc4=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw4');
          $success=[];
          if($tc1>0){
               $success['persen1']=(int) ceil(($tc1/$tw1)*100);
          }else{
               $success['persen1']=0;
          }
          if($tc2>0){
               $success['persen2']=(int) ceil(($tc2/$tw2)*100);
          }else{
               $success['persen2']=0;
          }
          if($tc3>0){
               $success['persen3']=(int) ceil(($tc3/$tw3)*100);
          }else{
               $success['persen3']=0;
          }
          if($tc4>0){
               $success['persen4']=(int) ceil(($tc4/$tw4)*100);
          }else{
               $success['persen4']=0;
          }
          $success['sasaran1']=$tw1;
          $success['sasaran2']=$tw2;
          $success['sasaran3']=$tw3;
          $success['sasaran4']=$tw4;
          $success['capaian1']=$tc1;
          $success['capaian2']=$tc2;
          $success['capaian3']=$tc3;
          $success['capaian4']=$tc4;
        }else{
               $data=Kelurahan::where('id_kec',$id)->orderBy('nama','Asc')->get();
               $success=[];
               $subdet=[];
               $sumtw1=0;
               $sumtw3=0;
               $sumtw3=0;
               $sumtw4=0;
               $sumtc1=0;
               $sumtc3=0;
               $sumtc3=0;
               $sumtc4=0;
               foreach($data as $no=>$o){
                    
                         $tw1=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw1');
                         $tw2=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw2');
                         $tw3=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw3');
                         $tw4=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw4');
                         $tc1=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw1');
                         $tc2=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw2');
                         $tc3=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw3');
                         $tc4=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw4');
                         
                    
                    $sumtw1+=$tw1;
                    $sumtw2+=$tw2;
                    $sumtw3+=$tw3;
                    $sumtw4+=$tw4;
                    $sumtc1+=$tc1;
                    $sumtc2+=$tc2;
                    $sumtc3+=$tc3;
                    $sumtc4+=$tc4;
                    
                    
                    $sub['id']=$o->id;
                    $sub['tahun']=$tahun;
                    $sub['nama']=$o->nama;
                    $sub['sasaran1']=(int) uang($tw1);
                    $sub['sasaran2']=(int) uang($tw2);
                    $sub['sasaran3']=(int) uang($tw3);
                    $sub['sasaran4']=(int) uang($tw4);
                    $sub['capaian1']=(int) uang($tc1);
                    $sub['capaian2']=(int) uang($tc2);
                    $sub['capaian3']=(int) uang($tc3);
                    $sub['capaian4']=(int) uang($tc4);
                    if($tc1>0){
                         $sub['persen1']=(int) ceil(($tc1/$tw1)*100);
                    }else{
                         $sub['persen1']=0;
                    }
                    if($tc2>0){
                         $sub['persen2']=(int) ceil(($tc2/$tw2)*100);
                    }else{
                         $sub['persen2']=0;
                    }
                    if($tc3>0){
                         $sub['persen3']=(int) ceil(($tc3/$tw3)*100);
                    }else{
                         $sub['persen3']=0;
                    }
                    if($tc4>0){
                         $sub['persen4']=(int) ceil(($tc4/$tw4)*100);
                    }else{
                         $sub['persen4']=0;
                    }
                    
                    $sub['color']=$o->color;
                    
                    
                    $subdet[]=$sub;
               }
               $success['tot_tw1']=uang($sumtw1);
               $success['tot_tw2']=uang($sumtw2);
               $success['tot_tw3']=uang($sumtw3);
               $success['tot_tw4']=uang($sumtw4);
               $success['tot_tc1']=uang($sumtc1);
               $success['tot_tc2']=uang($sumtc2);
               $success['tot_tc3']=uang($sumtc3);
               $success['tot_tc4']=uang($sumtc4);
               if($sumtc1>0){
                    $success['tot_persen1']=ceil(($sumtc1/$sumtw1)*100);
               }else{
                    $success['tot_persen1']=0;
               }
               if($sumtc2>0){
                    $success['tot_persen2']=ceil(($sumtc2/$sumtw2)*100);
               }else{
                    $success['tot_persen2']=0;
               }
               if($sumtc3>0){
                    $success['tot_persen3']=ceil(($sumtc3/$sumtw3)*100);
               }else{
                    $success['tot_persen3']=0;
               }
               if($sumtc4>0){
                    $success['tot_persen4']=ceil(($sumtc4/$sumtw4)*100);
               }else{
                    $success['tot_persen4']=0;
               }
               
               $success['data']=$subdet;
          }
        return response()->json($success, 200);
    }
    public function get_data_kelurahan_sensitif(request $request,$id)
    {
          
        error_reporting(0);
        $t_header_id=max_header_id(2);
        $mst=HeaderSpesifikasi::find($t_header_id);
        $tahun=date('Y',strtotime($mst->tanggal));
        $token=config('app.key');
        if($request->indikator_id==3 || $request->indikator_id==4 ){
          $tw1=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw1');
          $tw2=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw2');
          $tw3=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw3');
          $tw4=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_tw4');
          $tc1=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw1');
          $tc2=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw2');
          $tc3=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw3');
          $tc4=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kecamatan_id',$id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_tw4');
          $success=[];
          if($tc1>0){
               $success['persen1']=(int) ceil(($tc1/$tw1)*100);
          }else{
               $success['persen1']=0;
          }
          if($tc2>0){
               $success['persen2']=(int) ceil(($tc2/$tw2)*100);
          }else{
               $success['persen2']=0;
          }
          if($tc3>0){
               $success['persen3']=(int) ceil(($tc3/$tw3)*100);
          }else{
               $success['persen3']=0;
          }
          if($tc4>0){
               $success['persen4']=(int) ceil(($tc4/$tw4)*100);
          }else{
               $success['persen4']=0;
          }
          $success['sasaran1']=$tw1;
          $success['sasaran2']=$tw2;
          $success['sasaran3']=$tw3;
          $success['sasaran4']=$tw4;
          $success['capaian1']=$tc1;
          $success['capaian2']=$tc2;
          $success['capaian3']=$tc3;
          $success['capaian4']=$tc4;
        }else{
               $data=Kelurahan::where('id_kec',$id)->orderBy('nama','Asc')->get();
               $success=[];
               $subdet=[];
               $tot_sas_jan=0;
               $tot_sas_feb=0;
               $tot_sas_mar=0;
               $tot_sas_apr=0;
               $tot_sas_mei=0;
               $tot_sas_jun=0;
               $tot_sas_jul=0;
               $tot_sas_agu=0;
               $tot_sas_sep=0;
               $tot_sas_okt=0;
               $tot_sas_nov=0;
               $tot_sas_des=0;
               $tot_cap_jan=0;
               $tot_cap_feb=0;
               $tot_cap_mar=0;
               $tot_cap_apr=0;
               $tot_cap_mei=0;
               $tot_cap_jun=0;
               $tot_cap_jul=0;
               $tot_cap_agu=0;
               $tot_cap_sep=0;
               $tot_cap_okt=0;
               $tot_cap_nov=0;
               $tot_cap_des=0;

               $tot_apbn_jan=0;
               $tot_apbn_feb=0;
               $tot_apbn_mar=0;
               $tot_apbn_apr=0;
               $tot_apbn_mei=0;
               $tot_apbn_jun=0;
               $tot_apbn_jul=0;
               $tot_apbn_agu=0;
               $tot_apbn_sep=0;
               $tot_apbn_okt=0;
               $tot_apbn_nov=0;
               $tot_apbn_des=0;
               $tot_apbd_jan=0;
               $tot_apbd_feb=0;
               $tot_apbd_mar=0;
               $tot_apbd_apr=0;
               $tot_apbd_mei=0;
               $tot_apbd_jun=0;
               $tot_apbd_jul=0;
               $tot_apbd_agu=0;
               $tot_apbd_sep=0;
               $tot_apbd_okt=0;
               $tot_apbd_nov=0;
               $tot_apbd_des=0;
               $tot_jumlah=0;
               foreach($data as $no=>$o){
                    
                         $jumlah=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('jumlah');
                         $odf=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('odf');
                         $twjan=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_jan');
                         $twfeb=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_feb');
                         $twmar=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_mar');
                         $twapr=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_apr');
                         $twmei=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_mei');
                         $twjun=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_jun');
                         $twjul=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_jul');
                         $twagu=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_agu');
                         $twsep=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_sep');
                         $twokt=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_okt');
                         $twnov=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_nov');
                         $twdes=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('sas_des');
                         
                         $apbnjan=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_jan');
                         $apbnfeb=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_feb');
                         $apbnmar=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_mar');
                         $apbnapr=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_apr');
                         $apbnmei=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_mei');
                         $apbnjun=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_jun');
                         $apbnjul=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_jul');
                         $apbnagu=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_agu');
                         $apbnsep=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_sep');
                         $apbnokt=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_okt');
                         $apbnnov=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_nov');
                         $apbndes=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbn_des');
                         
                         $apbdjan=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_jan');
                         $apbdfeb=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_feb');
                         $apbdmar=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_mar');
                         $apbdapr=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_apr');
                         $apbdmei=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_mei');
                         $apbdjun=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_jun');
                         $apbdjul=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_jul');
                         $apbdagu=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_agu');
                         $apbdsep=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_sep');
                         $apbdokt=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_okt');
                         $apbdnov=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_nov');
                         $apbddes=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('apbd_des');
                        
                         $tcjan=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_jan');
                         $tcfeb=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_feb');
                         $tcmar=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_mar');
                         $tcapr=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_apr');
                         $tcmei=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_mei');
                         $tcjun=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_jun');
                         $tcjul=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_jul');
                         $tcagu=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_agu');
                         $tcsep=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_sep');
                         $tcokt=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_okt');
                         $tcnov=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_nov');
                         $tcdes=SpesifikasiGizi::where('t_header_id',$t_header_id)->where('kelurahan_id',$o->id)->where('tahun',date('Y'))->where('indikator_id',$request->indikator_id)->sum('cap_des');
                         
                         $tot_sas_jan+=$twjan;
                         $tot_sas_feb+=$twfeb;
                         $tot_sas_mar+=$twmar;
                         $tot_sas_apr+=$twapr;
                         $tot_sas_mei+=$twmei;
                         $tot_sas_jun+=$twjun;
                         $tot_sas_jul+=$twjul;
                         $tot_sas_agu+=$twagu;
                         $tot_sas_sep+=$twsep;
                         $tot_sas_okt+=$twokt;
                         $tot_sas_nov+=$twnov;
                         $tot_sas_des+=$twdes;    
                         $tot_apbn_jan+=$apbnjan;
                         $tot_apbn_feb+=$apbnfeb;
                         $tot_apbn_mar+=$apbnmar;
                         $tot_apbn_apr+=$apbnapr;
                         $tot_apbn_mei+=$apbnmei;
                         $tot_apbn_jun+=$apbnjun;
                         $tot_apbn_jul+=$apbnjul;
                         $tot_apbn_agu+=$apbnagu;
                         $tot_apbn_sep+=$apbnsep;
                         $tot_apbn_okt+=$apbnokt;
                         $tot_apbn_nov+=$apbnnov;
                         $tot_apbn_des+=$apbndes;    
                         $tot_apbd_jan+=$apbdjan;
                         $tot_apbd_feb+=$apbdfeb;
                         $tot_apbd_mar+=$apbdmar;
                         $tot_apbd_apr+=$apbdapr;
                         $tot_apbd_mei+=$apbdmei;
                         $tot_apbd_jun+=$apbdjun;
                         $tot_apbd_jul+=$apbdjul;
                         $tot_apbd_agu+=$apbdagu;
                         $tot_apbd_sep+=$apbdsep;
                         $tot_apbd_okt+=$apbdokt;
                         $tot_apbd_nov+=$apbdnov;
                         $tot_apbd_des+=$apbddes;    
                         $tot_cap_jan+=$tcjan;
                         $tot_cap_feb+=$tcfeb;
                         $tot_cap_mar+=$tcmar;
                         $tot_cap_apr+=$tcapr;
                         $tot_cap_mei+=$tcmei;
                         $tot_cap_jun+=$tcjun;
                         $tot_cap_jul+=$tcjul;
                         $tot_cap_agu+=$tcagu;
                         $tot_cap_sep+=$tcsep;
                         $tot_cap_okt+=$tcokt;
                         $tot_cap_nov+=$tcnov;
                         $tot_cap_des+=$tcdes;    
                         $tot_jumlah+=$jumlah;    
                    
                    
                    
                    $sub['id']=$o->id;
                    $sub['tahun']=$tahun;
                    $sub['nama']=$o->nama;
                    $sub['jumlah']=(int) $jumlah;
                    $sub['odf']=(int) $odf;
                    $sub['sas_jan']=(int) $twjan;
                    $sub['sas_feb']=(int) $twfeb;
                    $sub['sas_mar']=(int) $twmar;
                    $sub['sas_apr']=(int) $twapr;
                    $sub['sas_mei']=(int) $twmei;
                    $sub['sas_jun']=(int) $twjun;
                    $sub['sas_jul']=(int) $twjul;
                    $sub['sas_agu']=(int) $twagu;
                    $sub['sas_sep']=(int) $twsep;
                    $sub['sas_okt']=(int) $twokt;
                    $sub['sas_nov']=(int) $twnov;
                    $sub['sas_des']=(int) $twdes;
                    $sub['apbn_jan']=(int) $apbnjan;
                    $sub['apbn_feb']=(int) $apbnfeb;
                    $sub['apbn_mar']=(int) $apbnmar;
                    $sub['apbn_apr']=(int) $apbnapr;
                    $sub['apbn_mei']=(int) $apbnmei;
                    $sub['apbn_jun']=(int) $apbnjun;
                    $sub['apbn_jul']=(int) $apbnjul;
                    $sub['apbn_agu']=(int) $apbnagu;
                    $sub['apbn_sep']=(int) $apbnsep;
                    $sub['apbn_okt']=(int) $apbnokt;
                    $sub['apbn_nov']=(int) $apbnnov;
                    $sub['apbn_des']=(int) $apbndes;
                    $sub['apbd_jan']=(int) $apbdjan;
                    $sub['apbd_feb']=(int) $apbdfeb;
                    $sub['apbd_mar']=(int) $apbdmar;
                    $sub['apbd_apr']=(int) $apbdapr;
                    $sub['apbd_mei']=(int) $apbdmei;
                    $sub['apbd_jun']=(int) $apbdjun;
                    $sub['apbd_jul']=(int) $apbdjul;
                    $sub['apbd_agu']=(int) $apbdagu;
                    $sub['apbd_sep']=(int) $apbdsep;
                    $sub['apbd_okt']=(int) $apbdokt;
                    $sub['apbd_nov']=(int) $apbdnov;
                    $sub['apbd_des']=(int) $apbddes;
                    $sub['cap_jan']=(int) $tcjan;
                    $sub['cap_feb']=(int) $tcfeb;
                    $sub['cap_mar']=(int) $tcmar;
                    $sub['cap_apr']=(int) $tcapr;
                    $sub['cap_mei']=(int) $tcmei;
                    $sub['cap_jun']=(int) $tcjun;
                    $sub['cap_jul']=(int) $tcjul;
                    $sub['cap_agu']=(int) $tcagu;
                    $sub['cap_sep']=(int) $tcsep;
                    $sub['cap_okt']=(int) $tcokt;
                    $sub['cap_nov']=(int) $tcnov;
                    $sub['cap_des']=(int) $tcdes;
                    
                    $subdet[]=$sub;
               }
               
               
               $success['data']=$subdet;
               $success['tot_sas_jan']=$tot_sas_jan;
               $success['tot_sas_feb']=$tot_sas_feb;
               $success['tot_sas_mar']=$tot_sas_mar;
               $success['tot_sas_apr']=$tot_sas_apr;
               $success['tot_sas_mei']=$tot_sas_mei;
               $success['tot_sas_jun']=$tot_sas_jun;
               $success['tot_sas_jul']=$tot_sas_jul;
               $success['tot_sas_agu']=$tot_sas_agu;
               $success['tot_sas_sep']=$tot_sas_sep;
               $success['tot_sas_okt']=$tot_sas_okt;
               $success['tot_sas_nov']=$tot_sas_nov;
               $success['tot_sas_des']=$tot_sas_des;
               $success['tot_apbn_jan']=$tot_apbn_jan;
               $success['tot_apbn_feb']=$tot_apbn_feb;
               $success['tot_apbn_mar']=$tot_apbn_mar;
               $success['tot_apbn_apr']=$tot_apbn_apr;
               $success['tot_apbn_mei']=$tot_apbn_mei;
               $success['tot_apbn_jun']=$tot_apbn_jun;
               $success['tot_apbn_jul']=$tot_apbn_jul;
               $success['tot_apbn_agu']=$tot_apbn_agu;
               $success['tot_apbn_sep']=$tot_apbn_sep;
               $success['tot_apbn_okt']=$tot_apbn_okt;
               $success['tot_apbn_nov']=$tot_apbn_nov;
               $success['tot_apbn_des']=$tot_apbn_des;
               $success['tot_apbd_jan']=$tot_apbd_jan;
               $success['tot_apbd_feb']=$tot_apbd_feb;
               $success['tot_apbd_mar']=$tot_apbd_mar;
               $success['tot_apbd_apr']=$tot_apbd_apr;
               $success['tot_apbd_mei']=$tot_apbd_mei;
               $success['tot_apbd_jun']=$tot_apbd_jun;
               $success['tot_apbd_jul']=$tot_apbd_jul;
               $success['tot_apbd_agu']=$tot_apbd_agu;
               $success['tot_apbd_sep']=$tot_apbd_sep;
               $success['tot_apbd_okt']=$tot_apbd_okt;
               $success['tot_apbd_nov']=$tot_apbd_nov;
               $success['tot_apbd_des']=$tot_apbd_des;
               $success['tot_cap_jan']=$tot_cap_jan;
               $success['tot_cap_feb']=$tot_cap_feb;
               $success['tot_cap_mar']=$tot_cap_mar;
               $success['tot_cap_apr']=$tot_cap_apr;
               $success['tot_cap_mei']=$tot_cap_mei;
               $success['tot_cap_jun']=$tot_cap_jun;
               $success['tot_cap_jul']=$tot_cap_jul;
               $success['tot_cap_agu']=$tot_cap_agu;
               $success['tot_cap_sep']=$tot_cap_sep;
               $success['tot_cap_okt']=$tot_cap_okt;
               $success['tot_cap_nov']=$tot_cap_nov;
               $success['tot_cap_des']=$tot_cap_des;
               $success['tot_jumlah']=$tot_jumlah;
          }
        return response()->json($success, 200);
    }
    public function get_data_kelurahan_all(request $request,$id)
    {
     // dd(array_triwulan(1));
          error_reporting(0);
          $query = Gizi::query();
          
          $data = $query->where('keyid',$request->bulan);
               
         
          if($request->tahun>0){
               $data = $query->where('tahun',$request->tahun);
          }else{
               $data = $query->where('tahun',date('Y'));
          }
          $data = $query->where('kecamatan_id',$id)->orderBy('kelurahan','Asc')->get();

          return Datatables::of($data)
     
         ->make(true);
    }
}
