<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\User;
use App\Models\ViewTps;
use App\Models\ViewUser;
use App\Models\Tps;
use App\Models\Accesstoken;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Events\KirimCreated; 
use Validator;
   
class UserController extends BaseController
{
    public function bunyikan(Request $request){
        $datanot='@order@';
        KirimCreated::dispatch($datanot);
    }
    public function profil(Request $request){
        $akses = ViewUser::where('id',$request->user()['id'])->first(); 
        $data=ViewTps::where('nik',$akses->username)->where('caleg_id',$akses->caleg_id)->firstOrfail();
        $success=[];
        $success['name']=$akses->name;
        $success['provinsi']=$akses->provinsi;
        $success['kota']=$akses->kota;
        $success['kecamatan']=$akses->kecamatan;
        $success['kelurahan']=$akses->kelurahan;
        $success['no_tps']=$akses->no_tps;
        $success['nama_caleg']=$akses->nama_caleg;
        $success['wilayah']=$akses->wilayah;
        $success['foto']=url_plug().'/attach/foto_caleg/'.$akses->foto_caleg;
        $success['target']=$data->rencana;
        $success['actual']=$data->actual;
        $success['total_pemilih']=$data->total_pemilih;
        return $this->sendResponse($success, 'success');
    }
    
    public function save_target(Request $request)
    {
        try {
            error_reporting(0);
            $akses = ViewUser::where('id',$request->user()['id'])->first(); 
            $data=ViewTps::where('nik',$akses->username)->where('caleg_id',$akses->caleg_id)->firstOrfail();
            $rules = [];
            $messages = [];
            
            $rules['target']= 'required';
            $messages['target.required']= 'Masukan Target';

        
            $validator = Validator::make($request->all(), $rules, $messages);
            $val=$validator->Errors();


            if ($validator->fails()) {
                    $error="";
                    foreach(parsing_validator($val) as $value){
                        
                        foreach($value as $isi){
                            $error.=$isi."\n";
                        }
                    }
                    return $this->sendResponseerror($error);
            }else{
                $save=Tps::where('nomor',$akses->tps_id)->where('caleg_id',$akses->caleg_id)->update(['rencana'=>$request->target]);
                $success=true;
                $datanot='@'.$akses->caleg_id.'@';
                KirimCreated::dispatch($datanot);
                return $this->sendResponse($success, 'success');
            }
        } catch (\Throwable $th) {
            return $this->sendResponseerror($th->getMessage(), $th->getCode());
        }
        
    }
    public function save_actual(Request $request)
    {
        try {
            error_reporting(0);
            $akses = ViewUser::where('id',$request->user()['id'])->first(); 
            $data=ViewTps::where('nik',$akses->username)->where('caleg_id',$akses->caleg_id)->firstOrfail();
            $rules = [];
            $messages = [];
            
            $rules['actual']= 'required';
            $messages['actual.required']= 'Masukan Actual';

        
            $validator = Validator::make($request->all(), $rules, $messages);
            $val=$validator->Errors();


            if ($validator->fails()) {
                    $error="";
                    foreach(parsing_validator($val) as $value){
                        
                        foreach($value as $isi){
                            $error.=$isi."\n";
                        }
                    }
                    return $this->sendResponseerror($error);
            }else{
                $save=Tps::where('nomor',$akses->tps_id)->where('caleg_id',$akses->caleg_id)->update(['actual'=>$request->actual]);
                $success=true;
                return $this->sendResponse($success, 'success');
            }
        } catch (\Throwable $th) {
            return $this->sendResponseerror($th->getMessage(), $th->getCode());
        }
        
    }
    public function save_total_pemilih(Request $request)
    {
        try {
            error_reporting(0);
            $akses = ViewUser::where('id',$request->user()['id'])->first(); 
            $data=ViewTps::where('nik',$akses->username)->where('caleg_id',$akses->caleg_id)->firstOrfail();
            $rules = [];
            $messages = [];
            
            $rules['total_pemilih']= 'required';
            $messages['total_pemilih.required']= 'Masukan Jumlah Suara';

        
            $validator = Validator::make($request->all(), $rules, $messages);
            $val=$validator->Errors();


            if ($validator->fails()) {
                    $error="";
                    foreach(parsing_validator($val) as $value){
                        
                        foreach($value as $isi){
                            $error.=$isi."\n";
                        }
                    }
                    return $this->sendResponseerror($error);
            }else{
                $save=Tps::where('nomor',$akses->tps_id)->where('caleg_id',$akses->caleg_id)->update(['total_pemilih'=>$request->total_pemilih]);
                $success=true;
                return $this->sendResponse($success, 'success');
            }
        } catch (\Throwable $th) {
            return $this->sendResponseerror($th->getMessage(), $th->getCode());
        }
        
    }
    public function cekout(Request $request)
    {
        try {
            error_reporting(0);
                $auth = $request->user(); 
                $terms=Viewjadwalsales::where('NoU',$request->NoU)->first();
                $customer=Customer::where('KD_Customer',$request->KD_Customer)->first();
                $KD_Transaksi=penomoran_so();
                
                
                
                $save=Soheader::UpdateOrcreate([
                    'KD_Customer'=>$request->KD_Customer,
                    'Tanggal'=>date('Y-m-d H:i:s'),
                    
                    'status_data'=>1,
                
                    'KD_Salesman'=>$auth->username,
                    'KD_Transaksi'=>$KD_Transaksi,
                    'Jatuh_Tempo'=>tanggal_tempo('+'.$terms->Term),
                    'Term'=>$terms->Term,
                    'kd_divisi'=>$terms->kd_divisi,
                    'Customer'=>$customer->Perusahaan,
                    'Customer'=>$customer->Perusahaan,
                ]);
                $detail=Sodetail::where('kd_transaksi',$mst->KD_Transaksi)->where('sts',0)->update([
                    'sts'=>1,
                    'Tanggal'=>date('Y-m-d H:i:s'),
                    'kd_transaksi'=>$KD_Transaksi,
                ]);

                $datanot='@order@';
                KirimCreated::dispatch($datanot);
                $success=true;
                return $this->sendResponse($success, 'success');
           
        } catch (\Throwable $th) {
            return $this->sendResponseerror($th->getMessage(), $th->getCode());
        }
        
    }

    public function absen(Request $request)
    {
        try {
            error_reporting(0);
            $auth = $request->user(); 
            $rules = [];
            $messages = [];
            
            $rules['KD_Barang']= 'required';
            $messages['KD_Barang.required']= 'Pilih Barang';

            $rules['qty']= 'required';
            $messages['qty.required']= 'Pilih Jumlah';

            $rules['NoU']= 'required';
            $messages['NoU.required']= 'Pilih KD_Customer';
            
        
            $validator = Validator::make($request->all(), $rules, $messages);
            $val=$validator->Errors();


            if ($validator->fails()) {
                    $error="";
                    foreach(parsing_validator($val) as $value){
                        
                        foreach($value as $isi){
                            $error.=$isi."\n";
                        }
                    }
                    return $this->sendResponseerror($error);
            }else{
                
                $barang=Viewbarang::where('KD_Barang',$request->KD_Barang)->firstOrfail();
                $terms=Viewjadwalsales::where('NoU',$request->NoU)->first();
                $customer=Customer::where('KD_Customer',$terms->KD_Customer)->first();
                $cek=Soheader::where('KD_Customer',$terms->KD_Customer)->where('status_data',0)->count();
                if($cek>0){
                    $mst=Soheader::where('KD_Customer',$terms->KD_Customer)->where('status_data',0)->firstOrfail();
                    $KD_Transaksi=$mst->KD_Transaksi;
                }else{
                    $KD_Transaksi=penomoran_so();
                }
                $save=Soheader::UpdateOrcreate([
                    'KD_Customer'=>$terms->KD_Customer,
                    'NoU'=>$terms->NoU,
                    'tanggal'=>date('Y-m-d'),
                    
                    'status_data'=>0,
                ],[
                    'KD_Salesman'=>$auth->username,
                    'KD_Transaksi'=>$KD_Transaksi,
                    'Jatuh_Tempo'=>tanggal_tempo('+'.$terms->Term),
                    'Term'=>$terms->Term,
                    'kd_divisi'=>$terms->kd_divisi,
                    'Customer'=>$customer->Perusahaan,
                    'Customer'=>$customer->Perusahaan,
                    'waktu'=>date('Y-m-d H:i:s'),
                ]);

                if($request->qty_free==null || $request->qty_free=='0'){
                    $Qtyfree=0;
                }else{
                    $Qtyfree=$request->qty_free;
                }
                if($request->discon==null || $request->discon=='0'){
                    $discon=0;
                }else{
                    $discon=$request->discon;
                }
                $harga=($barang['harga_ke4']-$discon);
                $save2=Sodetail::UpdateOrcreate([
                    'NoU'=>$request->NoU,
                    'KD_Barang'=>$request->KD_Barang,
                    'kd_transaksi'=>$KD_Transaksi,
                    
                ],[
                    'qty'.$barang->hargamunculsatuanke=>$request->qty,
                    'Disc'.$barang->hargamunculsatuanke=>$request->discon,
                    'discon'=>$discon,
                    'Satuan'=>$request->Satuan,
                    'Harga_Satuan'=>$harga,
                    'Qty'=>$request->qty,
                    'Qtyfree'=>$Qtyfree,
                    'total'=>($harga*$request->qty),
                ]);
                $success=true;
                return $this->sendResponse($success, 'success');
            }
        } catch (\Throwable $th) {
            return $this->sendResponseerror($th->getMessage(), $th->getCode());
        }
        
    }

    public function hapus(Request $request)
    {
        $akses = $request->user(); 
        $delete=Sodetail::where('id',$request->id)->delete();
        $success=true;
        return $this->sendResponse($success, 'success');
    }
    public function keranjang(Request $request)
    {
        $akses = $request->user(); 
        if($request->page==""){
            $page=1;
        }else{
            $page=$request->page;
        }
        $act=$request->act;
        
        $query=Vieworder::query();
        $get=$query->where('sts',$act)->where('KD_Customer',$request->KD_Customer)->orderBy('sts','Asc')->paginate(20);
        $sum=$query->where('sts',$act)->where('KD_Customer',$request->KD_Customer)->sum('total');
        $sumnot=$query->where('KD_Customer',$request->KD_Customer)->where('sts',0)->sum('total');
        $queryall=Vieworder::query();
        $cek=$queryall->where('KD_Customer',$request->KD_Customer)->count();
        $cekin=$queryall->where('KD_Customer',$request->KD_Customer)->where('sts',0)->count();
        $success['total_page'] =  ceil($cek/10);
        $success['KD_Customer'] =  $request->KD_Customer;
        $success['total_bayar'] =  no_decimal($sum);
        $success['total_belum_bayar'] =  no_decimal($sumnot);
        $success['total_item'] =  $cek;
        $success['total_cekin'] =  $cekin;
        $success['current_page'] =  $page;
        $col=[];
        foreach($get as $o){
           $sub=[];
                $cl=[];
                $cl['id'] =$o->id;
                $cl['sts'] =$o->sts;
                $cl['kd_transaksi'] =$o->kd_transaksi;
                $cl['KD_Barang'] =$o->KD_Barang;
                $cl['tanggal'] =tanggal_indo($o->Tanggal);
                $cl['Nama_Barang'] = $o->Nama_Barang;
                $cl['Satuan'] = $o->Satuan;
                $cl['discon'] = $o->discon;
                $cl['harga_satuan'] = no_decimal($o->Harga_Satuan);
                $cl['Qty'] = $o->Qty;
                $cl['total'] = $o->total;
                if($o->photo!=null){
                    $cl['photo'] = url_plug().'/_file_foto/'.$o->photo;
                }else{
                    $cl['photo'] = url_plug().'/_file_foto/example.png';
                }
                
                $sub=$cl;  
                
            $col[]=$sub;
        }
        
        $success['item'] =  $col;
        
        

        return $this->sendResponse($success, 'success');
    }

    
}