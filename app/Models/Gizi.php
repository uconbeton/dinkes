<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gizi extends Model
{
    use HasFactory;
    protected $table = 't_gizi_warga';
    protected $guarded = ['id'];
    public $timestamps = false;
    function kecamatan(){
        return $this->belongsTo('App\Models\Kecamatan','id_kec','id');
    }
    function kelurahan(){
        return $this->belongsTo('App\Models\Kelurahan','kelurahan_id','id');
    }
    // function mpendidikan(){
    //     return $this->belongsTo('App\Models\Pendidikan','pendidikan_id','id');
    // }
}
