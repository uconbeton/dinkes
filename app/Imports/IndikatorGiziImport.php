<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\Gizi;
use App\Models\SpesifikasiGizi;
use App\Models\VwKelurahan;
use App\Models\VwKecamatan;

class IndikatorGiziImport implements ToModel, WithStartRow
{
    protected $tahun;
    protected $t_header_id;
    public function __construct(int $tahun,$t_header_id)
    {
        $this->tahun = $tahun; 
        $this->t_header_id = $t_header_id; 
    }
    public function collection(Collection $collection)
    {
        //
    }

    public function model(array $row)
    {
        error_reporting(0);
        if($row[0]==1){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>1,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[2],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'sas_jan'=> (int) $row[3],
                        'cap_jan'=> (int) $row[4],
                        
                        'sas_feb'=> (int) $row[5],
                        'cap_feb'=> (int) $row[6],
                        
                        'sas_mar'=> (int) $row[7],
                        'cap_mar'=> (int) $row[8],

                        'sas_tw1'=> (int) ((int) $row[5]+(int) $row[5]+(int) $row[7]),
                        'cap_tw1'=> (int) ((int) $row[4]+(int) $row[6]+(int) $row[8]),
                        
                        'sas_apr'=> (int) $row[12],
                        'cap_apr'=> (int) $row[13],
                        
                        'sas_mei'=> (int) $row[14],
                        'cap_mei'=> (int) $row[15],
                        
                        'sas_jun'=> (int) $row[16],
                        'cap_jun'=> (int) $row[17],

                        'sas_tw2'=> (int) ((int) $row[12]+(int) $row[14]+(int) $row[16]),
                        'cap_tw2'=> (int) ((int) $row[13]+(int) $row[15]+(int) $row[17]),
                        
                        'sas_jul'=> (int) $row[21],
                        'cap_jul'=> (int) $row[22],
                        
                        'sas_agu'=> (int) $row[23],
                        'cap_agu'=> (int) $row[24],
                        
                        'sas_sep'=> (int) $row[25],
                        'cap_sep'=> (int) $row[26],

                        'sas_tw3'=> (int) ((int) $row[21]+(int) $row[23]+(int) $row[25]),
                        'cap_tw3'=> (int) ((int) $row[22]+(int) $row[24]+(int) $row[26]),
                        
                        'sas_okt'=> (int) $row[30],
                        'cap_okt'=> (int) $row[31],
                        
                        'sas_nov'=> (int) $row[32],
                        'cap_nov'=> (int) $row[33],
                        
                        'sas_des'=> (int) $row[34],
                        'cap_des'=> (int) $row[55],
                        
                        'sas_tw4'=> (int) ((int) $row[30]+(int) $row[32]+(int) $row[34]),
                        'cap_tw4'=> (int) ((int) $row[31]+(int) $row[33]+(int) $row[35]),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==2){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        't_header_id'=>$this->t_header_id,
                        'indikator_id'=>2,
                        'tahun'=>$this->tahun,
                        'kelurahan'=>$row[2],
                        'kelurahan_id'=>$mst->id,
                    ],
                    ['nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'sas_jan'=> (int) $row[3],
                        'cap_jan'=> (int) $row[4],
                        
                        'sas_feb'=> (int) $row[5],
                        'cap_feb'=> (int) $row[6],
                        
                        'sas_mar'=> (int) $row[7],
                        'cap_mar'=> (int) $row[8],

                        'sas_tw1'=> (int) ((int) $row[5]+(int) $row[5]+(int) $row[7]),
                        'cap_tw1'=> (int) ((int) $row[4]+(int) $row[6]+(int) $row[8]),
                        
                        'sas_apr'=> (int) $row[12],
                        'cap_apr'=> (int) $row[13],
                        
                        'sas_mei'=> (int) $row[14],
                        'cap_mei'=> (int) $row[15],
                        
                        'sas_jun'=> (int) $row[16],
                        'cap_jun'=> (int) $row[17],

                        'sas_tw2'=> (int) ((int) $row[12]+(int) $row[14]+(int) $row[16]),
                        'cap_tw2'=> (int) ((int) $row[13]+(int) $row[15]+(int) $row[17]),
                        
                        'sas_jul'=> (int) $row[21],
                        'cap_jul'=> (int) $row[22],
                        
                        'sas_agu'=> (int) $row[23],
                        'cap_agu'=> (int) $row[24],
                        
                        'sas_sep'=> (int) $row[25],
                        'cap_sep'=> (int) $row[26],

                        'sas_tw3'=> (int) ((int) $row[21]+(int) $row[23]+(int) $row[25]),
                        'cap_tw3'=> (int) ((int) $row[22]+(int) $row[24]+(int) $row[26]),
                        
                        'sas_okt'=> (int) $row[30],
                        'cap_okt'=> (int) $row[31],
                        
                        'sas_nov'=> (int) $row[32],
                        'cap_nov'=> (int) $row[33],
                        
                        'sas_des'=> (int) $row[34],
                        'cap_des'=> (int) $row[55],
                        
                        'sas_tw4'=> (int) ((int) $row[30]+(int) $row[32]+(int) $row[34]),
                        'cap_tw4'=> (int) ((int) $row[31]+(int) $row[33]+(int) $row[35]),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==3){
            $cek=VwKecamatan::where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKecamatan::where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id,
                        'indikator_id'=>3,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        
                    ],
                    [
                        'nomor'=>$mst->id,
                        'kecamatan'=>$mst->nama,
                        'sas_jan'=>0,
                        'cap_jan'=>0,
                        
                        'sas_feb'=>0,
                        'cap_feb'=>0,
                        
                        'sas_mar'=>(int) $row[3],
                        'cap_mar'=>(int) $row[4],

                        'sas_tw1'=>(int) $row[3],
                        'cap_tw1'=>(int) $row[4],
                        
                        'sas_apr'=>0,
                        'cap_apr'=>0,
                        
                        'sas_mei'=>0,
                        'cap_mei'=>0,
                        
                        'sas_jun'=>(int) $row[8],
                        'cap_jun'=>(int) $row[9],

                        'sas_tw2'=>(int) $row[8],
                        'cap_tw2'=>(int) $row[9],
                        
                        'sas_jul'=>0,
                        'cap_jul'=>0,
                        
                        'sas_agu'=>0,
                        'cap_agu'=>0,
                        
                        'sas_sep'=>(int) $row[13],
                        'cap_sep'=>(int) $row[14],

                        'sas_tw3'=>(int) $row[13],
                        'cap_tw3'=>(int) $row[14],
                        
                        'sas_okt'=>0,
                        'cap_okt'=>0,
                        
                        'sas_nov'=>0,
                        'cap_nov'=>0,
                        
                        'sas_des'=>(int) $row[19],
                        'cap_des'=>(int) $row[18],
                        
                        'sas_tw3'=>(int) $row[19],
                        'cap_tw3'=>(int) $row[18],


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==4){
            $cek=VwKecamatan::where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKecamatan::where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id,
                        'indikator_id'=>4,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                    ],
                    [   'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->nama,
                        'sas_jan'=>0,
                        'cap_jan'=>0,
                        
                        'sas_feb'=>(int) $row[3],
                        'cap_feb'=>(int) $row[4],
                        
                        'sas_mar'=>0,
                        'cap_mar'=>0,

                        'sas_tw1'=>(int) $row[3],
                        'cap_tw1'=>(int) $row[4],
                        
                        'sas_apr'=>0,
                        'cap_apr'=>0,
                        
                        'sas_mei'=>0,
                        'cap_mei'=>0,
                        
                        'sas_jun'=>0,
                        'cap_jun'=>0,

                        'sas_tw2'=>(int) $row[8],
                        'cap_tw2'=>(int) $row[9],
                        
                        'sas_jul'=>0,
                        'cap_jul'=>0,
                        
                        'sas_agu'=>(int) $row[8],
                        'cap_agu'=>(int) $row[9],
                        
                        'sas_sep'=>0,
                        'cap_sep'=>0,

                        'sas_tw3'=>0,
                        'cap_tw3'=>0,
                        
                        'sas_okt'=>0,
                        'cap_okt'=>0,
                        
                        'sas_nov'=>0,
                        'cap_nov'=>0,
                        
                        'sas_des'=>0,
                        'cap_des'=>0,
                        
                        'sas_tw3'=>0,
                        'cap_tw3'=>0,


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==5){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>5,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[2],
                        'kelurahan_id'=>$mst->id,
                    ],
                    ['nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'sas_jan'=> (int) $row[3],
                        'cap_jan'=> (int) $row[4],
                        
                        'sas_feb'=> (int) $row[5],
                        'cap_feb'=> (int) $row[6],
                        
                        'sas_mar'=> (int) $row[7],
                        'cap_mar'=> (int) $row[8],

                        'sas_tw1'=> (int) ((int) $row[5]+(int) $row[5]+(int) $row[7]),
                        'cap_tw1'=> (int) ((int) $row[4]+(int) $row[6]+(int) $row[8]),
                        
                        'sas_apr'=> (int) $row[12],
                        'cap_apr'=> (int) $row[13],
                        
                        'sas_mei'=> (int) $row[14],
                        'cap_mei'=> (int) $row[15],
                        
                        'sas_jun'=> (int) $row[16],
                        'cap_jun'=> (int) $row[17],

                        'sas_tw2'=> (int) ((int) $row[12]+(int) $row[14]+(int) $row[16]),
                        'cap_tw2'=> (int) ((int) $row[13]+(int) $row[15]+(int) $row[17]),
                        
                        'sas_jul'=> (int) $row[21],
                        'cap_jul'=> (int) $row[22],
                        
                        'sas_agu'=> (int) $row[23],
                        'cap_agu'=> (int) $row[24],
                        
                        'sas_sep'=> (int) $row[25],
                        'cap_sep'=> (int) $row[26],

                        'sas_tw3'=> (int) ((int) $row[21]+(int) $row[23]+(int) $row[25]),
                        'cap_tw3'=> (int) ((int) $row[22]+(int) $row[24]+(int) $row[26]),
                        
                        'sas_okt'=> (int) $row[30],
                        'cap_okt'=> (int) $row[31],
                        
                        'sas_nov'=> (int) $row[32],
                        'cap_nov'=> (int) $row[33],
                        
                        'sas_des'=> (int) $row[34],
                        'cap_des'=> (int) $row[55],
                        
                        'sas_tw4'=> (int) ((int) $row[30]+(int) $row[32]+(int) $row[34]),
                        'cap_tw4'=> (int) ((int) $row[31]+(int) $row[33]+(int) $row[35]),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==6){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>6,
                        't_header_id'=>$this->t_header_id,
                        'tahun'=>$this->tahun,
                        'kelurahan'=>$row[2],
                        'kelurahan_id'=>$mst->id,
                    ],
                    ['nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'sas_jan'=> (int) $row[3],
                        'cap_jan'=> (int) $row[4],
                        
                        'sas_feb'=> (int) $row[5],
                        'cap_feb'=> (int) $row[6],
                        
                        'sas_mar'=> (int) $row[7],
                        'cap_mar'=> (int) $row[8],

                        'sas_tw1'=> (int) ((int) $row[5]+(int) $row[5]+(int) $row[7]),
                        'cap_tw1'=> (int) ((int) $row[4]+(int) $row[6]+(int) $row[8]),
                        
                        'sas_apr'=> (int) $row[12],
                        'cap_apr'=> (int) $row[13],
                        
                        'sas_mei'=> (int) $row[14],
                        'cap_mei'=> (int) $row[15],
                        
                        'sas_jun'=> (int) $row[16],
                        'cap_jun'=> (int) $row[17],

                        'sas_tw2'=> (int) ((int) $row[12]+(int) $row[14]+(int) $row[16]),
                        'cap_tw2'=> (int) ((int) $row[13]+(int) $row[15]+(int) $row[17]),
                        
                        'sas_jul'=> (int) $row[21],
                        'cap_jul'=> (int) $row[22],
                        
                        'sas_agu'=> (int) $row[23],
                        'cap_agu'=> (int) $row[24],
                        
                        'sas_sep'=> (int) $row[25],
                        'cap_sep'=> (int) $row[26],

                        'sas_tw3'=> (int) ((int) $row[21]+(int) $row[23]+(int) $row[25]),
                        'cap_tw3'=> (int) ((int) $row[22]+(int) $row[24]+(int) $row[26]),
                        
                        'sas_okt'=> (int) $row[30],
                        'cap_okt'=> (int) $row[31],
                        
                        'sas_nov'=> (int) $row[32],
                        'cap_nov'=> (int) $row[33],
                        
                        'sas_des'=> (int) $row[34],
                        'cap_des'=> (int) $row[55],
                        
                        'sas_tw4'=> (int) ( (int) $row[30]+ (int) $row[32]+ (int) $row[34]),
                        'cap_tw4'=> (int) ( (int) $row[31]+ (int) $row[33]+ (int) $row[35]),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==7){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>7,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[2],
                        'kelurahan_id'=>$mst->id,
                    ],
                    ['nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'sas_jan'=> (int) $row[3],
                        'cap_jan'=> (int) $row[4],
                        
                        'sas_feb'=> (int) $row[5],
                        'cap_feb'=> (int) $row[6],
                        
                        'sas_mar'=> (int) $row[7],
                        'cap_mar'=> (int) $row[8],

                        'sas_tw1'=> (int) ((int) $row[5]+(int) $row[5]+(int) $row[7]),
                        'cap_tw1'=> (int) ((int) $row[4]+(int) $row[6]+(int) $row[8]),
                        
                        'sas_apr'=> (int) $row[12],
                        'cap_apr'=> (int) $row[13],
                        
                        'sas_mei'=> (int) $row[14],
                        'cap_mei'=> (int) $row[15],
                        
                        'sas_jun'=> (int) $row[16],
                        'cap_jun'=> (int) $row[17],

                        'sas_tw2'=> (int) ( (int) $row[12]+ (int) $row[14]+ (int) $row[16]),
                        'cap_tw2'=> (int) ( (int) $row[13]+ (int) $row[15]+ (int) $row[17]),
                        
                        'sas_jul'=> (int) $row[21],
                        'cap_jul'=> (int) $row[22],
                        
                        'sas_agu'=> (int) $row[23],
                        'cap_agu'=> (int) $row[24],
                        
                        'sas_sep'=> (int) $row[25],
                        'cap_sep'=> (int) $row[26],

                        'sas_tw3'=> (int) ((int) $row[21]+(int) $row[23]+(int) $row[25]),
                        'cap_tw3'=> (int) ((int) $row[22]+(int) $row[24]+(int) $row[26]),
                        
                        'sas_okt'=> (int) $row[30],
                        'cap_okt'=> (int) $row[31],
                        
                        'sas_nov'=> (int) $row[32],
                        'cap_nov'=> (int) $row[33],
                        
                        'sas_des'=> (int) $row[34],
                        'cap_des'=> (int) $row[55],
                        
                        'sas_tw4'=> (int) ( (int) $row[30]+ (int) $row[32]+ (int) $row[34]),
                        'cap_tw4'=> (int) ( (int) $row[31]+ (int) $row[33]+ (int) $row[35]),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==8){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>8,
                        't_header_id'=>$this->t_header_id,
                        'tahun'=>$this->tahun,
                        'kelurahan'=>$row[2],
                        'kelurahan_id'=>$mst->id,
                    ],
                    ['nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'sas_jan'=> (int) $row[3],
                        'cap_jan'=> (int) $row[4],
                        
                        'sas_feb'=> (int) $row[5],
                        'cap_feb'=> (int) $row[6],
                        
                        'sas_mar'=> (int) $row[7],
                        'cap_mar'=> (int) $row[8],

                        'sas_tw1'=> (int) ((int) $row[5]+(int) $row[5]+(int) $row[7]),
                        'cap_tw1'=> (int) ((int) $row[4]+(int) $row[6]+(int) $row[8]),
                        
                        'sas_apr'=> (int) $row[12],
                        'cap_apr'=> (int) $row[13],
                        
                        'sas_mei'=> (int) $row[14],
                        'cap_mei'=> (int) $row[15],
                        
                        'sas_jun'=> (int) $row[16],
                        'cap_jun'=> (int) $row[17],

                        'sas_tw2'=> (int) ((int) $row[12]+(int) $row[14]+(int) $row[16]),
                        'cap_tw2'=> (int) ((int) $row[13]+(int) $row[15]+(int) $row[17]),
                        
                        'sas_jul'=> (int) $row[21],
                        'cap_jul'=> (int) $row[22],
                        
                        'sas_agu'=> (int) $row[23],
                        'cap_agu'=> (int) $row[24],
                        
                        'sas_sep'=> (int) $row[25],
                        'cap_sep'=> (int) $row[26],

                        'sas_tw3'=> (int) ((int) $row[21]+(int) $row[23]+(int) $row[25]),
                        'cap_tw3'=> (int) ((int) $row[22]+(int) $row[24]+(int) $row[26]),
                        
                        'sas_okt'=> (int) $row[30],
                        'cap_okt'=> (int) $row[31],
                        
                        'sas_nov'=> (int) $row[32],
                        'cap_nov'=> (int) $row[33],
                        
                        'sas_des'=> (int) $row[34],
                        'cap_des'=> (int) $row[55],
                        
                        'sas_tw4'=> (int) ((int) $row[30]+(int) $row[32]+(int) $row[34]),
                        'cap_tw4'=> (int) ((int) $row[31]+(int) $row[33]+(int) $row[35]),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==9){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[2])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>9,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[2],
                        'kelurahan_id'=>$mst->id,
                    ],
                    ['nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'sas_jan'=> (int) $row[3],
                        'cap_jan'=> (int) $row[4],
                        
                        'sas_feb'=> (int) $row[5],
                        'cap_feb'=> (int) $row[6],
                        
                        'sas_mar'=> (int) $row[7],
                        'cap_mar'=> (int) $row[8],

                        'sas_tw1'=> (int) ((int) $row[5]+(int) $row[5]+(int) $row[7]),
                        'cap_tw1'=> (int) ((int) $row[4]+(int) $row[6]+(int) $row[8]),
                        
                        'sas_apr'=> (int) $row[12],
                        'cap_apr'=> (int) $row[13],
                        
                        'sas_mei'=> (int) $row[14],
                        'cap_mei'=> (int) $row[15],
                        
                        'sas_jun'=> (int) $row[16],
                        'cap_jun'=> (int) $row[17],

                        'sas_tw2'=> (int) ((int) $row[12]+(int) $row[14]+(int) $row[16]),
                        'cap_tw2'=> (int) ((int) $row[13]+(int) $row[15]+(int) $row[17]),
                        
                        'sas_jul'=> (int) $row[21],
                        'cap_jul'=> (int) $row[22],
                        
                        'sas_agu'=> (int) $row[23],
                        'cap_agu'=> (int) $row[24],
                        
                        'sas_sep'=> (int) $row[25],
                        'cap_sep'=> (int) $row[26],

                        'sas_tw3'=> (int) ((int) $row[21]+(int) $row[23]+(int) $row[25]),
                        'cap_tw3'=> (int) ((int) $row[22]+(int) $row[24]+(int) $row[26]),
                        
                        'sas_okt'=> (int) $row[30],
                        'cap_okt'=> (int) $row[31],
                        
                        'sas_nov'=> (int) $row[32],
                        'cap_nov'=> (int) $row[33],
                        
                        'sas_des'=> (int) $row[34],
                        'cap_des'=> (int) $row[55],
                        
                        'sas_tw4'=> (int) ((int) $row[30]+(int) $row[32]+(int) $row[34]),
                        'cap_tw4'=> (int) ((int) $row[31]+(int) $row[33]+(int) $row[35]),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
            
        
    }

    public function startRow(): int
    {
        return 3;
    }
}
