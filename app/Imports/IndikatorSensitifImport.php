<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\Gizi;
use App\Models\SpesifikasiGizi;
use App\Models\VwKelurahan;
use App\Models\VwKecamatan;

class IndikatorSensitifImport implements ToModel, WithStartRow,WithCalculatedFormulas
{
    protected $tahun;
    protected $t_header_id;
    public function __construct(int $tahun,$t_header_id)
    {
        $this->tahun = $tahun; 
        $this->t_header_id = $t_header_id; 
    }
    public function collection(Collection $collection)
    {
        //
    }

    public function model(array $row)
    {
        error_reporting(0);
        if($row[0]==1){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>10,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> 0,
                        'sas_jan'=> (int) ubah_uang($row[4]),
                        'cap_jan'=> (int) ubah_uang($row[5]),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ubah_uang($row[8]),
                        
                        'sas_mar'=> (int) ubah_uang($row[10]),
                        'cap_mar'=> (int) ubah_uang($row[11]),

                        'sas_tw1'=> (int) ((int) ubah_uang($row[4])+(int) ubah_uang($row[7])+(int) ubah_uang($row[10])),
                        'cap_tw1'=> (int) ((int) ubah_uang($row[5])+(int) ubah_uang($row[8])+(int) ubah_uang($row[11])),
                        
                        'sas_apr'=> (int) ubah_uang($row[13]),
                        'cap_apr'=> (int) ubah_uang($row[14]),
                        
                        'sas_mei'=> (int) ubah_uang($row[16]),
                        'cap_mei'=> (int) ubah_uang($row[17]),
                        
                        'sas_jun'=> (int) ubah_uang($row[19]),
                        'cap_jun'=> (int) ubah_uang($row[20]),

                        'sas_tw2'=> (int) ((int) ubah_uang($row[13])+(int) ubah_uang($row[16])+(int) ubah_uang($row[19])),
                        'cap_tw2'=> (int) ((int) ubah_uang($row[14])+(int) ubah_uang($row[17])+(int) ubah_uang($row[20])),
                        
                        'sas_jul'=> (int) ubah_uang($row[22]),
                        'cap_jul'=> (int) ubah_uang($row[23]),
                        
                        'sas_agu'=> (int) ubah_uang($row[25]),
                        'cap_agu'=> (int) ubah_uang($row[26]),
                        
                        'sas_sep'=> (int) ubah_uang($row[29]),
                        'cap_sep'=> (int) ubah_uang($row[30]),

                        'sas_tw3'=> (int) ((int) ubah_uang($row[22])+(int) ubah_uang($row[25])+(int) ubah_uang($row[29])),
                        'cap_tw3'=> (int) ((int) ubah_uang($row[23])+(int) ubah_uang($row[26])+(int) ubah_uang($row[30])),
                        
                        'sas_okt'=> (int) ubah_uang($row[32]),
                        'cap_okt'=> (int) ubah_uang($row[33]),
                        
                        'sas_nov'=> (int) ubah_uang($row[35]),
                        'cap_nov'=> (int) ubah_uang($row[36]),
                        
                        'sas_des'=> (int) ubah_uang($row[38]),
                        'cap_des'=> (int) ubah_uang($row[39]),
                        
                        'sas_tw4'=> (int) ((int) ubah_uang($row[32])+(int) ubah_uang($row[35])+(int) ubah_uang($row[38])),
                        'cap_tw4'=> (int) ((int) ubah_uang($row[33])+(int) ubah_uang($row[36])+(int) ubah_uang($row[39])),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==2){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>11,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> 0,
                        'sas_jan'=> (int) ubah_uang($row[4]),
                        'cap_jan'=> (int) ubah_uang($row[5]),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ubah_uang($row[8]),
                        
                        'sas_mar'=> (int) ubah_uang($row[10]),
                        'cap_mar'=> (int) ubah_uang($row[11]),

                        'sas_tw1'=> (int) ((int) ubah_uang($row[4])+(int) ubah_uang($row[7])+(int) ubah_uang($row[10])),
                        'cap_tw1'=> (int) ((int) ubah_uang($row[5])+(int) ubah_uang($row[8])+(int) ubah_uang($row[11])),
                        
                        'sas_apr'=> (int) ubah_uang($row[13]),
                        'cap_apr'=> (int) ubah_uang($row[14]),
                        
                        'sas_mei'=> (int) ubah_uang($row[16]),
                        'cap_mei'=> (int) ubah_uang($row[17]),
                        
                        'sas_jun'=> (int) ubah_uang($row[19]),
                        'cap_jun'=> (int) ubah_uang($row[20]),

                        'sas_tw2'=> (int) ((int) ubah_uang($row[13])+(int) ubah_uang($row[16])+(int) ubah_uang($row[19])),
                        'cap_tw2'=> (int) ((int) ubah_uang($row[14])+(int) ubah_uang($row[17])+(int) ubah_uang($row[20])),
                        
                        'sas_jul'=> (int) ubah_uang($row[22]),
                        'cap_jul'=> (int) ubah_uang($row[23]),
                        
                        'sas_agu'=> (int) ubah_uang($row[25]),
                        'cap_agu'=> (int) ubah_uang($row[26]),
                        
                        'sas_sep'=> (int) ubah_uang($row[29]),
                        'cap_sep'=> (int) ubah_uang($row[30]),

                        'sas_tw3'=> (int) ((int) ubah_uang($row[22])+(int) ubah_uang($row[25])+(int) ubah_uang($row[29])),
                        'cap_tw3'=> (int) ((int) ubah_uang($row[23])+(int) ubah_uang($row[26])+(int) ubah_uang($row[30])),
                        
                        'sas_okt'=> (int) ubah_uang($row[32]),
                        'cap_okt'=> (int) ubah_uang($row[33]),
                        
                        'sas_nov'=> (int) ubah_uang($row[35]),
                        'cap_nov'=> (int) ubah_uang($row[36]),
                        
                        'sas_des'=> (int) ubah_uang($row[38]),
                        'cap_des'=> (int) ubah_uang($row[39]),
                        
                        'sas_tw4'=> (int) ((int) ubah_uang($row[32])+(int) ubah_uang($row[35])+(int) ubah_uang($row[38])),
                        'cap_tw4'=> (int) ((int) ubah_uang($row[33])+(int) ubah_uang($row[36])+(int) ubah_uang($row[39])),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==3){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>12,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> 0,
                        'sas_jan'=> (int) ubah_uang($row[4]),
                        'cap_jan'=> (int) ubah_uang($row[5]),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ubah_uang($row[8]),
                        
                        'sas_mar'=> (int) ubah_uang($row[10]),
                        'cap_mar'=> (int) ubah_uang($row[11]),

                        'sas_tw1'=> (int) ((int) ubah_uang($row[4])+(int) ubah_uang($row[7])+(int) ubah_uang($row[10])),
                        'cap_tw1'=> (int) ((int) ubah_uang($row[5])+(int) ubah_uang($row[8])+(int) ubah_uang($row[11])),
                        
                        'sas_apr'=> (int) ubah_uang($row[13]),
                        'cap_apr'=> (int) ubah_uang($row[14]),
                        
                        'sas_mei'=> (int) ubah_uang($row[16]),
                        'cap_mei'=> (int) ubah_uang($row[17]),
                        
                        'sas_jun'=> (int) ubah_uang($row[19]),
                        'cap_jun'=> (int) ubah_uang($row[20]),

                        'sas_tw2'=> (int) ((int) ubah_uang($row[13])+(int) ubah_uang($row[16])+(int) ubah_uang($row[19])),
                        'cap_tw2'=> (int) ((int) ubah_uang($row[14])+(int) ubah_uang($row[17])+(int) ubah_uang($row[20])),
                        
                        'sas_jul'=> (int) ubah_uang($row[22]),
                        'cap_jul'=> (int) ubah_uang($row[23]),
                        
                        'sas_agu'=> (int) ubah_uang($row[25]),
                        'cap_agu'=> (int) ubah_uang($row[26]),
                        
                        'sas_sep'=> (int) ubah_uang($row[29]),
                        'cap_sep'=> (int) ubah_uang($row[30]),

                        'sas_tw3'=> (int) ((int) ubah_uang($row[22])+(int) ubah_uang($row[25])+(int) ubah_uang($row[29])),
                        'cap_tw3'=> (int) ((int) ubah_uang($row[23])+(int) ubah_uang($row[26])+(int) ubah_uang($row[30])),
                        
                        'sas_okt'=> (int) ubah_uang($row[32]),
                        'cap_okt'=> (int) ubah_uang($row[33]),
                        
                        'sas_nov'=> (int) ubah_uang($row[35]),
                        'cap_nov'=> (int) ubah_uang($row[36]),
                        
                        'sas_des'=> (int) ubah_uang($row[38]),
                        'cap_des'=> (int) ubah_uang($row[39]),
                        
                        'sas_tw4'=> (int) ((int) ubah_uang($row[32])+(int) ubah_uang($row[35])+(int) ubah_uang($row[38])),
                        'cap_tw4'=> (int) ((int) ubah_uang($row[33])+(int) ubah_uang($row[36])+(int) ubah_uang($row[39])),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==4){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>13,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> (int) ubah_uang($row[4]),
                        'sas_jan'=> (int) ubah_uang($row[5]),
                        'cap_jan'=> (int) ((ubah_uang($row[5])/ubah_uang($row[4]))*100),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ((ubah_uang($row[7])/ubah_uang($row[4]))*100),
                        
                        'sas_mar'=> (int) ubah_uang($row[9]),
                        'cap_mar'=>  (int) ((ubah_uang($row[9])/ubah_uang($row[4]))*100),

                        'sas_tw1'=> 0,
                        'cap_tw1'=> 0,
                        
                        'sas_apr'=> (int) ubah_uang($row[11]),
                        'cap_apr'=>  (int) ((ubah_uang($row[11])/ubah_uang($row[4]))*100),
                        
                        'sas_mei'=> (int) ubah_uang($row[13]),
                        'cap_mei'=>  (int) ((ubah_uang($row[13])/ubah_uang($row[4]))*100),
                        
                        'sas_jun'=> (int) ubah_uang($row[15]),
                        'cap_jun'=>  (int)  ((ubah_uang($row[15])/ubah_uang($row[4]))*100),

                        'sas_tw2'=> 0,
                        'cap_tw2'=> 0,
                        
                        'sas_jul'=> (int) ubah_uang($row[17]),
                        'cap_jul'=>  (int)  ((ubah_uang($row[17])/ubah_uang($row[4]))*100),
                        
                        'sas_agu'=> (int) ubah_uang($row[19]),
                        'cap_agu'=>  (int)  ((ubah_uang($row[19])/ubah_uang($row[4]))*100),
                        
                        'sas_sep'=> (int) ubah_uang($row[21]),
                        'cap_sep'=>  (int)  ((ubah_uang($row[21])/ubah_uang($row[4]))*100),

                        'sas_tw3'=> (int) 0,
                        'cap_tw3'=> (int) 0,
                        
                        'sas_okt'=> (int) ubah_uang($row[23]),
                        'cap_okt'=>  (int) ((ubah_uang($row[23])/ubah_uang($row[4]))*100),
                        
                        'sas_nov'=> (int) ubah_uang($row[25]),
                        'cap_nov'=>  (int) ((ubah_uang($row[25])/ubah_uang($row[4]))*100),
                        
                        'sas_des'=> (int) ubah_uang($row[27]),
                        'cap_des'=>  (int)  ((ubah_uang($row[27])/ubah_uang($row[4]))*100),
                        
                        'sas_tw4'=> (int) 0,
                        'cap_tw4'=> (int) 0,


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==5){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>14,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> (int) ubah_uang($row[4]),
                        'sas_jan'=> (int) ubah_uang($row[5]),
                        'cap_jan'=> (int) ((ubah_uang($row[5])/ubah_uang($row[4]))*100),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ((ubah_uang($row[7])/ubah_uang($row[4]))*100),
                        
                        'sas_mar'=> (int) ubah_uang($row[9]),
                        'cap_mar'=> (int) ((ubah_uang($row[9])/ubah_uang($row[4]))*100),

                        'sas_tw1'=> 0,
                        'cap_tw1'=> 0,
                        
                        'sas_apr'=> (int) ubah_uang($row[11]),
                        'cap_apr'=> (int) ((ubah_uang($row[11])/ubah_uang($row[4]))*100),
                        
                        'sas_mei'=> (int) ubah_uang($row[13]),
                        'cap_mei'=> (int) ((ubah_uang($row[13])/ubah_uang($row[4]))*100),
                        
                        'sas_jun'=> (int) ubah_uang($row[15]),
                        'cap_jun'=> (int) ((ubah_uang($row[15])/ubah_uang($row[4]))*100),

                        'sas_tw2'=> 0,
                        'cap_tw2'=> 0,
                        
                        'sas_jul'=> (int) ubah_uang($row[17]),
                        'cap_jul'=> (int) ((ubah_uang($row[17])/ubah_uang($row[4]))*100),
                        
                        'sas_agu'=> (int) ubah_uang($row[19]),
                        'cap_agu'=> (int) ((ubah_uang($row[19])/ubah_uang($row[4]))*100),
                        
                        'sas_sep'=> (int) ubah_uang($row[21]),
                        'cap_sep'=> (int) ((ubah_uang($row[21])/ubah_uang($row[4]))*100),

                        'sas_tw3'=> (int) 0,
                        'cap_tw3'=> (int) 0,
                        
                        'sas_okt'=> (int) ubah_uang($row[23]),
                        'cap_okt'=> (int) ((ubah_uang($row[23])/ubah_uang($row[4]))*100),
                        
                        'sas_nov'=> (int) ubah_uang($row[25]),
                        'cap_nov'=> (int) ((ubah_uang($row[25])/ubah_uang($row[4]))*100),
                        
                        'sas_des'=> (int) ubah_uang($row[27]),
                        'cap_des'=> (int) ((ubah_uang($row[27])/ubah_uang($row[4]))*100),
                        
                        'sas_tw4'=> (int) 0,
                        'cap_tw4'=> (int) 0,


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==6){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama_upper',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>15,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> (int) ubah_uang($row[4]),
                        'apbn_jan'=> (int) ubah_uang($row[5]),
                        'apbd_jan'=>  (int)  ubah_uang($row[7]),
                        'pbi_jan'=>  (int)  (ubah_uang($row[5])+(int)  ubah_uang($row[7])),
                        'apbn_feb'=> (int) ubah_uang($row[11]),
                        'apbd_feb'=>  (int)  ubah_uang($row[13]),
                        'pbi_feb'=>  (int)  (ubah_uang($row[11])+(int)  ubah_uang($row[13])),
                        'apbn_mar'=> (int) ubah_uang($row[17]),
                        'apbd_mar'=>  (int)  ubah_uang($row[19]),
                        'pbi_mar'=>  (int)  (ubah_uang($row[17])+(int)  ubah_uang($row[19])),
                        'apbn_apr'=> (int) ubah_uang($row[23]),
                        'apbd_apr'=>  (int)  ubah_uang($row[25]),
                        'pbi_apr'=>  (int)  (ubah_uang($row[23])+(int)  ubah_uang($row[25])),
                        'apbn_mei'=> (int) ubah_uang($row[29]),
                        'apbd_mei'=>  (int)  ubah_uang($row[31]),
                        'pbi_mei'=>  (int)  (ubah_uang($row[29])+(int)  ubah_uang($row[31])),
                        'apbn_jun'=> (int) ubah_uang($row[35]),
                        'apbd_jun'=>  (int)  ubah_uang($row[37]),
                        'pbi_jun'=>  (int)  (ubah_uang($row[35])+(int)  ubah_uang($row[37])),
                        'apbn_jul'=> (int) ubah_uang($row[41]),
                        'apbd_jul'=>  (int)  ubah_uang($row[43]),
                        'pbi_jul'=>  (int)  (ubah_uang($row[41])+(int)  ubah_uang($row[43])),
                        'apbn_agu'=> (int) ubah_uang($row[47]),
                        'apbd_agu'=>  (int)  ubah_uang($row[49]),
                        'pbi_agu'=>  (int)  (ubah_uang($row[47])+(int)  ubah_uang($row[49])),
                        'apbn_sep'=> (int) ubah_uang($row[51]),
                        'apbd_sep'=>  (int)  ubah_uang($row[53]),
                        'pbi_sep'=>  (int)  (ubah_uang($row[51])+(int)  ubah_uang($row[53])),
                        'apbn_okt'=> (int) ubah_uang($row[59]),
                        'apbd_okt'=>  (int)  ubah_uang($row[61]),
                        'pbi_okt'=>  (int)  (ubah_uang($row[59])+(int)  ubah_uang($row[61])),
                        'apbn_nov'=> (int) ubah_uang($row[65]),
                        'apbd_nov'=>  (int)  ubah_uang($row[67]),
                        'pbi_nov'=>  (int)  (ubah_uang($row[65])+(int)  ubah_uang($row[67])),
                        'apbn_des'=> (int) ubah_uang($row[71]),
                        'apbd_des'=>  (int)  ubah_uang($row[73]),
                        'pbi_des'=>  (int)  (ubah_uang($row[71])+(int)  ubah_uang($row[73])),
                        
                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        
        if($row[0]==7){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>16,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> 0,
                        'sas_jan'=> (int) ubah_uang($row[4]),
                        'cap_jan'=> (int) ubah_uang($row[5]),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ubah_uang($row[8]),
                        
                        'sas_mar'=> (int) ubah_uang($row[10]),
                        'cap_mar'=> (int) ubah_uang($row[11]),

                        'sas_tw1'=> (int) ((int) ubah_uang($row[4])+(int) ubah_uang($row[7])+(int) ubah_uang($row[10])),
                        'cap_tw1'=> (int) ((int) ubah_uang($row[5])+(int) ubah_uang($row[8])+(int) ubah_uang($row[11])),
                        
                        'sas_apr'=> (int) ubah_uang($row[13]),
                        'cap_apr'=> (int) ubah_uang($row[14]),
                        
                        'sas_mei'=> (int) ubah_uang($row[16]),
                        'cap_mei'=> (int) ubah_uang($row[17]),
                        
                        'sas_jun'=> (int) ubah_uang($row[19]),
                        'cap_jun'=> (int) ubah_uang($row[20]),

                        'sas_tw2'=> (int) ((int) ubah_uang($row[13])+(int) ubah_uang($row[16])+(int) ubah_uang($row[19])),
                        'cap_tw2'=> (int) ((int) ubah_uang($row[14])+(int) ubah_uang($row[17])+(int) ubah_uang($row[20])),
                        
                        'sas_jul'=> (int) ubah_uang($row[22]),
                        'cap_jul'=> (int) ubah_uang($row[23]),
                        
                        'sas_agu'=> (int) ubah_uang($row[25]),
                        'cap_agu'=> (int) ubah_uang($row[26]),
                        
                        'sas_sep'=> (int) ubah_uang($row[29]),
                        'cap_sep'=> (int) ubah_uang($row[30]),

                        'sas_tw3'=> (int) ((int) ubah_uang($row[22])+(int) ubah_uang($row[25])+(int) ubah_uang($row[29])),
                        'cap_tw3'=> (int) ((int) ubah_uang($row[23])+(int) ubah_uang($row[26])+(int) ubah_uang($row[30])),
                        
                        'sas_okt'=> (int) ubah_uang($row[32]),
                        'cap_okt'=> (int) ubah_uang($row[33]),
                        
                        'sas_nov'=> (int) ubah_uang($row[35]),
                        'cap_nov'=> (int) ubah_uang($row[36]),
                        
                        'sas_des'=> (int) ubah_uang($row[38]),
                        'cap_des'=> (int) ubah_uang($row[39]),
                        
                        'sas_tw4'=> (int) ((int) ubah_uang($row[32])+(int) ubah_uang($row[35])+(int) ubah_uang($row[38])),
                        'cap_tw4'=> (int) ((int) ubah_uang($row[33])+(int) ubah_uang($row[36])+(int) ubah_uang($row[39])),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==8){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>17,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> 0,
                        'sas_jan'=> (int) ubah_uang($row[4]),
                        'cap_jan'=> (int) ubah_uang($row[5]),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ubah_uang($row[8]),
                        
                        'sas_mar'=> (int) ubah_uang($row[10]),
                        'cap_mar'=> (int) ubah_uang($row[11]),

                        'sas_tw1'=>  (int) ubah_uang($row[6]),
                        'cap_tw1'=> (int) 0,
                        
                        'sas_apr'=> (int) 0,
                        'cap_apr'=> (int) 0,
                        
                        'sas_mei'=> (int) 0,
                        'cap_mei'=> (int) 0,
                        
                        'sas_jun'=> (int) 0,
                        'cap_jun'=> (int) 0,

                        'sas_tw2'=>  (int) ubah_uang($row[9]),
                        'cap_tw2'=> (int) 0,
                        
                        'sas_jul'=> (int) 0,
                        'cap_jul'=> (int) 0,
                        
                        'sas_agu'=> (int) 0,
                        'cap_agu'=> (int) 0,
                        
                        'sas_sep'=> (int) 0,
                        'cap_sep'=> (int) 0,

                        'sas_tw3'=>  (int) ubah_uang($row[12]),
                        'cap_tw3'=> (int) 0,
                        
                        'sas_okt'=> (int) 0,
                        'cap_okt'=> (int) 0,
                        
                        'sas_nov'=> (int) 0,
                        'cap_nov'=> (int) 0,
                        
                        'sas_des'=> (int) 0,
                        'cap_des'=> (int) 0,
                        
                        'sas_tw4'=> (int) 0,
                        'cap_tw4'=> (int) 0,


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==9){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>18,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> 0,
                        'sas_jan'=> (int) ubah_uang($row[4]),
                        'cap_jan'=> (int) ubah_uang($row[5]),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ubah_uang($row[8]),
                        
                        'sas_mar'=> (int) ubah_uang($row[10]),
                        'cap_mar'=> (int) ubah_uang($row[11]),

                        'sas_tw1'=> (int) ((int) ubah_uang($row[4])+(int) ubah_uang($row[7])+(int) ubah_uang($row[10])),
                        'cap_tw1'=> (int) ((int) ubah_uang($row[5])+(int) ubah_uang($row[8])+(int) ubah_uang($row[11])),
                        
                        'sas_apr'=> (int) ubah_uang($row[13]),
                        'cap_apr'=> (int) ubah_uang($row[14]),
                        
                        'sas_mei'=> (int) ubah_uang($row[16]),
                        'cap_mei'=> (int) ubah_uang($row[17]),
                        
                        'sas_jun'=> (int) ubah_uang($row[19]),
                        'cap_jun'=> (int) ubah_uang($row[20]),

                        'sas_tw2'=> (int) ((int) ubah_uang($row[13])+(int) ubah_uang($row[16])+(int) ubah_uang($row[19])),
                        'cap_tw2'=> (int) ((int) ubah_uang($row[14])+(int) ubah_uang($row[17])+(int) ubah_uang($row[20])),
                        
                        'sas_jul'=> (int) ubah_uang($row[22]),
                        'cap_jul'=> (int) ubah_uang($row[23]),
                        
                        'sas_agu'=> (int) ubah_uang($row[25]),
                        'cap_agu'=> (int) ubah_uang($row[26]),
                        
                        'sas_sep'=> (int) ubah_uang($row[29]),
                        'cap_sep'=> (int) ubah_uang($row[30]),

                        'sas_tw3'=> (int) ((int) ubah_uang($row[22])+(int) ubah_uang($row[25])+(int) ubah_uang($row[29])),
                        'cap_tw3'=> (int) ((int) ubah_uang($row[23])+(int) ubah_uang($row[26])+(int) ubah_uang($row[30])),
                        
                        'sas_okt'=> (int) ubah_uang($row[32]),
                        'cap_okt'=> (int) ubah_uang($row[33]),
                        
                        'sas_nov'=> (int) ubah_uang($row[35]),
                        'cap_nov'=> (int) ubah_uang($row[36]),
                        
                        'sas_des'=> (int) ubah_uang($row[38]),
                        'cap_des'=> (int) ubah_uang($row[39]),
                        
                        'sas_tw4'=> (int) ((int) ubah_uang($row[32])+(int) ubah_uang($row[35])+(int) ubah_uang($row[38])),
                        'cap_tw4'=> (int) ((int) ubah_uang($row[33])+(int) ubah_uang($row[36])+(int) ubah_uang($row[39])),


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
        if($row[0]==10){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>19,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> (int) ubah_uang($row[4]),
                        'sas_jan'=> (int) ubah_uang($row[5]),
                        'cap_jan'=> (int) ((ubah_uang($row[5])/ubah_uang($row[4]))*100),
                        
                        'sas_feb'=> (int) ubah_uang($row[7]),
                        'cap_feb'=> (int) ((ubah_uang($row[7])/ubah_uang($row[4]))*100),
                        
                        'sas_mar'=> (int) ubah_uang($row[9]),
                        'cap_mar'=> (int) ((ubah_uang($row[9])/ubah_uang($row[4]))*100),

                        'sas_tw1'=> 0,
                        'cap_tw1'=> 0,
                        
                        'sas_apr'=> (int) ubah_uang($row[11]),
                        'cap_apr'=> (int) ((ubah_uang($row[11])/ubah_uang($row[4]))*100),
                        
                        'sas_mei'=> (int) ubah_uang($row[13]),
                        'cap_mei'=> (int) ((ubah_uang($row[13])/ubah_uang($row[4]))*100),
                        
                        'sas_jun'=> (int) ubah_uang($row[15]),
                        'cap_jun'=> (int) ((ubah_uang($row[15])/ubah_uang($row[4]))*100),

                        'sas_tw2'=> 0,
                        'cap_tw2'=> 0,
                        
                        'sas_jul'=> (int) ubah_uang($row[17]),
                        'cap_jul'=> (int) ((ubah_uang($row[17])/ubah_uang($row[4]))*100),
                        
                        'sas_agu'=> (int) ubah_uang($row[19]),
                        'cap_agu'=> (int) ((ubah_uang($row[19])/ubah_uang($row[4]))*100),
                        
                        'sas_sep'=> (int) ubah_uang($row[21]),
                        'cap_sep'=> (int) ((ubah_uang($row[21])/ubah_uang($row[4]))*100),

                        'sas_tw3'=> (int) 0,
                        'cap_tw3'=> (int) 0,
                        
                        'sas_okt'=> (int) ubah_uang($row[23]),
                        'cap_okt'=> (int) ((ubah_uang($row[23])/ubah_uang($row[4]))*100),
                        
                        'sas_nov'=> (int) ubah_uang($row[25]),
                        'cap_nov'=> (int) ((ubah_uang($row[25])/ubah_uang($row[4]))*100),
                        
                        'sas_des'=> (int) ubah_uang($row[27]),
                        'cap_des'=> (int) ((ubah_uang($row[27])/ubah_uang($row[4]))*100),
                        
                        'sas_tw4'=> (int) 0,
                        'cap_tw4'=> (int) 0,


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
            
        if($row[0]==11){
            $cek=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->count();
            if($cek>0){
                $mst=VwKelurahan::whereIn('id_kec',array_kecamatan())->where('nama',$row[3])->first();
                return SpesifikasiGizi::UpdateOrCreate(
                    [
                        'kecamatan_id'=>$mst->id_kec,
                        'indikator_id'=>20,
                        'tahun'=>$this->tahun,
                        't_header_id'=>$this->t_header_id,
                        'kelurahan'=>$row[3],
                        'kelurahan_id'=>$mst->id,
                        
                    ],
                    [
                        'nomor'=>$mst->id_kec.$mst->id,
                        'kecamatan'=>$mst->kecamatan['nama'],
                        'jumlah'=> (int) ubah_uang($row[4]),
                        'odf'=> (int) ubah_uang($row[5]),
                        'sas_jan'=> 0,
                        'cap_jan'=> 0,
                        
                        'sas_feb'=> 0,
                        'cap_feb'=> 0,
                        
                        'sas_mar'=> 0,
                        'cap_mar'=> 0,

                        'sas_tw1'=> (int) 0,
                        'cap_tw1'=> (int) 0,
                        
                        'sas_apr'=> (int) 0,
                        'cap_apr'=> (int) 0,
                        
                        'sas_mei'=> (int) 0,
                        'cap_mei'=> (int) 0,
                        
                        'sas_jun'=> (int) 0,
                        'cap_jun'=> (int) 0,

                        'sas_tw2'=> (int) 0,
                        'cap_tw2'=> (int) 0,
                        
                        'sas_jul'=> (int) 0,
                        'cap_jul'=> (int) 0,
                        
                        'sas_agu'=> (int) 0,
                        'cap_agu'=> (int) 0,
                        
                        'sas_sep'=> (int) 0,
                        'cap_sep'=> (int) 0,

                        'sas_tw3'=> (int) 0,
                        'cap_tw3'=> (int) 0,
                        
                        'sas_okt'=> (int) 0,
                        'cap_okt'=> (int) 0,
                        
                        'sas_nov'=> (int) 0,
                        'cap_nov'=> (int) 0,
                        
                        'sas_des'=> (int) 0,
                        'cap_des'=> (int) 0,
                        
                        'sas_tw4'=> (int) 0,
                        'cap_tw4'=> (int) 0,


                        'created_at'=>date('Y-m-d H:i:s'),
                    ],
                );
            }else{
                
            }
        }
    }

    public function startRow(): int
    {
        return 3;
    }
}
