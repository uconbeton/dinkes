<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\Gizi;
use App\Models\VwKelurahan;

class GiziImport implements ToModel, WithStartRow
{
    protected $tahun;
    protected $bulan;
    protected $kecamatan_id;
    public function __construct(int $tahun,$bulan,$kecamatan_id)
    {
        $this->tahun = $tahun; 
        $this->bulan = $bulan; 
        $this->kecamatan_id = $kecamatan_id; 
    }
    public function collection(Collection $collection)
    {
        //
    }

    public function model(array $row)
    {
        $cek=VwKelurahan::where('id_kec',$this->kecamatan_id)->where('nama_upper',$row[1])->count();
        if($cek>0){
            $mst=VwKelurahan::where('id_kec',$this->kecamatan_id)->where('nama_upper',$row[1])->first();
            return Gizi::UpdateOrCreate(
                [
                    'kecamatan_id'=>$this->kecamatan_id,
                    'keyid'=>$this->bulan,
                    'tahun'=>$this->tahun,
                    'kelurahan'=>$row[1],
                    'kelurahan_id'=>$mst->id,
                ],
                [
                    
                    'SKL23'=>$row[2],
                    'SKP23'=>$row[3],
                    'SKL59'=>$row[4],
                    'SKP59'=>$row[5],

                    'KRL23'=>$row[6],
                    'KRP23'=>$row[7],
                    'KRL59'=>$row[8],
                    'KRP59'=>$row[9],

                    'BBNL23'=>$row[10],
                    'BBNP23'=>$row[11],
                    'BBNL59'=>$row[12],
                    'BBNP59'=>$row[13],

                    'RSL23'=>$row[14],
                    'RSP23'=>$row[15],
                    'RSL59'=>$row[16],
                    'RSP59'=>$row[17],

                    'SPL23'=>$row[18],
                    'SPP23'=>$row[19],
                    'SPL59'=>$row[20],
                    'SPP59'=>$row[21],

                    'PDL23'=>$row[22],
                    'PDP23'=>$row[23],
                    'PDL59'=>$row[24],
                    'PDP59'=>$row[25],

                    'NRL23'=>$row[26],
                    'NRP23'=>$row[27],
                    'NRL59'=>$row[28],
                    'NRP59'=>$row[29],

                    'TGL23'=>$row[30],
                    'TGP23'=>$row[31],
                    'TGL59'=>$row[32],
                    'TGP59'=>$row[33],

                    'GBL23'=>$row[34],
                    'GBP23'=>$row[35],
                    'GBL59'=>$row[36],
                    'GBP59'=>$row[37],

                    'GKL23'=>$row[38],
                    'GKP23'=>$row[39],
                    'GKL59'=>$row[40],
                    'GKP59'=>$row[41],

                    'GNL23'=>$row[42],
                    'GNP23'=>$row[43],
                    'GNL59'=>$row[44],
                    'GNP59'=>$row[45],

                    'RGLL23'=>$row[46],
                    'RGLP23'=>$row[47],
                    'RGLL59'=>$row[48],
                    'RGLP59'=>$row[49],

                    'GLL23'=>$row[50],
                    'GLP23'=>$row[51],
                    'GLL59'=>$row[52],
                    'GLP59'=>$row[53],

                    'OBL23'=>$row[54],
                    'OBP23'=>$row[55],
                    'OBL59'=>$row[56],
                    'OBP59'=>$row[57],



                    'created_at'=>date('Y-m-d H:i:s'),
                ],
            );
        }else{
            
        }
            
        
    }

    public function startRow(): int
    {
        return 7;
    }
}
