<?php

    function tipe_caleg(){
        $data=App\Models\TipeCaleg::orderBy('id','Asc')->get();
        return $data;
    }
    function get_provinsi(){
        $data=App\Models\Provinsi::orderBy('id','Asc')->get();
        return $data;
    }
    function get_indikator($id){
        $data=App\Models\Indikator::where('tipe_id',$id)->where('active',1)->orderBy('id','Asc')->get();
        return $data;
    }
    function max_id_kelurahan($id_kec){
        $data=App\Models\Kelurahan::where('id_kec',$id_kec)->orderBy('id','Asc')->firstOrfail();
        return $data->id;
    }
    function max_header_id($id){
        $cek=App\Models\HeaderSpesifikasi::where('tipe_id',$id)->count();
        if($cek>0){
            $data=App\Models\HeaderSpesifikasi::where('tipe_id',$id)->orderBy('id','Desc')->firstOrfail();
            $no=$data->id;
        }else{
            $no=0;
        }
        
        return $no;
    }
    function get_header_id($id){
        $data=App\Models\HeaderSpesifikasi::where('tipe_id',$id)->orderBy('id','Desc')->get();
         
        return $data;
    }
    function get_kota($id_prov){
        $data=App\Models\Kota::where('id_prov',$id_prov)->whereNotin('id',get_delete_daerah(1))->orderBy('nama','Asc')->get();
        return $data;
    }
    function get_kecamatan(){
        $data=App\Models\Kecamatan::where('id_kab',3672)->orderBy('nama','Asc')->get();
        return $data;
    }
    function count_kota(){
        $query=App\Models\Kota::query();
        
        $data=$query->where('id_prov',Auth::user()->provinsi_id)->count();
        return $data;
    }
    function count_rencana_tps($act,$id){
        $query=App\Models\Tps::query();
        if($act==1){
            $data=$query->where('provinsi_id',$id)->sum('rencana');
        }
        if($act==2){
            $data=$query->where('kota_id',$id)->sum('rencana');
        }
        if($act==3){
            $data=$query->where('kecamatan_id',$id)->sum('rencana');
        }
        if($act==4){
            $data=$query->where('kode_kelurahan',$id)->sum('rencana');
        }
       
       
        return $data;
    }
    function count_actual_tps($act,$id){
        $query=App\Models\Tps::query();
        if($act==1){
            $data=$query->where('provinsi_id',$id)->sum('actual');
        }
        if($act==2){
            $data=$query->where('kota_id',$id)->sum('actual');
        }
        if($act==3){
            $data=$query->where('kecamatan_id',$id)->sum('actual');
        }
        if($act==4){
            $data=$query->where('kode_kelurahan',$id)->sum('actual');
        }
       
       
        return $data;
    }
    function count_rencana_aktual($id){
        $query=App\Models\Tps::query();
        
        $array=$query->where('kode_kelurahan',$id)->sum('rencana');
       
        return $data;
    }

    function array_kota(){
        $array  = array_column(
            App\Models\Kota::where('id_prov',Auth::user()->provinsi_id)
            ->get()
            ->toArray(),'id'
         );

        return $array;
    }
    function array_kecamatan(){
        $array  = array_column(
            App\Models\Kecamatan::where('id_kab',3672)
            ->get()
            ->toArray(),'id'
         );

        return $array;
    }
    function array_triwulan($id){
        $array  = array_column(
            App\Models\Triwulan::where('keyid',$id)
            ->get()
            ->toArray(),'triwulan'
         );

        return $array;
    }
    function triwulan_aktif(){
        $array  = App\Models\Triwulan::where('triwulan',date('m'))->first();

        return $array->keyid;
    }
    function get_delete_daerah($id){
        $array  = array_column(
            App\Models\DaerahHide::where('caleg_id',Auth::user()->caleg_id)->where('kategori_area_id',$id)
            ->get()
            ->toArray(),'area_id'
         );

        return $array;
    }
    

?>