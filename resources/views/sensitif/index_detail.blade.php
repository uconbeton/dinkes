@extends('layouts.app_web')

@section('content')
<style>
    .ttdh{
        font-size:11px;
        padding:1%;
        vertical-align:middle !important;
        border:solid 1px #75757c !important;
        background: #baf9ba;
    }
    th{
        font-size:12px;
        background: #ffedc2;
        vertical-align:middle
    }
    .kecil{
        font-size:11px;
    }
    td{
        font-size:12px;
        padding: 5px 10px !important;
        background:#fff;
    }
</style>
<div id="page-title" class="page-title has-bg" style="">
	<div class="bg-cvr"  id="bg-cvr" style="background-image: linear-gradient(to bottom, #ffffff, #dad4f4, #b6abe8, #9182db, #6a5bcd);"></div>
    <div class="container" style="color:#000">
            
            <div class="row row-space-30 " style="margin-top:1%;">
            
				
                <div class="col-lg-12" style="text-align:center">
                <img src="{{url_plug()}}/assets/img/cover/logo.png?v={{date('ymdhis')}}" width="10%">
                    <h3 class="page-header" style="padding:2%;text-align:center;margin-bottom: 3%;"><b>KOTA CILEGON</b><br>Data Intervensi Spesifik Kecamatan {{$data->nama}}<br><small>Tahun {{$tahun}}</small></h3>
                </div>
            </div>
			<div class="row row-space-30" style="background:blue">
            
                <!-- <div class="col-lg-12 layout-mobile" style="text-align:center">
                    <table width="100%">
                        <tr>
                            <td width="50%"><a href="javascript:;" class="btn btn-white btn-block"> Lihat Detail Stunting</a></td>
                            <td><a href="javascript:;" class="btn btn-blue btn-block"> Ubah Tahun Tampilan</a></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-12  col-xs-2 m-b-5 layout-web" style="text-align:center">
                    &nbsp;
				</div> -->
			</div>
            @foreach(get_indikator(3) as $o)   
                @if($o->id==3 || $o->id==4)
                <div class="row row-space-30" style="margin-top:1%;margin-top: 1%; ">
                    
                    <div class="col-lg-12" style=" padding-top: 1%;text-align:left;border-bottom: solid 3px #dfdfe9;padding:0px">
                        
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #f7f7ff;">
                        <h5 class="page-header" style="padding:2%;text-align:center;margin-bottom: 2%;text-transform: uppercase;"><b>Tabel Nilai</b><br>   <small>{{$o->keterangan}}</small></h5>
                    
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #ffffff; border: solid 1px #e8e8ef;">
                            <div class="table-responsive">
								<table class="table table-bordered m-b-0">
									<thead>
										<tr>
											<th colspan="12">{{$o->keterangan}}</th>
										</tr>
										<tr>
											<th colspan="3">Triwulan Ke 1</th>
											<th colspan="3">Triwulan Ke 2</th>
											<th colspan="3">Triwulan Ke 3</th>
											<th colspan="3">Triwulan Ke 4</th>
										</tr>
										<tr>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											
										</tr>
									</thead>
									<tbody id="tampil-data-{{$o->id}}">
										
									</tbody>
                                    
								</table>
							</div>
                        
                    </div>
                    
                    
                </div>
                @else
                <div class="row row-space-30" style="margin-top:1%;margin-top: 1%; ">
                    
                    <div class="col-lg-12" style=" padding-top: 1%;text-align:left;border-bottom: solid 3px #dfdfe9;padding:0px">
                        
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #f7f7ff;">
                        <h5 class="page-header" style="padding:2%;text-align:center;margin-bottom: 2%;text-transform: uppercase;"><b>Tabel Nilai</b><br>   <small>{{$o->keterangan}}</small></h5>
                    
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #ffffff; border: solid 1px #e8e8ef;">
                            <div class="table-responsive">
                                @if($o->id==10)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==11)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==12)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Persalinan</th>
                                                <th class="kecil" width="5%">Jumlah Peserta KB pasca persalinan</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==13)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th rowspan="2" >JUMLAH KK</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                <th class="kecil" width="5%">PAMRT</th>
                                                <th class="kecil" width="5%">% PAMRT</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_jumlah"></th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==14)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th rowspan="2" >Jumlah KK</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">Pengelolaan Limbah Cair RT</th>
                                                <th class="kecil" width="5%">% Pengelolaan Limbah Cair RT</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_jumlah"></th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==15)
                                    <table class="table table-bordered m-b-0" width="150%">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th rowspan="2" >Jumlah Penduduk</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                <th class="kecil" width="3%">PBI<br>APBN</th>
                                                <th class="kecil" width="3%">PBI<br>APBD</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_jumlah"></th>
                                                <th id="tot{{$o->id}}_apbn_jan"></th>
                                                <th id="tot{{$o->id}}_apbd_jan"></th>
                                                <th id="tot{{$o->id}}_apbn_feb"></th>
                                                <th id="tot{{$o->id}}_apbd_feb"></th>
                                                <th id="tot{{$o->id}}_apbn_mar"></th>
                                                <th id="tot{{$o->id}}_apbd_mar"></th>
                                                <th id="tot{{$o->id}}_apbn_apr"></th>
                                                <th id="tot{{$o->id}}_apbd_apr"></th>
                                                <th id="tot{{$o->id}}_apbn_mei"></th>
                                                <th id="tot{{$o->id}}_apbd_mei"></th>
                                                <th id="tot{{$o->id}}_apbn_jun"></th>
                                                <th id="tot{{$o->id}}_apbd_jun"></th>
                                                <th id="tot{{$o->id}}_apbn_jul"></th>
                                                <th id="tot{{$o->id}}_apbd_jul"></th>
                                                <th id="tot{{$o->id}}_apbn_agu"></th>
                                                <th id="tot{{$o->id}}_apbd_agu"></th>
                                                <th id="tot{{$o->id}}_apbn_sep"></th>
                                                <th id="tot{{$o->id}}_apbd_sep"></th>
                                                <th id="tot{{$o->id}}_apbn_okt"></th>
                                                <th id="tot{{$o->id}}_apbd_okt"></th>
                                                <th id="tot{{$o->id}}_apbn_nov"></th>
                                                <th id="tot{{$o->id}}_apbd_nov"></th>
                                                <th id="tot{{$o->id}}_apbn_des"></th>
                                                <th id="tot{{$o->id}}_apbd_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==16)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting</th>
                                                <th class="kecil" width="5%">Jumlah KK beresiko Stunting yang memperoleh pendampingan</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==17)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" width="5%">No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th colspan="2">{{$tahun}}</th>
                                                <th colspan="2">{{$tahun+1}}</th>
                                                <th colspan="2">{{$tahun+2}}</th>
                                                
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="11%">Jumlah KK Miskin</th>
                                                <th class="kecil" width="11%">Jumlah KK Miskin yang memperoleh bantuan Tunai Bersyarat</th>
                                                <th class="kecil" width="11%">Jumlah KK Miskin</th>
                                                <th class="kecil" width="11%">Jumlah KK Miskin yang memperoleh bantuan Tunai Bersyarat</th>
                                                <th class="kecil" width="11%">Jumlah KK Miskin</th>
                                                <th class="kecil" width="11%">Jumlah KK Miskin yang memperoleh bantuan Tunai Bersyarat</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="2">Total</th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==18)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran Layanan KAP</th>
                                                <th class="kecil" width="5%">Jumlah Sasaran yang talah memperoleh KAP</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==19)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" >No</th>
                                                <th rowspan="2" >Kelurahan</th>
                                                <th rowspan="2" >Tahun</th>
                                                <th rowspan="2" >Jumlah KK Miskin</th>
                                                <th colspan="2">Januari</th>
                                                <th colspan="2">Februari</th>
                                                <th colspan="2">Maret</th>
                                                <th colspan="2">April</th>
                                                <th colspan="2">Mei</th>
                                                <th colspan="2">Juni</th>
                                                <th colspan="2">Juli</th>
                                                <th colspan="2">Agustus</th>
                                                <th colspan="2">September</th>
                                                <th colspan="2">Oktober</th>
                                                <th colspan="2">November</th>
                                                <th colspan="2">Desenber</th>
                                            </tr>
                                            <tr>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                <th class="kecil" width="5%">Jumlah KK Miskin yang memperoleh bantuan Pangan</th>
                                                <th class="kecil" width="5%">%</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="3">Total</th>
                                                <th id="tot{{$o->id}}_jumlah"></th>
                                                <th id="tot{{$o->id}}_sas_jan"></th>
                                                <th id="tot{{$o->id}}_cap_jan"></th>
                                                <th id="tot{{$o->id}}_sas_feb"></th>
                                                <th id="tot{{$o->id}}_cap_feb"></th>
                                                <th id="tot{{$o->id}}_sas_mar"></th>
                                                <th id="tot{{$o->id}}_cap_mar"></th>
                                                <th id="tot{{$o->id}}_sas_apr"></th>
                                                <th id="tot{{$o->id}}_cap_apr"></th>
                                                <th id="tot{{$o->id}}_sas_mei"></th>
                                                <th id="tot{{$o->id}}_cap_mei"></th>
                                                <th id="tot{{$o->id}}_sas_jun"></th>
                                                <th id="tot{{$o->id}}_cap_jun"></th>
                                                <th id="tot{{$o->id}}_sas_jul"></th>
                                                <th id="tot{{$o->id}}_cap_jul"></th>
                                                <th id="tot{{$o->id}}_sas_agu"></th>
                                                <th id="tot{{$o->id}}_cap_agu"></th>
                                                <th id="tot{{$o->id}}_sas_sep"></th>
                                                <th id="tot{{$o->id}}_cap_sep"></th>
                                                <th id="tot{{$o->id}}_sas_okt"></th>
                                                <th id="tot{{$o->id}}_cap_okt"></th>
                                                <th id="tot{{$o->id}}_sas_nov"></th>
                                                <th id="tot{{$o->id}}_cap_nov"></th>
                                                <th id="tot{{$o->id}}_sas_des"></th>
                                                <th id="tot{{$o->id}}_cap_des"></th>
                                            
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
                                @if($o->id==20)
                                    <table class="table table-bordered m-b-0">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th>Kelurahan</th>
                                                <th  width="10%">JUMLAH KK</th>
                                                <th  width="10%">% ODF</th>
                                                
                                            </tr>
                                            
                                        </thead>
                                        <tbody id="tampil-data-{{$o->id}}">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="2">Total</th>
                                                <th id="tot{{$o->id}}_jumlah"></th>
                                                <th id="tot{{$o->id}}_odf"></th>
                                                
                                            </tr>
                                        </tfoot>
                                    </table>
                                @endif
							</div>
                        
                    </div>
                    
                    
                </div>
                @endif
            @endforeach
    </div>
</div>
    
@endsection

@section('modal')

        <div id="modal-form" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;">                                               
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form ( Create / Update )</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydataform" method="post" action="{{ url('employe') }}" enctype="multipart/form-data" >
                            @csrf
                            <div id="tampil-form"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                        <a href="javascript:;" class="btn btn-success" id="save-data">Save</a>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
@endsection
@push('ajax')
	<script src="{{url_plug()}}/assets/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="{{url_plug()}}/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>
	<script>
		/*
		Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
		Version: 4.6.0
		Author: Sean Ngu
		Website: http://www.seantheme.com/color-admin/admin/
        
		*/
        @foreach(get_indikator(3) as $o)
                
                function get_dta{{$o->id}}(){
                    @if($o->id==17 || $o->id==20 || $o->id==15) 
                        @if($o->id==17)
                            $.ajax({ 
                                type: 'GET', 
                                url: "{{ url('intervensi-sensitif/'.$data->id.'/getdata')}}?indikator_id={{$o->id}}", 
                                data: { ide: 1 }, 
                                dataType: 'json',
                                beforeSend: function() {
                                    $("#tampil-data-{{$o->id}}").html("");
                                },
                                success: function (data) {
                                    $('#tot{{$o->id}}_sas_jan').html(data.tot_sas_jan);
                                    $('#tot{{$o->id}}_sas_feb').html(data.tot_sas_feb);
                                    $('#tot{{$o->id}}_sas_mar').html(data.tot_sas_mar);
                                    $('#tot{{$o->id}}_sas_apr').html(data.tot_sas_apr);
                                    $('#tot{{$o->id}}_sas_mei').html(data.tot_sas_mei);
                                    $('#tot{{$o->id}}_sas_jun').html(data.tot_sas_jun);
                                    $('#tot{{$o->id}}_sas_jul').html(data.tot_sas_jul);
                                    $('#tot{{$o->id}}_sas_agu').html(data.tot_sas_agu);
                                    $('#tot{{$o->id}}_sas_sep').html(data.tot_sas_sep);
                                    $('#tot{{$o->id}}_sas_okt').html(data.tot_sas_okt);
                                    $('#tot{{$o->id}}_sas_nov').html(data.tot_sas_nov);
                                    $('#tot{{$o->id}}_sas_des').html(data.tot_sas_des);
                                    $('#tot{{$o->id}}_cap_jan').html(data.tot_cap_jan);
                                    $('#tot{{$o->id}}_cap_feb').html(data.tot_cap_feb);
                                    $('#tot{{$o->id}}_cap_mar').html(data.tot_cap_mar);
                                    $('#tot{{$o->id}}_cap_apr').html(data.tot_cap_apr);
                                    $('#tot{{$o->id}}_cap_mei').html(data.tot_cap_mei);
                                    $('#tot{{$o->id}}_cap_jun').html(data.tot_cap_jun);
                                    $('#tot{{$o->id}}_cap_jul').html(data.tot_cap_jul);
                                    $('#tot{{$o->id}}_cap_agu').html(data.tot_cap_agu);
                                    $('#tot{{$o->id}}_cap_sep').html(data.tot_cap_sep);
                                    $('#tot{{$o->id}}_cap_okt').html(data.tot_cap_okt);
                                    $('#tot{{$o->id}}_cap_nov').html(data.tot_cap_nov);
                                    $('#tot{{$o->id}}_cap_des').html(data.tot_cap_des);
                                    $.each(data.data, function(i, result){
                                        if(result.total>0){
                                            var color="red";
                                        }else{
                                            var color="blueblue";
                                        }
                                        var tampil='<tr>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+(i+1)+'</td>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+result.nama+'</td>'
                                                        +'<td class="ttdd">'+result.sas_jan+'</td>'
                                                        +'<td class="ttdd">'+result.cap_jan+'</td>'
                                                        +'<td class="ttdd">'+result.sas_feb+'</td>'
                                                        +'<td class="ttdd">'+result.cap_feb+'</td>'
                                                        +'<td class="ttdd">'+result.sas_mar+'</td>'
                                                        +'<td class="ttdd">'+result.cap_mar+'</td>'
                                                        
                                                    +'</tr>';
                                        $("#tampil-data-{{$o->id}}").append(tampil);
                                    });
                                
                                }
                            });
                        @elseif($o->id==20)
                            $.ajax({ 
                                type: 'GET', 
                                url: "{{ url('intervensi-sensitif/'.$data->id.'/getdata')}}?indikator_id={{$o->id}}", 
                                data: { ide: 1 }, 
                                dataType: 'json',
                                beforeSend: function() {
                                    $("#tampil-data-{{$o->id}}").html("");
                                },
                                success: function (data) {
                                    $('#tot{{$o->id}}_sas_jan').html(data.tot_sas_jan);
                                    $('#tot{{$o->id}}_sas_feb').html(data.tot_sas_feb);
                                    $('#tot{{$o->id}}_sas_mar').html(data.tot_sas_mar);
                                    $('#tot{{$o->id}}_sas_apr').html(data.tot_sas_apr);
                                    $('#tot{{$o->id}}_sas_mei').html(data.tot_sas_mei);
                                    $('#tot{{$o->id}}_sas_jun').html(data.tot_sas_jun);
                                    $('#tot{{$o->id}}_sas_jul').html(data.tot_sas_jul);
                                    $('#tot{{$o->id}}_sas_agu').html(data.tot_sas_agu);
                                    $('#tot{{$o->id}}_sas_sep').html(data.tot_sas_sep);
                                    $('#tot{{$o->id}}_sas_okt').html(data.tot_sas_okt);
                                    $('#tot{{$o->id}}_sas_nov').html(data.tot_sas_nov);
                                    $('#tot{{$o->id}}_sas_des').html(data.tot_sas_des);
                                    $('#tot{{$o->id}}_cap_jan').html(data.tot_cap_jan);
                                    $('#tot{{$o->id}}_cap_feb').html(data.tot_cap_feb);
                                    $('#tot{{$o->id}}_cap_mar').html(data.tot_cap_mar);
                                    $('#tot{{$o->id}}_cap_apr').html(data.tot_cap_apr);
                                    $('#tot{{$o->id}}_cap_mei').html(data.tot_cap_mei);
                                    $('#tot{{$o->id}}_cap_jun').html(data.tot_cap_jun);
                                    $('#tot{{$o->id}}_cap_jul').html(data.tot_cap_jul);
                                    $('#tot{{$o->id}}_cap_agu').html(data.tot_cap_agu);
                                    $('#tot{{$o->id}}_cap_sep').html(data.tot_cap_sep);
                                    $('#tot{{$o->id}}_cap_okt').html(data.tot_cap_okt);
                                    $('#tot{{$o->id}}_cap_nov').html(data.tot_cap_nov);
                                    $('#tot{{$o->id}}_cap_des').html(data.tot_cap_des);
                                    $.each(data.data, function(i, result){
                                        if(result.total>0){
                                            var color="red";
                                        }else{
                                            var color="blueblue";
                                        }
                                        var tampil='<tr>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+(i+1)+'</td>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+result.nama+'</td>'
                                                        +'<td class="ttdd">'+result.jumlah+'</td>'
                                                        +'<td class="ttdd">'+result.odf+'</td>'
                                                    +'</tr>';
                                        $("#tampil-data-{{$o->id}}").append(tampil);
                                    });
                                
                                }
                            });
                        @else
                            $.ajax({ 
                                type: 'GET', 
                                url: "{{ url('intervensi-sensitif/'.$data->id.'/getdata')}}?indikator_id={{$o->id}}", 
                                data: { ide: 1 }, 
                                dataType: 'json',
                                beforeSend: function() {
                                    $("#tampil-data-{{$o->id}}").html("");
                                },
                                success: function (data) {
                                    $('#tot{{$o->id}}_apbn_jan').html(data.tot_apbn_jan);
                                    $('#tot{{$o->id}}_apbn_feb').html(data.tot_apbn_feb);
                                    $('#tot{{$o->id}}_apbn_mar').html(data.tot_apbn_mar);
                                    $('#tot{{$o->id}}_apbn_apr').html(data.tot_apbn_apr);
                                    $('#tot{{$o->id}}_apbn_mei').html(data.tot_apbn_mei);
                                    $('#tot{{$o->id}}_apbn_jun').html(data.tot_apbn_jun);
                                    $('#tot{{$o->id}}_apbn_jul').html(data.tot_apbn_jul);
                                    $('#tot{{$o->id}}_apbn_agu').html(data.tot_apbn_agu);
                                    $('#tot{{$o->id}}_apbn_sep').html(data.tot_apbn_sep);
                                    $('#tot{{$o->id}}_apbn_okt').html(data.tot_apbn_okt);
                                    $('#tot{{$o->id}}_apbn_nov').html(data.tot_apbn_nov);
                                    $('#tot{{$o->id}}_apbn_des').html(data.tot_apbn_des);
                                    $('#tot{{$o->id}}_apbd_jan').html(data.tot_apbd_jan);
                                    $('#tot{{$o->id}}_apbd_feb').html(data.tot_apbd_feb);
                                    $('#tot{{$o->id}}_apbd_mar').html(data.tot_apbd_mar);
                                    $('#tot{{$o->id}}_apbd_apr').html(data.tot_apbd_apr);
                                    $('#tot{{$o->id}}_apbd_mei').html(data.tot_apbd_mei);
                                    $('#tot{{$o->id}}_apbd_jun').html(data.tot_apbd_jun);
                                    $('#tot{{$o->id}}_apbd_jul').html(data.tot_apbd_jul);
                                    $('#tot{{$o->id}}_apbd_agu').html(data.tot_apbd_agu);
                                    $('#tot{{$o->id}}_apbd_sep').html(data.tot_apbd_sep);
                                    $('#tot{{$o->id}}_apbd_okt').html(data.tot_apbd_okt);
                                    $('#tot{{$o->id}}_apbd_nov').html(data.tot_apbd_nov);
                                    $('#tot{{$o->id}}_apbd_des').html(data.tot_apbd_des);
                                    $('#tot{{$o->id}}_jumlah').html(data.tot_jumlah);
                                    $.each(data.data, function(i, result){
                                        if(result.total>0){
                                            var color="red";
                                        }else{
                                            var color="blueblue";
                                        }
                                        var tampil='<tr>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+(i+1)+'</td>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+result.nama+'</td>'
                                                        +'<td class="ttdd">'+result.tahun+'</td>'
                                                        +'<td class="ttdd">'+result.jumlah+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_jan+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_jan+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_feb+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_feb+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_mar+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_mar+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_apr+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_apr+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_mei+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_mei+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_jun+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_jun+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_jul+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_jul+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_agu+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_agu+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_sep+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_sep+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_okt+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_okt+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_nov+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_nov+'</td>'
                                                        +'<td class="ttdd">'+result.apbn_des+'</td>'
                                                        +'<td class="ttdd">'+result.apbd_des+'</td>'
                                                      
                                                    +'</tr>';
                                        $("#tampil-data-{{$o->id}}").append(tampil);
                                    });
                                
                                }
                            });
                        @endif
                    @else
                        @if($o->id==10 || $o->id==11 || $o->id==12 || $o->id==16 || $o->id==18) 
                            $.ajax({ 
                                type: 'GET', 
                                url: "{{ url('intervensi-sensitif/'.$data->id.'/getdata')}}?indikator_id={{$o->id}}", 
                                data: { ide: 1 }, 
                                dataType: 'json',
                                beforeSend: function() {
                                    $("#tampil-data-{{$o->id}}").html("");
                                },
                                success: function (data) {
                                    $('#tot{{$o->id}}_sas_jan').html(data.tot_sas_jan);
                                    $('#tot{{$o->id}}_sas_feb').html(data.tot_sas_feb);
                                    $('#tot{{$o->id}}_sas_mar').html(data.tot_sas_mar);
                                    $('#tot{{$o->id}}_sas_apr').html(data.tot_sas_apr);
                                    $('#tot{{$o->id}}_sas_mei').html(data.tot_sas_mei);
                                    $('#tot{{$o->id}}_sas_jun').html(data.tot_sas_jun);
                                    $('#tot{{$o->id}}_sas_jul').html(data.tot_sas_jul);
                                    $('#tot{{$o->id}}_sas_agu').html(data.tot_sas_agu);
                                    $('#tot{{$o->id}}_sas_sep').html(data.tot_sas_sep);
                                    $('#tot{{$o->id}}_sas_okt').html(data.tot_sas_okt);
                                    $('#tot{{$o->id}}_sas_nov').html(data.tot_sas_nov);
                                    $('#tot{{$o->id}}_sas_des').html(data.tot_sas_des);
                                    $('#tot{{$o->id}}_cap_jan').html(data.tot_cap_jan);
                                    $('#tot{{$o->id}}_cap_feb').html(data.tot_cap_feb);
                                    $('#tot{{$o->id}}_cap_mar').html(data.tot_cap_mar);
                                    $('#tot{{$o->id}}_cap_apr').html(data.tot_cap_apr);
                                    $('#tot{{$o->id}}_cap_mei').html(data.tot_cap_mei);
                                    $('#tot{{$o->id}}_cap_jun').html(data.tot_cap_jun);
                                    $('#tot{{$o->id}}_cap_jul').html(data.tot_cap_jul);
                                    $('#tot{{$o->id}}_cap_agu').html(data.tot_cap_agu);
                                    $('#tot{{$o->id}}_cap_sep').html(data.tot_cap_sep);
                                    $('#tot{{$o->id}}_cap_okt').html(data.tot_cap_okt);
                                    $('#tot{{$o->id}}_cap_nov').html(data.tot_cap_nov);
                                    $('#tot{{$o->id}}_cap_des').html(data.tot_cap_des);
                                    $.each(data.data, function(i, result){
                                        if(result.total>0){
                                            var color="red";
                                        }else{
                                            var color="blueblue";
                                        }
                                        var tampil='<tr>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+(i+1)+'</td>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+result.nama+'</td>'
                                                        +'<td class="ttdd">'+result.tahun+'</td>'
                                                        +'<td class="ttdd">'+result.sas_jan+'</td>'
                                                        +'<td class="ttdd">'+result.cap_jan+'</td>'
                                                        +'<td class="ttdd">'+result.sas_feb+'</td>'
                                                        +'<td class="ttdd">'+result.cap_feb+'</td>'
                                                        +'<td class="ttdd">'+result.sas_mar+'</td>'
                                                        +'<td class="ttdd">'+result.cap_mar+'</td>'
                                                        +'<td class="ttdd">'+result.sas_apr+'</td>'
                                                        +'<td class="ttdd">'+result.cap_apr+'</td>'
                                                        +'<td class="ttdd">'+result.sas_mei+'</td>'
                                                        +'<td class="ttdd">'+result.cap_mei+'</td>'
                                                        +'<td class="ttdd">'+result.sas_jun+'</td>'
                                                        +'<td class="ttdd">'+result.cap_jun+'</td>'
                                                        +'<td class="ttdd">'+result.sas_jul+'</td>'
                                                        +'<td class="ttdd">'+result.cap_jul+'</td>'
                                                        +'<td class="ttdd">'+result.sas_agu+'</td>'
                                                        +'<td class="ttdd">'+result.cap_agu+'</td>'
                                                        +'<td class="ttdd">'+result.sas_sep+'</td>'
                                                        +'<td class="ttdd">'+result.cap_sep+'</td>'
                                                        +'<td class="ttdd">'+result.sas_okt+'</td>'
                                                        +'<td class="ttdd">'+result.cap_okt+'</td>'
                                                        +'<td class="ttdd">'+result.sas_nov+'</td>'
                                                        +'<td class="ttdd">'+result.cap_nov+'</td>'
                                                        +'<td class="ttdd">'+result.sas_des+'</td>'
                                                        +'<td class="ttdd">'+result.cap_des+'</td>'
                                                      
                                                    +'</tr>';
                                        $("#tampil-data-{{$o->id}}").append(tampil);
                                    });
                                
                                }
                            });
                        @endif
                        @if($o->id==13 || $o->id==14 ||  $o->id==19) 
                            $.ajax({ 
                                type: 'GET', 
                                url: "{{ url('intervensi-sensitif/'.$data->id.'/getdata')}}?indikator_id={{$o->id}}", 
                                data: { ide: 1 }, 
                                dataType: 'json',
                                beforeSend: function() {
                                    $("#tampil-data-{{$o->id}}").html("");
                                },
                                success: function (data) {
                                    $('#tot{{$o->id}}_sas_jan').html(data.tot_sas_jan);
                                    $('#tot{{$o->id}}_sas_feb').html(data.tot_sas_feb);
                                    $('#tot{{$o->id}}_sas_mar').html(data.tot_sas_mar);
                                    $('#tot{{$o->id}}_sas_apr').html(data.tot_sas_apr);
                                    $('#tot{{$o->id}}_sas_mei').html(data.tot_sas_mei);
                                    $('#tot{{$o->id}}_sas_jun').html(data.tot_sas_jun);
                                    $('#tot{{$o->id}}_sas_jul').html(data.tot_sas_jul);
                                    $('#tot{{$o->id}}_sas_agu').html(data.tot_sas_agu);
                                    $('#tot{{$o->id}}_sas_sep').html(data.tot_sas_sep);
                                    $('#tot{{$o->id}}_sas_okt').html(data.tot_sas_okt);
                                    $('#tot{{$o->id}}_sas_nov').html(data.tot_sas_nov);
                                    $('#tot{{$o->id}}_sas_des').html(data.tot_sas_des);
                                    $('#tot{{$o->id}}_cap_jan').html(data.tot_cap_jan);
                                    $('#tot{{$o->id}}_cap_feb').html(data.tot_cap_feb);
                                    $('#tot{{$o->id}}_cap_mar').html(data.tot_cap_mar);
                                    $('#tot{{$o->id}}_cap_apr').html(data.tot_cap_apr);
                                    $('#tot{{$o->id}}_cap_mei').html(data.tot_cap_mei);
                                    $('#tot{{$o->id}}_cap_jun').html(data.tot_cap_jun);
                                    $('#tot{{$o->id}}_cap_jul').html(data.tot_cap_jul);
                                    $('#tot{{$o->id}}_cap_agu').html(data.tot_cap_agu);
                                    $('#tot{{$o->id}}_cap_sep').html(data.tot_cap_sep);
                                    $('#tot{{$o->id}}_cap_okt').html(data.tot_cap_okt);
                                    $('#tot{{$o->id}}_cap_nov').html(data.tot_cap_nov);
                                    $('#tot{{$o->id}}_cap_des').html(data.tot_cap_des);
                                    $('#tot{{$o->id}}_jumlah').html(data.tot_jumlah);
                                    $.each(data.data, function(i, result){
                                        if(result.total>0){
                                            var color="red";
                                        }else{
                                            var color="blueblue";
                                        }
                                        var tampil='<tr>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+(i+1)+'</td>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+result.nama+'</td>'
                                                        +'<td class="ttdd">'+result.tahun+'</td>'
                                                        +'<td class="ttdd">'+result.jumlah+'</td>'
                                                        +'<td class="ttdd">'+result.sas_jan+'</td>'
                                                        +'<td class="ttdd">'+result.cap_jan+'</td>'
                                                        +'<td class="ttdd">'+result.sas_feb+'</td>'
                                                        +'<td class="ttdd">'+result.cap_feb+'</td>'
                                                        +'<td class="ttdd">'+result.sas_mar+'</td>'
                                                        +'<td class="ttdd">'+result.cap_mar+'</td>'
                                                        +'<td class="ttdd">'+result.sas_apr+'</td>'
                                                        +'<td class="ttdd">'+result.cap_apr+'</td>'
                                                        +'<td class="ttdd">'+result.sas_mei+'</td>'
                                                        +'<td class="ttdd">'+result.cap_mei+'</td>'
                                                        +'<td class="ttdd">'+result.sas_jun+'</td>'
                                                        +'<td class="ttdd">'+result.cap_jun+'</td>'
                                                        +'<td class="ttdd">'+result.sas_jul+'</td>'
                                                        +'<td class="ttdd">'+result.cap_jul+'</td>'
                                                        +'<td class="ttdd">'+result.sas_agu+'</td>'
                                                        +'<td class="ttdd">'+result.cap_agu+'</td>'
                                                        +'<td class="ttdd">'+result.sas_sep+'</td>'
                                                        +'<td class="ttdd">'+result.cap_sep+'</td>'
                                                        +'<td class="ttdd">'+result.sas_okt+'</td>'
                                                        +'<td class="ttdd">'+result.cap_okt+'</td>'
                                                        +'<td class="ttdd">'+result.sas_nov+'</td>'
                                                        +'<td class="ttdd">'+result.cap_nov+'</td>'
                                                        +'<td class="ttdd">'+result.sas_des+'</td>'
                                                        +'<td class="ttdd">'+result.cap_des+'</td>'
                                                      
                                                    +'</tr>';
                                        $("#tampil-data-{{$o->id}}").append(tampil);
                                    });
                                
                                }
                            });
                        @endif
                    @endif
                }
        @endforeach
		setTimeout(()=> {
         
        }, 1000);

        function cari_act(act){
            location.assign("{{url('intervensi-sensitif/'.$data->id)}}?bulan={{$bulan}}&act="+act+"&tahun={{$tahun}}")
        }
        function pilih_triwulan(act){
            location.assign("{{url('intervensi-sensitif/'.$data->id)}}?tahun={{$tahun}}&bulan="+act)
        }
		$(document).ready(function() {
            @foreach(get_indikator(3) as $o)   
                get_dta{{$o->id}}();
            @endforeach
			
		});

        setTimeout(()=> {
            @if($act==2)
            for(var i=2; i<=25;i++)
            {
                load_isi(i);
            }
            @else
            for(var i=2; i<=17;i++)
            {
                load_isi(i);
            }
            @endif
        }, 1000);
        function load_isi(num){
        
            
            var total=0;
            $('table tbody tr').each(function(){
                var value= parseInt($('td',this).eq(num).text());
                if(value>0){
                    total +=value;
                }else{
                    total +=0;
                }
                
            });
            $('.nilai_'+num).text(+total);
            
        
        }
        $(document).ready(function() {
            
           

		});
	</script>

@endpush
