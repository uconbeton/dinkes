<input type="hidden" name="id" value="{{$id}}">
<div class="row form-group m-b-4">
    <label class="col-md-3 col-form-label">Nama Calon</label>
    <div class="col-md-8">
        <div class="input-group">
            <input type="text" name="nama_caleg"  value="{{$data->nama}}"  class="form-control">
        </div>
    </div>
</div>
<div class="row form-group m-b-4">
    <label class="col-md-3 col-form-label">NIK (Nomor KTP) </label>
    <div class="col-md-4">
        <div class="input-group">
            <input type="text" name="nik" {{$disabled}} value="{{$data->nik}}" class="form-control">
        </div>
    </div>
</div>
<div class="row form-group m-b-4">
    <label class="col-md-3 col-form-label">Tipe Pencalonan</label>
    <div class="col-md-5">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-chevron-down"></i></span></div>
            <select class="default-select2 form-control" name="tipe_caleg_id" id="tipe_caleg_id" onchange="pilih_tipe_caleg(this.value)">
               
                    <option value="">.:: Pilih</option>
                    @foreach(tipe_caleg() as $tp)
                        <option value="{{$tp->id}}" @if($data->tipe_caleg_id==$tp->id) selected @endif >- {{$tp->tipe_caleg}}</option>
                    @endforeach
                
            </select>
        </div>
    </div>
</div>
<div class="row form-group m-b-4">
    <label class="col-md-3 col-form-label">Provinsi</label>
    <div class="col-md-5">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-chevron-down"></i></span></div>
            <select class="form-control provinsi_id" name="provinsi_id" id="provinsi_id" onchange="pilih_provinsi_id(this.value)">
               
                    <option value="">.:: Pilih</option>
                    @foreach(get_provinsi() as $tp)
                        <option value="{{$tp->id}}" @if($tp->id==$data->provinsi_id) selected @endif >- {{$tp->nama}}</option>
                    @endforeach
                
            </select>
        </div>
    </div>
</div>
<div class="row form-group m-b-4 " id="tampil_kota"> 
    <label class="col-md-3 col-form-label">Kota / Kabupaten</label>
    <div class="col-md-5">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-chevron-down"></i></span></div>
            <select class="form-control kota_id" name="kota_id" id="kota_id" onchange="pilih_kota_id(this.value)">
               
                    <option value="">.:: Pilih</option>
                    
                
            </select>
        </div>
    </div>
</div>
<div class="row form-group m-b-4 " id="tampil_kecamatan"> 
    <label class="col-md-3 col-form-label">Kecamatan</label>
    <div class="col-md-5">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-chevron-down"></i></span></div>
            <select class="form-control kecamatan_id" name="kecamatan_id" id="kecamatan_id" onchange="pilih_kecamatan_id(this.value)">
               
                    <option value="">.:: Pilih</option>
                    
                
            </select>
        </div>
    </div>
</div>
<div class="row form-group m-b-4">
    <label class="col-md-3 col-form-label">Foto Caleg </label>
    <div class="col-md-5">
        <div class="input-group">
            <input type="file" name="file" id="profile-img-file-input" class="form-control">
        </div>
    </div>
   
    
</div>
<div class="row form-group m-b-4">
    <label class="col-md-3 col-form-label">&nbsp; </label>
    <div class="col-md-3">
        <div id="imgPreview" style="height:150px;background:#e6e6f1">
            @if($id>0)
                <img width="100%" height="150px" src="{{url_plug()}}/attach/foto_caleg/{{$data->foto}}">
            @endif
        </div>
    </div>
    
</div>


<style>
        
    </style>
<script>
        @if($id>0)
            @if($data->tipe_caleg_id==1)
                $("#tampil_kota").hide();
                $("#tampil_kecamatan").hide();
            @elseif($data->tipe_caleg_id==2)
                $("#tampil_kota").show();
                $("#kota_id").load("{{url('caleg/getkota')}}?id_prov={{$data->provinsi_id}}&id={{$data->kota_id}}");
                $("#tampil_kecamatan").hide();
            @elseif($data->tipe_caleg_id==3)
                $("#tampil_kota").show();
                $("#kota_id").load("{{url('caleg/getkota')}}?id_prov={{$data->provinsi_id}}&id={{$data->kota_id}}");
                $("#tampil_kecamatan").show();
                $("#kecamatan_id").load("{{url('caleg/getkecamatan')}}?id_kota={{$data->kota_id}}&id={{$data->kecamatan_id}}");
            @else
                $("#tampil_kota").hide();
                $("#tampil_kecamatan").hide();
            @endif
        @else
            $("#tampil_kota").hide();
            $("#tampil_kecamatan").hide();
        @endif
        
        $("#tipe_caleg_id").select2();
        $(".provinsi_id").select2();
        $(".kota_id").select2();
        $(".kecamatan_id").select2();
        
        $('#profile-img-file-input').change(function() {
            const file = this.files[0];
            console.log(file);
            if (file) {
                let reader = new FileReader();
                reader.onload = function(event) {
                    console.log(event.target.result);
                    $('#imgPreview').html('<img width="100%" height="150px" src="'+event.target.result+'">');
                }
                reader.readAsDataURL(file);
            }
        });
        function pilih_tipe_caleg(id){
            $("#tipe_caleg_id").select2({ closeOnSelect: true });
            if(id==1){
                $("#tampil_kota").hide();
                $("#tampil_kecamatan").hide();
            }
            else if(id==2){
                $("#tampil_kota").show();
                $("#tampil_kecamatan").hide();
            }
            else if(id==3){
                $("#tampil_kota").show();
                $("#tampil_kecamatan").show();
            }else{
                $("#tampil_kota").hide();
                $("#tampil_kecamatan").hide();
            }
            $(".provinsi_id").select2().val(null).trigger("change");
            $(".kota_id").select2().val(null).trigger("change");
            $(".kecamatan_id").select2().val(null).trigger("change");
        }
        function pilih_provinsi_id(id){
            
            $(".provinsi_id").select2({ closeOnSelect: true });
            $("#kota_id").load("{{url('caleg/getkota')}}?id_prov="+id);
            $("#kecamatan_id").load("{{url('caleg/getkecamatan')}}?id_kota=0");
            
        }
        function pilih_kota_id(id){
            
            $(".kota_id").select2({ closeOnSelect: true });
            $("#kecamatan_id").load("{{url('caleg/getkecamatan')}}?id_kota="+id);
        }
</script>