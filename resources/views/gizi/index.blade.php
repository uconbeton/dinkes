@extends('layouts.app')
@push('datatable')
<style>
    .uuu{

}
    
        .table thead tr th {
            font-weight: 600;
            text-transform: uppercase;
            border-bottom: 1px solid #b8c1ca;
            text-align: center;
            vertical-align: middle;
        }
    
    
</style>
    <script>
        function load_data(){
			$.ajax({ 
                type: 'GET', 
                url: "{{ url('caleg/getdatatipe')}}", 
                data: { ide: 1 }, 
                dataType: 'json',
                beforeSend: function() {
                    
                },
                success: function (data) {
                    $.each(data, function(i, result){
                        if(result.id==1){
                            $('#nilai_provinsi').html(result.total);
                        }
                        if(result.id==2){
                            $('#nilai_kota').html(result.total);
                        }
                        if(result.id==3){
                            $('#nilai_kecamatan').html(result.total);
                        }
                    });
                  
                }
            });
        }
        
        
       
       
        setTimeout(()=> {
            @if($act==2)
            for(var i=2; i<=25;i++)
            {
                load_isi(i);
            }
            @else
            for(var i=2; i<=17;i++)
            {
                load_isi(i);
            }
            @endif
        }, 1000);

        function load_isi(num){
        
            
            var total=0;
            $('table tbody tr').each(function(){
                var value= parseInt($('td',this).eq(num).text());
                if(value>0){
                    total +=value;
                }else{
                    total +=0;
                }
                
            });
            $('.nilai_'+num).text(+total);
            
        
        }

        $(document).ready(function() {
            
            @if($act==0)
                var table=$('#data-table-fixed-header').DataTable({
                        lengthMenu: [20, 40, 60],
                        lengthChange:false,
                        paging:false,
                        fixedHeader: {
                            header: true,
                            headerOffset: $('#header').height()
                        },
                        responsive: false,
                        ajax:"{{ url('gzkecamatan/'.$data->id.'/getdata')}}?bulan={{$bulan}}&tahun={{$tahun}}",
                        dom: 'lrtip',
                        columns: [
                            { data: 'id', render: function (data, type, row, meta) 
                                {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                } 
                            },
                            
                            { data: 'kelurahan' },
                            { data: 'SKL23' },
                            { data: 'SKP23' },
                            { data: 'SKL59' },
                            { data: 'SKP59' },
                            { data: 'KRL23' },
                            { data: 'KRP23' },
                            { data: 'KRL59' },
                            { data: 'KRP59' },
                            { data: 'BBNL23' },
                            { data: 'BBNP23' },
                            { data: 'BBNL59' },
                            { data: 'BBNP59' },
                            { data: 'RSL23' },
                            { data: 'RSP23' },
                            { data: 'RSL59' },
                            { data: 'RSP59' },
                            
                        ],
                        
                        language: {
                            paginate: {
                                // remove previous & next text from pagination
                                previous: '<< previous',
                                next: 'Next>>'
                            }
                        }
                    });
                    
            @endif
            @if($act==1)
                var table=$('#data-table-fixed-header').DataTable({
                        lengthMenu: [20, 40, 60],
                        lengthChange:false,
                        paging:false,
                        fixedHeader: {
                            header: true,
                            headerOffset: $('#header').height()
                        },
                        responsive: false,
                        ajax:"{{ url('gzkecamatan/'.$data->id.'/getdata')}}?bulan={{$bulan}}&tahun={{$tahun}}",
                        dom: 'lrtip',
                        columns: [
                            { data: 'id', render: function (data, type, row, meta) 
                                {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                } 
                            },
                            
                            { data: 'kelurahan' },
                            { data: 'SPL23' },
                            { data: 'SPP23' },
                            { data: 'SPL59' },
                            { data: 'SPP59' },
                            { data: 'PDL23' },
                            { data: 'PDP23' },
                            { data: 'PDL59' },
                            { data: 'PDP59' },
                            { data: 'NRL23' },
                            { data: 'NRP23' },
                            { data: 'NRL59' },
                            { data: 'NRP59' },
                            { data: 'TGL23' },
                            { data: 'TGP23' },
                            { data: 'TGL59' },
                            { data: 'TGP59' },
                            
                        ],
                        
                        language: {
                            paginate: {
                                // remove previous & next text from pagination
                                previous: '<< previous',
                                next: 'Next>>'
                            }
                        }
                    });
                    
            @endif
            @if($act==2)
                var table=$('#data-table-fixed-header').DataTable({
                        lengthMenu: [20, 40, 60],
                        lengthChange:false,
                        paging:false,
                        fixedHeader: {
                            header: true,
                            headerOffset: $('#header').height()
                        },
                        responsive: false,
                        ajax:"{{ url('gzkecamatan/'.$data->id.'/getdata')}}?bulan={{$bulan}}&tahun={{$tahun}}",
                        dom: 'lrtip',
                        columns: [
                            { data: 'id', render: function (data, type, row, meta) 
                                {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                } 
                            },
                            
                            { data: 'kelurahan' },
                            { data: 'GBL23' },
                            { data: 'GBP23' },
                            { data: 'GBL59' },
                            { data: 'GBP59' },
                            { data: 'GKL23' },
                            { data: 'GKP23' },
                            { data: 'GKL59' },
                            { data: 'GKP59' },
                            { data: 'GNL23' },
                            { data: 'GNP23' },
                            { data: 'GNL59' },
                            { data: 'GNP59' },
                            { data: 'RGLL23' },
                            { data: 'RGLP23' },
                            { data: 'RGLL59' },
                            { data: 'RGLP59' },
                            { data: 'GLL23' },
                            { data: 'GLP23' },
                            { data: 'GLL59' },
                            { data: 'GLP59' },
                            { data: 'OBL23' },
                            { data: 'OBP23' },
                            { data: 'OBL59' },
                            { data: 'OBP59' },
                            
                        ],
                        
                        language: {
                            paginate: {
                                // remove previous & next text from pagination
                                previous: '<< previous',
                                next: 'Next>>'
                            }
                        }
                    });
                    
            @endif

		});

        
    </script>
@endpush
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb float-xl-right">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">Daftar Status Gizi Anak Kecamatan {{$data->nama}}</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Daftar Status Gizi Anak Kecamatan {{$data->nama}} <small> E-APPC</small></h1>
        <!-- end page-header -->
        <!-- begin row -->
        <div class="row">
            <!-- begin col-2 -->
            
            <!-- end col-2 -->
            <!-- begin col-10 -->
            <div class="col-xl-12">
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title">&nbsp;</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin alert -->
                    <div class="row" style="padding:2%">
                        <div class="col-md-2">
                            <span class="btn btn-sm btn-primary" onclick="add_data(0)"><i class="fas fa-cloud-upload-alt fa-fw"></i> Import data</span>
                        </div>
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-2">
                            <select id="tahun" class="form-control">
                                <option>.:: Tahun</option>
                                @for($x=date('Y');$x>=2020;$x--)
                                <option value="{{$x}}" @if($tahun==$x) selected @endif>{{$x}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select id="bulan" class="form-control">
                                <option>.:: Triwulan</option>
                                <option value="1"  @if($bulan==1) selected @endif > Triwulan 1</option>
                                <option value="2" @if($bulan==2) selected @endif > Triwulan 2</option>
                                <option value="3" @if($bulan==3) selected @endif > Triwulan 3</option>
                                <option value="4" @if($bulan==4) selected @endif> Triwulan 4</option>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-sm btn-success" onclick="cari()"><i class="fas fa-search fa-fw"></i> Cari</span>
                        </div>
                    </div>
                    <!-- end alert -->
                    <!-- begin panel-body -->
                    <div class="panel-body" style="background: #eeeef5;" >
                        <div class="row">
                            
                            <div class="col-md-12">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a href="#default-tab-1" onclick="cari_act(0)"  class="nav-link @if($act==0) active  @endif ">
                                            <span class="d-sm-none"><i class="fas fa-chart-pie"></i> Berat Badan</span>
                                            <span class="d-sm-block d-none"><i class="fas fa-chart-pie"></i> Berat Badan</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#default-tab-2" onclick="cari_act(1)"  class="nav-link  @if($act==1) active  @endif ">
                                            <span class="d-sm-none"><i class="fas fa-chart-pie"></i> Tinggi Badan</span>
                                            <span class="d-sm-block d-none"><i class="fas fa-chart-pie"></i> Tinggi Badan</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#default-tab-3" onclick="cari_act(2)"  class="nav-link  @if($act==2) active  @endif ">
                                            <span class="d-sm-none"><i class="fas fa-chart-pie"></i> Tinggi Badan</span>
                                            <span class="d-sm-block d-none"><i class="fas fa-chart-pie"></i> Tinggi Badan</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="default-tab-1">
                                            @if($act==0)
                                                <div class="table-responsive">
                                                    <table id="data-table-fixed-header" class="table table-striped table-bordered table-td-valign-middle">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="3" width="1%" data-orderable="false">NO</th>
                                                                <th rowspan="3"  class="text-nowrap">Kelurahan</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Sangat Kurang</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Kurang</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Berat Badan Normal</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Risiko Lebih</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                            </tr>
                                                        
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th></th>
                                                                <th> TOTAL</th>
                                                                <th class="nilai_2"></th>
                                                                <th class="nilai_3"></th>
                                                                <th class="nilai_4"></th>
                                                                <th class="nilai_5"></th>
                                                                <th class="nilai_6"></th>
                                                                <th class="nilai_7"></th>
                                                                <th class="nilai_8"></th>
                                                                <th class="nilai_9"></th>
                                                                <th class="nilai_10"></th>
                                                                <th class="nilai_11"></th>
                                                                <th class="nilai_12"></th>
                                                                <th class="nilai_13"></th>
                                                                <th class="nilai_14"></th>
                                                                <th class="nilai_15"></th>
                                                                <th class="nilai_16"></th>
                                                                <th class="nilai_17"></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            @endif
                                            @if($act==1)
                                                <div class="table-responsive">
                                                    <table id="data-table-fixed-header" class="table table-striped table-bordered table-td-valign-middle">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="3" width="1%" data-orderable="false">NO</th>
                                                                <th rowspan="3"  class="text-nowrap">Kelurahan</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Sangat Pendek</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Pendek</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Normal</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Tinggi</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                            </tr>
                                                        
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th></th>
                                                                <th> TOTAL</th>
                                                                <th class="nilai_2"></th>
                                                                <th class="nilai_3"></th>
                                                                <th class="nilai_4"></th>
                                                                <th class="nilai_5"></th>
                                                                <th class="nilai_6"></th>
                                                                <th class="nilai_7"></th>
                                                                <th class="nilai_8"></th>
                                                                <th class="nilai_9"></th>
                                                                <th class="nilai_10"></th>
                                                                <th class="nilai_11"></th>
                                                                <th class="nilai_12"></th>
                                                                <th class="nilai_13"></th>
                                                                <th class="nilai_14"></th>
                                                                <th class="nilai_15"></th>
                                                                <th class="nilai_16"></th>
                                                                <th class="nilai_17"></th>
                                                            </tr>
                                                        </tfoot>
                                                        
                                                    </table>
                                                </div>
                                            @endif
                                            @if($act==2)
                                                <div class="table-responsive">
                                                    <table id="data-table-fixed-header" style="width:130%" class="table table-striped table-bordered table-td-valign-middle">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="3" width="1%" data-orderable="false">NO</th>
                                                                <th rowspan="3"  class="text-nowrap">Kelurahan</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Gizi Buruk</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Gizi Kurang</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Normal</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Risiko Gizi Lebih</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Gizi Lebih</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Obesitas</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                            </tr>
                                                        
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th></th>
                                                                <th> TOTAL</th>
                                                                <th class="nilai_2"></th>
                                                                <th class="nilai_3"></th>
                                                                <th class="nilai_4"></th>
                                                                <th class="nilai_5"></th>
                                                                <th class="nilai_6"></th>
                                                                <th class="nilai_7"></th>
                                                                <th class="nilai_8"></th>
                                                                <th class="nilai_9"></th>
                                                                <th class="nilai_10"></th>
                                                                <th class="nilai_11"></th>
                                                                <th class="nilai_12"></th>
                                                                <th class="nilai_13"></th>
                                                                <th class="nilai_14"></th>
                                                                <th class="nilai_15"></th>
                                                                <th class="nilai_16"></th>
                                                                <th class="nilai_17"></th>
                                                                <th class="nilai_18"></th>
                                                                <th class="nilai_19"></th>
                                                                <th class="nilai_20"></th>
                                                                <th class="nilai_21"></th>
                                                                <th class="nilai_22"></th>
                                                                <th class="nilai_23"></th>
                                                                <th class="nilai_24"></th>
                                                                <th class="nilai_25"></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            @endif
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- end panel-body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-10 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@section('modal')

        <div id="modal-form" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;">                                               
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Import Data Status Gizi</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydataform" method="post" action="{{ url('gzkecamatan/'.$data->id.'/import') }}" enctype="multipart/form-data" >
                            @csrf
                            <!-- <input type="submit"> -->
                            <div class="note note-yellow m-b-15">
                                <div class="note-icon f-s-20">
                                    <i class="fa fa-lightbulb fa-2x"></i>
                                </div>
                                <div class="note-content">
                                    <h4 class="m-t-5 m-b-5 p-b-2">Notes</h4>
                                    <ul class="m-b-5 p-l-25">
                                        <li>Import Data Status Gizi Kecamatan {{$data->nama}} </li>
                                        <li>File Hanya berformat (xls,xlsx)</li>
                                        <li>Tunggu hingga proses import selesai</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="form-group row m-b-5">
                                <label class="col-form-label col-md-3">Kecamatan</label>
                                <div class="col-md-9">
                                    <input type="hidden"  class="form-control m-b-5" placeholder="" name="kecamatan_id" value="{{$data->id}}" />
                                    <input type="text" disabled class="form-control m-b-5" placeholder="" value="{{$data->nama}}" />
                                </div>
                            </div>
                            <div class="form-group row m-b-5">
                                <label class="col-form-label col-md-3">Periode</label>
                                <div class="col-md-3">
                                    <select name="tahun" class="form-control">
                                        <option>.:: Tahun</option>
                                        @for($x=date('Y');$x>=2020;$x--)
                                        <option value="{{$x}}">{{$x}}</option>
                                       @endfor
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select name="bulan" class="form-control">
                                        <option>.:: Triwulan</option>
                                        <option value="1"> Triwulan 1</option>
                                        <option value="2"> Triwulan 2</option>
                                        <option value="3"> Triwulan 3</option>
                                        <option value="4"> Triwulan 4</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row m-b-5">
                                <label class="col-form-label col-md-3">File (xls,xlsx)</label>
                                <div class="col-md-9">
                                    <input type="file"  class="form-control m-b-5" placeholder="" name="file" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Batal</a>
                        <a href="javascript:;" class="btn btn-success" id="save-data">Proses Import</a>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
@endsection


@push('ajax')
    <script>
        function cari(){
            var bulan=$('#bulan').val();
            var tahun=$('#tahun').val();
            location.assign("{{url('gzkecamatan/'.$data->id)}}?act={{$act}}&bulan="+bulan+"&tahun="+tahun)
        }
        function cari_act(act){
            var bulan=$('#bulan').val();
            var tahun=$('#tahun').val();
            location.assign("{{url('gzkecamatan/'.$data->id)}}?act="+act+"&bulan="+bulan+"&tahun="+tahun)
        }
        
        function add_data(id){
            $('#modal-form').modal('show');
            $('#tampil-form').load("{{url('caleg/modal')}}?id="+id);
        }


        function delete_data(id){
            
            swal({
                title: "Yakin menghapus data ini ?",
                text: "data akan hilang dari data  ini",
                type: "warning",
                icon: "error",
                showCancelButton: true,
                align:"center",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then((willDelete) => {
                if (willDelete) {
                     
                        $.ajax({
                            type: 'GET',
                            url: "{{url('caleg/delete')}}",
                            data: "id="+id,
                            success: function(msg){
                                swal("Success! berhasil terhapus!", {
                                    icon: "success",
                                });
                                load_data();
                                var tables=$('#data-table-fixed-header').DataTable();
                                    tables.ajax.url("{{ url('caleg/getdata')}}").load();
                            }
                        });
                    
                } else {
                    
                }
            });
            
        } 

        $('#save-data').on('click', () => {
            var bulan=$('#bulan').val();
            var tahun=$('#tahun').val();
            
            var form=document.getElementById('mydataform');
                $.ajax({
                    type: 'POST',
                    url: "{{ url('gzkecamatan/'.$data->id.'/import') }}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function() {
                        document.getElementById("loadnya").style.width = "100%";
                    },
                    success: function(msg){
                        var bat=msg.split('@');
                        if(bat[1]=='ok'){
                            document.getElementById("loadnya").style.width = "0px";
                            swal("Success! diproses !", {
                                icon: "success",
                            });
                            $('#modal-form').modal('hide');
                            location.assign("{{url('gzkecamatan/'.$data->id)}}?act={{$act}}&bulan="+bulan+"&tahun="+tahun)
                        }else{
                            document.getElementById("loadnya").style.width = "0px";
                            swal({
                                title: 'Notifikasi',
                               
                                html:true,
                                text:'ss',
                                icon: 'error',
                                buttons: {
                                    cancel: {
                                        text: 'Tutup',
                                        value: null,
                                        visible: true,
                                        className: 'btn btn-dangers',
                                        closeModal: true,
                                    },
                                    
                                }
                            });
                            $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:1%;text-align:left;font-size:13px">'+msg+'</div>')
                        }
                        
                        
                    }
                });
        });
    </script>
@endpush