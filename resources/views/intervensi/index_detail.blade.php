@extends('layouts.app_web')

@section('content')
<style>
    .ttdh{
        font-size:11px;
        padding:1%;
        vertical-align:middle !important;
        border:solid 1px #75757c !important;
        background: #baf9ba;
    }
    th{
        font-size:12px;
        background: #ffedc2;
        vertical-align:middle
    }
    td{
        font-size:12px;
        padding: 5px 10px !important;
        background:#fff;
    }
</style>
<div id="page-title" class="page-title has-bg" style="">
	<div class="bg-cvr"  id="bg-cvr" style="background-image: linear-gradient(to bottom, #ffffff, #dad4f4, #b6abe8, #9182db, #6a5bcd);"></div>
    <div class="container" style="color:#000">
            
            <div class="row row-space-30 " style="margin-top:1%;">
            
				
                <div class="col-lg-12" style="text-align:center">
                <img src="{{url_plug()}}/assets/img/cover/logo.png?v={{date('ymdhis')}}" width="10%">
                    <h3 class="page-header" style="padding:2%;text-align:center;margin-bottom: 3%;"><b>KOTA CILEGON</b><br>Data Intervensi Spesifik Kecamatan {{$data->nama}}<br><small>Tahun {{$tahun}}</small></h3>
                </div>
            </div>
			<div class="row row-space-30" style="background:blue">
            
                <!-- <div class="col-lg-12 layout-mobile" style="text-align:center">
                    <table width="100%">
                        <tr>
                            <td width="50%"><a href="javascript:;" class="btn btn-white btn-block"> Lihat Detail Stunting</a></td>
                            <td><a href="javascript:;" class="btn btn-blue btn-block"> Ubah Tahun Tampilan</a></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-12  col-xs-2 m-b-5 layout-web" style="text-align:center">
                    &nbsp;
				</div> -->
			</div>
            @foreach(get_indikator(2) as $o)   
                @if($o->id==3 || $o->id==4)
                <div class="row row-space-30" style="margin-top:1%;margin-top: 1%; ">
                    
                    <div class="col-lg-12" style=" padding-top: 1%;text-align:left;border-bottom: solid 3px #dfdfe9;padding:0px">
                        
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #f7f7ff;">
                        <h5 class="page-header" style="padding:2%;text-align:center;margin-bottom: 2%;text-transform: uppercase;"><b>Tabel Nilai</b><br>   <small>{{$o->keterangan}}</small></h5>
                    
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #ffffff; border: solid 1px #e8e8ef;">
                            <div class="table-responsive">
								<table class="table table-bordered m-b-0">
									<thead>
										<tr>
											<th colspan="12">{{$o->keterangan}}</th>
										</tr>
										<tr>
											<th colspan="3">Triwulan Ke 1</th>
											<th colspan="3">Triwulan Ke 2</th>
											<th colspan="3">Triwulan Ke 3</th>
											<th colspan="3">Triwulan Ke 4</th>
										</tr>
										<tr>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											
										</tr>
									</thead>
									<tbody id="tampil-data-{{$o->id}}">
										
									</tbody>
                                    
								</table>
							</div>
                        
                    </div>
                    
                    
                </div>
                @else
                <div class="row row-space-30" style="margin-top:1%;margin-top: 1%; ">
                    
                    <div class="col-lg-12" style=" padding-top: 1%;text-align:left;border-bottom: solid 3px #dfdfe9;padding:0px">
                        
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #f7f7ff;">
                        <h5 class="page-header" style="padding:2%;text-align:center;margin-bottom: 2%;text-transform: uppercase;"><b>Tabel Nilai</b><br>   <small>{{$o->keterangan}}</small></h5>
                    
                    </div>
                    <div class="col-lg-12" style=" padding-bottom: 1%; padding-top: 1%;background: #ffffff; border: solid 1px #e8e8ef;">
                            <div class="table-responsive">
								<table class="table table-bordered m-b-0">
									<thead>
										<tr>
											<th rowspan="2" >No</th>
											<th rowspan="2" >Kelurahan</th>
											<th rowspan="2" >Tahun</th>
											<th colspan="3">Triwulan Ke 1</th>
											<th colspan="3">Triwulan Ke 2</th>
											<th colspan="3">Triwulan Ke 3</th>
											<th colspan="3">Triwulan Ke 4</th>
										</tr>
										<tr>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											<th width="5%">Sasaran</th>
											<th width="5%">Capaian</th>
											<th width="5%">%</th>
											
										</tr>
									</thead>
									<tbody id="tampil-data-{{$o->id}}">
										
									</tbody>
                                    <tfoot>
										<tr>
											<th colspan="3">Total</th>
											<th id="tot{{$o->id}}_tw1"></th>
											<th id="tot{{$o->id}}_tc1"></th>
											<th id="tot{{$o->id}}_persen1"></th>
											<th id="tot{{$o->id}}_tw2"></th>
											<th id="tot{{$o->id}}_tc2"></th>
											<th id="tot{{$o->id}}_persen2"></th>
											<th id="tot{{$o->id}}_tw3"></th>
											<th id="tot{{$o->id}}_tc3"></th>
											<th id="tot{{$o->id}}_persen3"></th>
											<th id="tot{{$o->id}}_tw4"></th>
											<th id="tot{{$o->id}}_tc4"></th>
											<th id="tot{{$o->id}}_persen4"></th>
										
										</tr>
									</tfoot>
								</table>
							</div>
                        
                    </div>
                    
                    
                </div>
                @endif
            @endforeach
    </div>
</div>
    
@endsection

@section('modal')

        <div id="modal-form" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;">                                               
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form ( Create / Update )</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydataform" method="post" action="{{ url('employe') }}" enctype="multipart/form-data" >
                            @csrf
                            <div id="tampil-form"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                        <a href="javascript:;" class="btn btn-success" id="save-data">Save</a>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
@endsection
@push('ajax')
	<script src="{{url_plug()}}/assets/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="{{url_plug()}}/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>
	<script>
		/*
		Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
		Version: 4.6.0
		Author: Sean Ngu
		Website: http://www.seantheme.com/color-admin/admin/
        
		*/
        @foreach(get_indikator(2) as $o)
                
                function get_dta{{$o->id}}(){
                    @if($o->id==3 || $o->id==4) 
                    $.ajax({ 
                                type: 'GET', 
                                url: "{{ url('intervensi/'.$data->id.'/getdata')}}?indikator_id={{$o->id}}", 
                                data: { ide: 1 }, 
                                dataType: 'json',
                                beforeSend: function() {
                                    $("#tampil-data-{{$o->id}}").html("");
                                },
                                success: function (data) {
                                   
                                        var tampil='<tr>'
                                                        +'<td class="ttdd">'+data.sasaran1+'</td>'
                                                        +'<td class="ttdd">'+data.capaian1+'</td>'
                                                        +'<td class="ttdd">'+data.persen1+'%</td>'
                                                        +'<td class="ttdd">'+data.sasaran2+'</td>'
                                                        +'<td class="ttdd">'+data.capaian2+'</td>'
                                                        +'<td class="ttdd">'+data.persen2+'%</td>'
                                                        +'<td class="ttdd">'+data.sasaran3+'</td>'
                                                        +'<td class="ttdd">'+data.capaian3+'</td>'
                                                        +'<td class="ttdd">'+data.persen3+'%</td>'
                                                        +'<td class="ttdd">'+data.sasaran4+'</td>'
                                                        +'<td class="ttdd">'+data.capaian4+'</td>'
                                                        +'<td class="ttdd">'+data.persen4+'%</td>'
                                                    +'</tr>';
                                        $("#tampil-data-{{$o->id}}").append(tampil);
                                   
                                }
                            });
                    @else
                            $.ajax({ 
                                type: 'GET', 
                                url: "{{ url('intervensi/'.$data->id.'/getdata')}}?indikator_id={{$o->id}}", 
                                data: { ide: 1 }, 
                                dataType: 'json',
                                beforeSend: function() {
                                    $("#tampil-data-{{$o->id}}").html("");
                                },
                                success: function (data) {
                                    $('#tot{{$o->id}}_tw1').html(data.tot_tw1);
                                    $('#tot{{$o->id}}_tw2').html(data.tot_tw2);
                                    $('#tot{{$o->id}}_tw3').html(data.tot_tw3);
                                    $('#tot{{$o->id}}_tw4').html(data.tot_tw4);
                                    $('#tot{{$o->id}}_tc1').html(data.tot_tc1);
                                    $('#tot{{$o->id}}_tc2').html(data.tot_tc2);
                                    $('#tot{{$o->id}}_tc3').html(data.tot_tc3);
                                    $('#tot{{$o->id}}_tc4').html(data.tot_tc4);
                                    $('#tot{{$o->id}}_persen1').html(data.tot_persen1);
                                    $('#tot{{$o->id}}_persen2').html(data.tot_persen2);
                                    $('#tot{{$o->id}}_persen3').html(data.tot_persen3);
                                    $('#tot{{$o->id}}_persen4').html(data.tot_persen4);
                                    $.each(data.data, function(i, result){
                                        if(result.total>0){
                                            var color="red";
                                        }else{
                                            var color="blueblue";
                                        }
                                        var tampil='<tr>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+(i+1)+'</td>'
                                                        +'<td class="ttdd" style="text-align:left;background: #9b9be9; color: #fff;">'+result.nama+'</td>'
                                                        +'<td class="ttdd">'+result.tahun+'</td>'
                                                        +'<td class="ttdd">'+result.sasaran1+'</td>'
                                                        +'<td class="ttdd">'+result.capaian1+'</td>'
                                                        +'<td class="ttdd">'+result.persen1+'%</td>'
                                                        +'<td class="ttdd">'+result.sasaran2+'</td>'
                                                        +'<td class="ttdd">'+result.capaian2+'</td>'
                                                        +'<td class="ttdd">'+result.persen2+'%</td>'
                                                        +'<td class="ttdd">'+result.sasaran3+'</td>'
                                                        +'<td class="ttdd">'+result.capaian3+'</td>'
                                                        +'<td class="ttdd">'+result.persen3+'%</td>'
                                                        +'<td class="ttdd">'+result.sasaran4+'</td>'
                                                        +'<td class="ttdd">'+result.capaian4+'</td>'
                                                        +'<td class="ttdd">'+result.persen4+'%</td>'
                                                    
                                                    +'</tr>';
                                        $("#tampil-data-{{$o->id}}").append(tampil);
                                    });
                                
                                }
                            });
                    @endif
                }
        @endforeach
		setTimeout(()=> {
         
        }, 1000);

        function cari_act(act){
            location.assign("{{url('intervensi/'.$data->id)}}?bulan={{$bulan}}&act="+act+"&tahun={{$tahun}}")
        }
        function pilih_triwulan(act){
            location.assign("{{url('intervensi/'.$data->id)}}?tahun={{$tahun}}&bulan="+act)
        }
		$(document).ready(function() {
            @foreach(get_indikator(2) as $o)   
                get_dta{{$o->id}}();
            @endforeach
			
		});

        setTimeout(()=> {
            @if($act==2)
            for(var i=2; i<=25;i++)
            {
                load_isi(i);
            }
            @else
            for(var i=2; i<=17;i++)
            {
                load_isi(i);
            }
            @endif
        }, 1000);
        function load_isi(num){
        
            
            var total=0;
            $('table tbody tr').each(function(){
                var value= parseInt($('td',this).eq(num).text());
                if(value>0){
                    total +=value;
                }else{
                    total +=0;
                }
                
            });
            $('.nilai_'+num).text(+total);
            
        
        }
        $(document).ready(function() {
            
           

		});
	</script>

@endpush
