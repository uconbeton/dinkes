@extends('layouts.app_web')

@section('content')
<style>
    .ttdh{
        font-size:11px;
        padding:1%;
        vertical-align:middle !important;
        border:solid 1px #75757c !important;
        background: #baf9ba;
    }
    .ttdd{
        font-size:11px;
        background:#fff;
        border:solid 1px #75757c !important;
    }
</style>
<div id="page-title" class="page-title has-bg" style="">
	<div class="bg-cvr" id="bg-cvr" style="background-image: linear-gradient(to bottom, #ffffff, #dad4f4, #b6abe8, #9182db, #6a5bcd);"></div>
    <div class="container" style="color:#000">
            
            <div class="row  " style="margin-top:0;">
            
				
                <div class="col-lg-12" style="text-align:center">
                    <h3 class="page-header" style="padding:2%;text-align:center;margin-bottom: 3%;"><b>KOTA CILEGON</b><br>Data Stunting Kecamatan {{$data->nama}}<br><small>Tahun {{$tahun}}</small></h3>
                </div>
            </div>
			<div class="row " style="background:blue">
            
                <!-- <div class="col-lg-12 layout-mobile" style="text-align:center">
                    <table width="100%">
                        <tr>
                            <td width="50%"><a href="javascript:;" class="btn btn-white btn-block"> Lihat Detail Stunting</a></td>
                            <td><a href="javascript:;" class="btn btn-blue btn-block"> Ubah Tahun Tampilan</a></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-12  col-xs-2 m-b-5 layout-web" style="text-align:center">
                    &nbsp;
				</div> -->
			</div>
                
            <div class="row row-space-30" style="margin-top:1%;margin-top: 1%; ">
                
                <div class="col-lg-12" style=" padding-top: 1%;text-align:left;border-bottom: solid 3px #dfdfe9;padding:0px">
                    <div class="btn-group">
                        <button onclick="pilih_triwulan(1)" class="btn btn-white " @if($bulan==1) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 1</button>
                        <button onclick="pilih_triwulan(2)" class="btn btn-white " @if($bulan==2) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 2</button>
                        <button onclick="pilih_triwulan(3)" class="btn btn-white " @if($bulan==3) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 3</button>
                        <button onclick="pilih_triwulan(4)" class="btn btn-white " @if($bulan==4) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 4</button>
                        
                    </div>
				</div>
                <div class="col-lg-8" style=" padding-bottom: 1%; padding-top: 1%;background: #f7f7ff;">
                    <h5 class="page-header" style="padding:2%;text-align:center;margin-bottom: 3%;text-transform: capitalize;"><b>grafik</b>  persentase stunting </h5>
                    <canvas id="bar-chart" data-render="chart-js"></canvas>
                    
				</div>
                <div class="col-lg-4" style="padding-top: 1%;background: #9e9ea7; color: #fff;">
                        <h5 class="page-header" style="padding:2%;text-align:center;margin-bottom: 3%;text-transform: capitalize;"><b>tabel </b> persentase stunting </h5>
                        <table class="table">
                            <tbody> 
                                <tr>
                                    <td width="50%" style="padding: 1%;font-size:11px; background: blue; color: #fff;">NR (Normal)</td>
                                    <td  style="padding: 1%;font-size:11px; background: red; color: #fff;">PD (Pendek)</td>
                                 </tr>
                                <tr>
                                    <td  style="padding: 1%;font-size:11px; background: yellow; color: #000;">TG (Tinggi)</td>
                                    <td  style="padding: 1%;font-size:11px; background: orange; color: #fff;">SP (Sangat Pendek)</td>
                                 </tr>
                            </tbody>
                        </table>
                        <div class="table-responsive">
							<table class="table table-bordered table-panel mb-0">
								<thead>
									<tr>	
										<th  rowspan="2" class="ttdh">KELURAHAN</th>
										<th  colspan="4" class="ttdh">KATEGORI</th>
									</tr>
									<tr>	
										<th  class="ttdh">NR</th>
										<th  class="ttdh">TG</th>
										<th  class="ttdh">PD</th>
										<th  class="ttdh">SP</th>
									</tr>
								</thead>
								<tbody id="tampil-data">
									
									
								</tbody>
							</table>
						</div>
                    
				</div>
                
				
			</div>
    </div>
</div>
<div id="content" class="content" style="padding-top: 1px;">
		<!-- begin container -->
		<div class="container">
			<!-- begin row -->
			<div class="row">
                            
                            <div class="col-md-12" style="padding: 0px;">
                                <div>
                                    <h5 class="page-header" style="padding:2%;text-align:center;margin-bottom: 1%;text-transform: capitalize;"><b>tabel  Nilai stunting </b> <br><small>Triwulan {{$bulan}} Tahun {{$tahun}}</small></h5>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a href="#default-tab-1" onclick="cari_act(0)"  class="nav-link @if($act==0) active  @endif ">
                                            <span class="d-sm-none"><i class="fas fa-chart-pie"></i> Berat Badan Per Umur</span>
                                            <span class="d-sm-block d-none"><i class="fas fa-chart-pie"></i> Berat Badan  Per Umur</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#default-tab-2" onclick="cari_act(1)"  class="nav-link  @if($act==1) active  @endif ">
                                            <span class="d-sm-none"><i class="fas fa-chart-pie"></i> Tinggi Badan  Per Umur</span>
                                            <span class="d-sm-block d-none"><i class="fas fa-chart-pie"></i> Tinggi Badan  Per Umur</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#default-tab-3" onclick="cari_act(2)"  class="nav-link  @if($act==2) active  @endif ">
                                            <span class="d-sm-none"><i class="fas fa-chart-pie"></i>Berat Badan Per Tinggi Badan</span>
                                            <span class="d-sm-block d-none"><i class="fas fa-chart-pie"></i>Berat Badan Per Tinggi Badan</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" style="padding: 1%; background: #fff; border: solid 1px white;" id="default-tab-1">
                                            @if($act==0)
                                                <div class="table-responsive">
                                                    <table id="data-table-fixed-header" class="table table-striped table-bordered table-td-valign-middle">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="3" width="4%" data-orderable="false">NO</th>
                                                                <th rowspan="3"  class="text-nowrap">Kelurahan</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Sangat Kurang</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Kurang</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Berat Badan Normal</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Risiko Lebih</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                            </tr>
                                                        
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th></th>
                                                                <th> TOTAL</th>
                                                                <th class="nilai_2"></th>
                                                                <th class="nilai_3"></th>
                                                                <th class="nilai_4"></th>
                                                                <th class="nilai_5"></th>
                                                                <th class="nilai_6"></th>
                                                                <th class="nilai_7"></th>
                                                                <th class="nilai_8"></th>
                                                                <th class="nilai_9"></th>
                                                                <th class="nilai_10"></th>
                                                                <th class="nilai_11"></th>
                                                                <th class="nilai_12"></th>
                                                                <th class="nilai_13"></th>
                                                                <th class="nilai_14"></th>
                                                                <th class="nilai_15"></th>
                                                                <th class="nilai_16"></th>
                                                                <th class="nilai_17"></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            @endif
                                            @if($act==1)
                                                <div class="table-responsive">
                                                    <table id="data-table-fixed-header" class="table table-striped table-bordered table-td-valign-middle">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="3" width="1%" data-orderable="false">NO</th>
                                                                <th rowspan="3"  class="text-nowrap">Kelurahan</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Sangat Pendek</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Pendek</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Normal</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Tinggi</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                                <th class="text-nowrap" width="5%" >L</th>
                                                                <th class="text-nowrap" width="5%" >P</th>
                                                            </tr>
                                                        
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th></th>
                                                                <th> TOTAL</th>
                                                                <th class="nilai_2"></th>
                                                                <th class="nilai_3"></th>
                                                                <th class="nilai_4"></th>
                                                                <th class="nilai_5"></th>
                                                                <th class="nilai_6"></th>
                                                                <th class="nilai_7"></th>
                                                                <th class="nilai_8"></th>
                                                                <th class="nilai_9"></th>
                                                                <th class="nilai_10"></th>
                                                                <th class="nilai_11"></th>
                                                                <th class="nilai_12"></th>
                                                                <th class="nilai_13"></th>
                                                                <th class="nilai_14"></th>
                                                                <th class="nilai_15"></th>
                                                                <th class="nilai_16"></th>
                                                                <th class="nilai_17"></th>
                                                            </tr>
                                                        </tfoot>
                                                        
                                                    </table>
                                                </div>
                                            @endif
                                            @if($act==2)
                                                <div class="table-responsive">
                                                    <table id="data-table-fixed-header" style="width:130%" class="table table-striped table-bordered table-td-valign-middle">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="3" width="1%" data-orderable="false">NO</th>
                                                                <th rowspan="3"  class="text-nowrap">Kelurahan</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Gizi Buruk</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Gizi Kurang</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Normal</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Risiko Gizi Lebih</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Gizi Lebih</th>
                                                                <th rowspan="1" class="text-nowrap" colspan="4" >Obesitas</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                                <th class="text-nowrap" colspan="2" >0-23</th>
                                                                <th class="text-nowrap" colspan="2">0-59</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                                <th class="text-nowrap" width="3%" >L</th>
                                                                <th class="text-nowrap" width="3%" >P</th>
                                                            </tr>
                                                        
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th></th>
                                                                <th> TOTAL</th>
                                                                <th class="nilai_2"></th>
                                                                <th class="nilai_3"></th>
                                                                <th class="nilai_4"></th>
                                                                <th class="nilai_5"></th>
                                                                <th class="nilai_6"></th>
                                                                <th class="nilai_7"></th>
                                                                <th class="nilai_8"></th>
                                                                <th class="nilai_9"></th>
                                                                <th class="nilai_10"></th>
                                                                <th class="nilai_11"></th>
                                                                <th class="nilai_12"></th>
                                                                <th class="nilai_13"></th>
                                                                <th class="nilai_14"></th>
                                                                <th class="nilai_15"></th>
                                                                <th class="nilai_16"></th>
                                                                <th class="nilai_17"></th>
                                                                <th class="nilai_18"></th>
                                                                <th class="nilai_19"></th>
                                                                <th class="nilai_20"></th>
                                                                <th class="nilai_21"></th>
                                                                <th class="nilai_22"></th>
                                                                <th class="nilai_23"></th>
                                                                <th class="nilai_24"></th>
                                                                <th class="nilai_25"></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            @endif
                                </div>
                            </div>
                            
                        </div>
		</div>
		<!-- end container -->
	</div>
@endsection

@section('modal')

        <div id="modal-form" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;">                                               
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form ( Create / Update )</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydataform" method="post" action="{{ url('employe') }}" enctype="multipart/form-data" >
                            @csrf
                            <div id="tampil-form"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                        <a href="javascript:;" class="btn btn-success" id="save-data">Save</a>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
@endsection
@push('ajax')
	<script src="{{url_plug()}}/assets/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="{{url_plug()}}/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>
	<script>
		/*
		Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
		Version: 4.6.0
		Author: Sean Ngu
		Website: http://www.seantheme.com/color-admin/admin/
        
		*/
        $.getJSON("{{ url('stunting/'.$data->id.'/getdata')}}?bulan={{$bulan}}").done( function (results){
			var labels=[];
			var data=[];
			var labels=results.map(function (item){
				return item.nama;
			});
			var normal=results.map(function (item){
				return item.normal;
			});
			var pendek=results.map(function (item){
				return item.pendek;
			});
			var sangat_pendek=results.map(function (item){
				return item.sangat_pendek;
			});
			var tinggi=results.map(function (item){
				return item.tinggi;
			});
			createChart(labels,normal,pendek,sangat_pendek,tinggi);
			
		});
        function createChart(labels,normal,pendek,sangat_pendek,tinggi){
			

			var barChartData = {
				labels: labels,
				datasets: [{
					label: 'PENDEK',
					borderWidth: 2,
					borderColor: "red",
					backgroundColor: "red",
					data: pendek
				},{
					label: 'SENGAT PENDEK',
					borderWidth: 2,
					borderColor: "orange",
					backgroundColor: "orange",
					data: sangat_pendek
				},{
					label: 'TINGGI',
					borderWidth: 2,
					borderColor: "yellow",
					backgroundColor: "yellow",
					data: tinggi
				},{
					label: 'NORMAL',
					borderWidth: 2,
					borderColor: "blue",
					backgroundColor: "blue",
					data: normal
				}]
			};
			
			var ctx2 = document.getElementById('bar-chart').getContext('2d');
			var barChart = new Chart(ctx2, {
				type: 'bar',
				data: barChartData
			});

		}
        function get_dta(){
            $.ajax({ 
                type: 'GET', 
                url: "{{ url('stunting/'.$data->id.'/getdata')}}?bulan={{$bulan}}", 
                data: { ide: 1 }, 
                dataType: 'json',
                beforeSend: function() {
                    $("#tampil-data").html("");
                },
                success: function (data) {
                    $.each(data, function(i, result){
                        if(result.total>0){
                            var color="red";
                        }else{
                            var color="blueblue";
                        }
                        var tampil='<tr>'
										+'<td class="ttdd" style="text-align:left">'+result.nama+'</td>'
										+'<td class="ttdd">'+result.normal+'%</td>'
										+'<td class="ttdd">'+result.tinggi+'%</td>'
										+'<td class="ttdd">'+result.pendek+'%</td>'
										+'<td class="ttdd">'+result.sangat_pendek+'%</td>'
									+'</tr>';
                        $("#tampil-data").append(tampil);
                    });
                  
                }
            });
           
        }
		setTimeout(()=> {
         
        }, 1000);

        function cari_act(act){
            location.assign("{{url('stunting/'.$data->id)}}?bulan={{$bulan}}&act="+act+"&tahun={{$tahun}}")
        }
        function pilih_triwulan(act){
            location.assign("{{url('stunting/'.$data->id)}}?tahun={{$tahun}}&bulan="+act)
        }
		$(document).ready(function() {
            get_dta();
			
		});

        setTimeout(()=> {
            @if($act==2)
            for(var i=2; i<=25;i++)
            {
                load_isi(i);
            }
            @else
            for(var i=2; i<=17;i++)
            {
                load_isi(i);
            }
            @endif
        }, 1000);
        function load_isi(num){
        
            
            var total=0;
            $('table tbody tr').each(function(){
                var value= parseInt($('td',this).eq(num).text());
                if(value>0){
                    total +=value;
                }else{
                    total +=0;
                }
                
            });
            $('.nilai_'+num).text(+total);
            
        
        }
        $(document).ready(function() {
            
            @if($act==0)
                var table=$('#data-table-fixed-header').DataTable({
                        lengthMenu: [20, 40, 60],
                        lengthChange:false,
                        paging:false,
                        ordering:false,
                        fixedHeader: {
                            header: true,
                            headerOffset: $('#header').height()
                        },
                        responsive: false,
                        ajax:"{{ url('stunting/'.$data->id.'/getdataall')}}?bulan={{$bulan}}&tahun={{$tahun}}",
                        dom: 'lrtip',
                        columns: [
                            { data: 'id', render: function (data, type, row, meta) 
                                {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                } 
                            },
                            
                            { data: 'kelurahan' },
                            { data: 'SKL23' },
                            { data: 'SKP23' },
                            { data: 'SKL59' },
                            { data: 'SKP59' },
                            { data: 'KRL23' },
                            { data: 'KRP23' },
                            { data: 'KRL59' },
                            { data: 'KRP59' },
                            { data: 'BBNL23' },
                            { data: 'BBNP23' },
                            { data: 'BBNL59' },
                            { data: 'BBNP59' },
                            { data: 'RSL23' },
                            { data: 'RSP23' },
                            { data: 'RSL59' },
                            { data: 'RSP59' },
                            
                        ],
                        
                        language: {
                            paginate: {
                                // remove previous & next text from pagination
                                previous: '<< previous',
                                next: 'Next>>'
                            }
                        }
                    });
                    
            @endif
            @if($act==1)
                var table=$('#data-table-fixed-header').DataTable({
                        lengthMenu: [20, 40, 60],
                        lengthChange:false,
                        paging:false,
                        ordering:false,
                        fixedHeader: {
                            header: true,
                            headerOffset: $('#header').height()
                        },
                        responsive: false,
                        ajax:"{{ url('stunting/'.$data->id.'/getdataall')}}?bulan={{$bulan}}&tahun={{$tahun}}",
                        dom: 'lrtip',
                        columns: [
                            { data: 'id', render: function (data, type, row, meta) 
                                {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                } 
                            },
                            
                            { data: 'kelurahan' },
                            { data: 'SPL23' },
                            { data: 'SPP23' },
                            { data: 'SPL59' },
                            { data: 'SPP59' },
                            { data: 'PDL23' },
                            { data: 'PDP23' },
                            { data: 'PDL59' },
                            { data: 'PDP59' },
                            { data: 'NRL23' },
                            { data: 'NRP23' },
                            { data: 'NRL59' },
                            { data: 'NRP59' },
                            { data: 'TGL23' },
                            { data: 'TGP23' },
                            { data: 'TGL59' },
                            { data: 'TGP59' },
                            
                        ],
                        
                        language: {
                            paginate: {
                                // remove previous & next text from pagination
                                previous: '<< previous',
                                next: 'Next>>'
                            }
                        }
                    });
                    
            @endif
            @if($act==2)
                var table=$('#data-table-fixed-header').DataTable({
                        lengthMenu: [20, 40, 60],
                        lengthChange:false,
                        paging:false,
                        ordering:false,
                        fixedHeader: {
                            header: true,
                            headerOffset: $('#header').height()
                        },
                        responsive: false,
                        ajax:"{{ url('stunting/'.$data->id.'/getdataall')}}?bulan={{$bulan}}&tahun={{$tahun}}",
                        dom: 'lrtip',
                        columns: [
                            { data: 'id', render: function (data, type, row, meta) 
                                {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                } 
                            },
                            
                            { data: 'kelurahan' },
                            { data: 'GBL23' },
                            { data: 'GBP23' },
                            { data: 'GBL59' },
                            { data: 'GBP59' },
                            { data: 'GKL23' },
                            { data: 'GKP23' },
                            { data: 'GKL59' },
                            { data: 'GKP59' },
                            { data: 'GNL23' },
                            { data: 'GNP23' },
                            { data: 'GNL59' },
                            { data: 'GNP59' },
                            { data: 'RGLL23' },
                            { data: 'RGLP23' },
                            { data: 'RGLL59' },
                            { data: 'RGLP59' },
                            { data: 'GLL23' },
                            { data: 'GLP23' },
                            { data: 'GLL59' },
                            { data: 'GLP59' },
                            { data: 'OBL23' },
                            { data: 'OBP23' },
                            { data: 'OBL59' },
                            { data: 'OBP59' },
                            
                        ],
                        
                        language: {
                            paginate: {
                                // remove previous & next text from pagination
                                previous: '<< previous',
                                next: 'Next>>'
                            }
                        }
                    });
                    
            @endif

		});
	</script>

@endpush
