@extends('layouts.app_web')

@section('content')
<div id="page-title" class="page-title has-bg" style="">
	<div class="bg-cvr" style="background: url({{url_plug()}}/assets/img/cover/cover-dinas.jpg?v={{date('ymdhis')}}) center 0px / cover no-repeat"></div>
    <div class="container" style="color:#000">
        <div class="row">
            
            <!-- <div class="col-md-3 layout-mobile">
                <img src="{{url_plug()}}/assets/img/cover/logo.png?v={{date('ymdhis')}}" width="50%">
            </div> -->
            <div class="col-md-9" >
                
                <h2 style="color:blue">DINAS KESEHATAN KOTA CILEGON</h2>
                <h2 style="color:#000">Cilegon Bergerak Atasi Stunting</h2>
                <h5 style="color:#000">Dapatkan informasi lengkap terkait penanganan stunting di Kota Cilegon, mulai dari data, layanan konsultasi hingga penyaluran bantuan.</h5>
            </div>
            <div class="col-md-3 layout-web">
                <img src="{{url_plug()}}/assets/img/cover/logo.png?v={{date('ymdhis')}}" width="50%">
            </div>
        </div>
    </div>
</div>
    <div id="content" class="content" style="padding-top: 1px;">
		<!-- begin container -->
		<div class="container">
			<!-- begin row -->
			
			<div class="row row-space-30" style="margin-top:5%;">
				
                <div class="col-lg-12" style="min-height: 300px; padding-bottom: 3%; padding-top: 1%;">
                    <h3 class="page-header" style="text-align:center;margin-bottom: 3%;">Data Stunting {{$tahun}} <br><small>Daerah Pengawasan Tingkat Kecamatan - Kota Cilegon</small></h3>
                    <div class="row row-space-30" style="margin-top:1%;margin-top: 1%; ">
                
                        <div class="col-lg-12" style=" padding-top: 1%;text-align:center;border-bottom: solid 3px #dfdfe9;padding:0px">
                            <div class="btn-group">
                                <button onclick="pilih_triwulan(1)" class="btn btn-white " @if($bulan==1) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 1</button>
                                <button onclick="pilih_triwulan(2)" class="btn btn-white " @if($bulan==2) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 2</button>
                                <button onclick="pilih_triwulan(3)" class="btn btn-white " @if($bulan==3) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 3</button>
                                <button onclick="pilih_triwulan(4)" class="btn btn-white " @if($bulan==4) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 4</button>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row row-space-1" id="tampil-data">
				    </div>
				</div>
				
				
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
    
@endsection

@section('modal')

        <div id="modal-form" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;">                                               
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form ( Create / Update )</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydataform" method="post" action="{{ url('employe') }}" enctype="multipart/form-data" >
                            @csrf
                            <div id="tampil-form"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                        <a href="javascript:;" class="btn btn-success" id="save-data">Save</a>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
@endsection
@push('ajax')
	<script src="{{url_plug()}}/assets/plugins/chart.js/dist/Chart.min.js"></script>
	<script>
		/*
		Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
		Version: 4.6.0
		Author: Sean Ngu
		Website: http://www.seantheme.com/color-admin/admin/
        
		*/
    
        function pilih_triwulan(act){
            location.assign("{{url('/')}}?tahun={{$tahun}}&bulan="+act)
        }
        function get_dta(){
            $.ajax({ 
                type: 'GET', 
                url: "{{ url('getkecamatan')}}?bulan={{$bulan}}&tahun={{$tahun}}", 
                data: { ide: 1 }, 
                dataType: 'json',
                beforeSend: function() {
                    $("#tampil-data").html("");
                },
                success: function (data) {
                    $.each(data, function(i, result){
                        if(result.total>0){
                            var color="red";
                        }else{
                            var color="blueblue";
                        }
                        var tampil='<div class="col-md-3" onclick="detail(`'+result.nama+'`)" >'
                                            +'<div class="card border-0 text-truncate mb-3 bg-'+color+' text-white">'
                                                +'<div class="card-body">'
                                                    +'<div class="mb-3 text-white">'
                                                        +'<b class="mb-3" style="text-transform:uppercase">KEC. '+result.nama+'</b>' 
                                                        +'<span class="ml-2"><i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-title="Conversion Rate" data-placement="top" data-content="Percentage of sessions that resulted in orders from total number of sessions." data-original-title="" title=""></i></span>'
                                                    +'</div>'
                                                    +'<div class="d-flex align-items-center mb-1">'
                                                        +'<h2 class="text-white mb-0"><span data-animation="number" data-value="'+result.total+'">'+result.total+'</span>%</h2>'
                                                        +'<div class="ml-auto">'
                                                            +'<div id="conversion-rate-sparkline"></div>'
                                                        +'</div>'
                                                    +'</div>'
                                                    +'<div class="d-flex mb-2" style="background: #dfdfc769; padding: 2%;margin-top:1%;margin-bottom: 4px !important;">'
                                                        +'<div class="d-flex align-items-center">'
                                                            +'<i class="fa fa-circle text-yellow f-s-8 mr-2"></i>'
                                                            +'Normal'
                                                        +'</div>'
                                                        +'<div class="d-flex align-items-center ml-auto">'
                                                            +'<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> </div>'
                                                            +'<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="'+result.normal+'">'+result.normal+'</span>%</div>'
                                                        +'</div>'
                                                    +'</div>'
                                                    +'<div class="d-flex mb-2" style="background: #dfdfc769; padding: 2%;margin-bottom: 4px !important;">'
                                                        +'<div class="d-flex align-items-center">'
                                                            +'<i class="fa fa-circle text-yellow f-s-8 mr-2"></i>'
                                                            +'Tinggi'
                                                        +'</div>'
                                                        +'<div class="d-flex align-items-center ml-auto">'
                                                            +'<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> </div>'
                                                            +'<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="'+result.tinggi+'">'+result.tinggi+'</span>%</div>'
                                                        +'</div>'
                                                    +'</div>'
                                                   +'<div class="d-flex mb-2" style="background: #dfdfc769; padding: 2%;margin-top:1%;margin-bottom: 4px !important;">'
                                                        +'<div class="d-flex align-items-center">'
                                                            +'<i class="fa fa-circle text-yellow f-s-8 mr-2"></i>'
                                                            +'Pendek'
                                                        +'</div>'
                                                        +'<div class="d-flex align-items-center ml-auto">'
                                                            +'<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> </div>'
                                                            +'<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="'+result.pendek+'">'+result.pendek+'</span>%</div>'
                                                        +'</div>'
                                                    +'</div>'
                                                   +'<div class="d-flex mb-2" style="background: #dfdfc769; padding: 2%;margin-top:1%;margin-bottom: 4px !important;">'
                                                        +'<div class="d-flex align-items-center">'
                                                            +'<i class="fa fa-circle text-yellow f-s-8 mr-2"></i>'
                                                            +'Sangat Pendek'
                                                        +'</div>'
                                                        +'<div class="d-flex align-items-center ml-auto">'
                                                            +'<div class="text-grey f-s-11"><i class="fa fa-caret-up"></i> </div>'
                                                            +'<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="'+result.sangat_pendek+'">'+result.sangat_pendek+'</span>%</div>'
                                                        +'</div>'
                                                    +'</div>'
                                                    
                                                +'</div>'
                                            +'</div>'
                                        +'</div>'
                                    +'<div>';
                        $("#tampil-data").append(tampil);
                    });
                  
                }
            });
           
        }
		setTimeout(()=> {
         
        }, 1000);
		$(document).ready(function() {
            get_dta();
			
		});

        function detail(nama){
            location.assign("{{url('detail-kecamatan')}}/"+nama)
        }
	</script>

@endpush
