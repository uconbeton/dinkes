@extends('layouts.app')
@push('datatable')
<style>
    .uuu{

}
    
        .table thead tr th {
            font-weight: 600;
            font-size: 11px;
            text-transform: uppercase;
            border-bottom: 1px solid #b8c1ca;
            text-align: center;
            vertical-align: middle;
        }
        td{
            font-size: 11px;
            text-transform: uppercase;
            border-bottom: 1px solid #b8c1ca;
            
        }
        .nav.nav-tabs .nav-item .nav-link {
            padding: 2px 15px;
            font-weight: 600;
            color: #6f8293;
            border: solid 1px #b7aeae;
            background: aqua;
            border-radius: 0px;
        }
        .nav.nav-tabs .nav-item .nav-link.active, .nav.nav-tabs .nav-item .nav-link:focus, .nav.nav-tabs .nav-item .nav-link:hover {
    color: #2d353c;
    background: white;
}
</style>
    <script>
        function load_data(){
			$.ajax({ 
                type: 'GET', 
                url: "{{ url('caleg/getdatatipe')}}", 
                data: { ide: 1 }, 
                dataType: 'json',
                beforeSend: function() {
                    
                },
                success: function (data) {
                    $.each(data, function(i, result){
                        if(result.id==1){
                            $('#nilai_provinsi').html(result.total);
                        }
                        if(result.id==2){
                            $('#nilai_kota').html(result.total);
                        }
                        if(result.id==3){
                            $('#nilai_kecamatan').html(result.total);
                        }
                    });
                  
                }
            });
        }
        
        
       
       
        
        
        @foreach(get_indikator(2) as $no=>$o)
                setTimeout(()=> {
                    @if($o->id==3 || $o->id==4)
                        @if($o->id==3)
                            for(var i=2; i<=10;i++)
                            {
                                load_isi({{$o->id}},i);
                            }
                        @else
                            for(var i=2; i<=6;i++)
                            {
                                load_isi({{$o->id}},i);
                            }
                        @endif
                        
                    @else
                        for(var i=3; i<=27;i++)
                        {
                            load_isi({{$o->id}},i);
                        }
                    @endif
                }, 66000);
                
        @endforeach
        function load_isi(id,num){
                    
                        
                    var total=0;
                    $('#data-table-fixed-header-'+id+' tbody tr').each(function(){
                        var value= parseInt($('td',this).eq(num).text());
                        if(value>0){
                            total +=value;
                        }else{
                            total +=0;
                        }
                        
                    });
                    $('.nilai'+id+'_'+num).text(+total);
                    
                
                }
        $(document).ready(function() {
            @foreach(get_indikator(2) as $no=>$o)
                
                   
                @if($o->id==3 || $o->id==4)

                     @if($o->id==3)
                        var table=$('#data-table-fixed-header-{{$o->id}}').DataTable({
                            lengthMenu: [20, 40, 60],
                            lengthChange:false,
                            paging:false,
                            fixedHeader: {
                                header: false,
                                headerOffset: $('#header').height()
                            },
                            responsive: false,
                            ordering:false,
                            ajax:"{{ url('indikator-gizi/getdata')}}?act={{$act}}&indikator_id={{$o->id}}",
                            dom: 'lrtip',
                            columns: [
                                { data: 'id', render: function (data, type, row, meta) 
                                    {
                                        return meta.row + meta.settings._iDisplayStart + 1;
                                    } 
                                },
                                
                                { data: 'nama_kecamatan' },
                                { data: 'sas_mar' },
                                { data: 'num_cap_mar' },
                                { data: 'sas_jun' },
                                { data: 'num_cap_jun' },
                                { data: 'sas_sep' },
                                { data: 'num_cap_sep' },
                                { data: 'sas_des' },
                                { data: 'num_cap_des' },
                                
                            ],
                            
                            language: {
                                paginate: {
                                    // remove previous & next text from pagination
                                    previous: '<< previous',
                                    next: 'Next>>'
                                }
                            }
                        });
                    @else
                    var table=$('#data-table-fixed-header-{{$o->id}}').DataTable({
                            lengthMenu: [20, 40, 60],
                            lengthChange:false,
                            paging:false,
                            fixedHeader: {
                                header: false,
                                headerOffset: $('#header').height()
                            },
                            responsive: false,
                            ordering:false,
                            ajax:"{{ url('indikator-gizi/getdata')}}?act={{$act}}&indikator_id={{$o->id}}",
                            dom: 'lrtip',
                            columns: [
                                { data: 'id', render: function (data, type, row, meta) 
                                    {
                                        return meta.row + meta.settings._iDisplayStart + 1;
                                    } 
                                },
                                
                                { data: 'nama_kecamatan' },
                                { data: 'sas_feb' },
                                { data: 'num_cap_feb' },
                                { data: 'sas_agu' },
                                { data: 'num_cap_agu' },
                                
                            ],
                            
                            language: {
                                paginate: {
                                    // remove previous & next text from pagination
                                    previous: '<< previous',
                                    next: 'Next>>'
                                }
                            }
                        });
                    @endif
                @else
                    var table=$('#data-table-fixed-header-{{$o->id}}').DataTable({
                            lengthMenu: [20, 40, 60],
                            lengthChange:false,
                            paging:false,
                            fixedHeader: {
                                header: false,
                                headerOffset: $('#header').height()
                            },
                            responsive: false,
                            ordering:false,
                            ajax:"{{ url('indikator-gizi/getdata')}}?act={{$act}}&indikator_id={{$o->id}}",
                            dom: 'lrtip',
                            columns: [
                                { data: 'id', render: function (data, type, row, meta) 
                                    {
                                        return meta.row + meta.settings._iDisplayStart + 1;
                                    } 
                                },
                                
                                { data: 'kecamatannya' },
                                { data: 'kelurahan' },
                                { data: 'sas_jan' },
                                { data: 'num_cap_jan' },
                                { data: 'sas_feb' },
                                { data: 'num_cap_feb' },
                                { data: 'sas_mar' },
                                { data: 'num_cap_mar' },
                                { data: 'sas_apr' },
                                { data: 'num_cap_apr' },
                                { data: 'sas_mei' },
                                { data: 'num_cap_mei' },
                                { data: 'sas_jun' },
                                { data: 'num_cap_jun' },
                                { data: 'sas_jul' },
                                { data: 'num_cap_jul' },
                                { data: 'sas_agu' },
                                { data: 'num_cap_agu' },
                                { data: 'sas_sep' },
                                { data: 'num_cap_sep' },
                                { data: 'sas_okt' },
                                { data: 'num_cap_okt' },
                                { data: 'sas_nov' },
                                { data: 'num_cap_nov' },
                                { data: 'sas_des' },
                                { data: 'num_cap_des' },
                                
                            ],
                            
                            language: {
                                paginate: {
                                    // remove previous & next text from pagination
                                    previous: '<< previous',
                                    next: 'Next>>'
                                }
                            }
                        });
                @endif 
                $('a[data-bs-toggle="tab"]').on("shown.bs.tab", function (e) {
                    $.fn.dataTable.tables({ visible: true})
                    .columns.adjust()
                    .responsive.recalc();
                });
           @endforeach

		});

        
    </script>
@endpush
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb float-xl-right">
            <li class="breadcrumb-item"><a href="javascript:;">Home </a></li>
            <li class="breadcrumb-item active">Daftar Indikator Intervensi Spesifik</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Daftar Indikator Intervensi Spesifik <small> </small></h1>
        <!-- end page-header -->
        <!-- begin row -->
        <div class="row">
            <!-- begin col-2 -->
            
            <!-- end col-2 -->
            <!-- begin col-10 -->
            <div class="col-xl-12">
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title">&nbsp;</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin alert -->
                    <div class="row" style="padding:2%">
                        <div class="col-md-2">
                            <span class="btn btn-sm btn-primary" onclick="add_data(0)"><i class="fas fa-cloud-upload-alt fa-fw"></i> Import data</span>
                        </div>
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-2">
                            <select id="tahun" class="form-control">
                                <option>.:: Tahun</option>
                                @for($x=date('Y');$x>=2020;$x--)
                                <option value="{{$x}}" @if($tahun==$x) selected @endif>{{$x}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select id="bulan" class="form-control">
                                <option>.:: Periode Input</option>
                                @foreach(get_header_id(3) as $o)
                                <option value="{{$o->id}}" @if($act==$o->id) selected @endif >{{$o->tanggal}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-sm btn-success" onclick="cari()"><i class="fas fa-search fa-fw"></i> Cari</span>
                        </div>
                    </div>
                    <!-- end alert -->
                    <!-- begin panel-body -->
                    <div class="panel-body" style="background: #eeeef5;" >
                        <div class="row">
                            
                            <div class="col-md-12">
                                <ul class="nav nav-tabs">
                                    @foreach(get_indikator(2) as $no=>$o)
                                    <li class="nav-item">
                                        <a href="#default-tab-{{$o->id}}"   data-toggle="tab"  class="nav-link @if($no==0) active  @endif ">
                                            <span class="d-sm-none"><i class="fas fa-chart-pie"></i> {{$o->id}}. {{$o->indikator}}</span>
                                            <span class="d-sm-block d-none"><i class="fas fa-chart-pie"></i> {{$o->id}}. {{$o->indikator}}</span>
                                        </a>
                                    </li>
                                   @endforeach
                                </ul>
                                <div class="tab-content">
                                     @foreach(get_indikator(2) as $no=>$o)
                                        <div class="tab-pane fade @if($no==0) active show @endif" id="default-tab-{{$o->id}}">
                                                <h6>{{$o->keterangan}}</h6>
                                                <div class="table-responsive">
                                                    @if($o->id==3 || $o->id==4)
                                                         @if($o->id==3)
                                                         <table id="data-table-fixed-header-{{$o->id}}" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-nowrap" rowspan="2" width="3%">No</th>
                                                                    <th class="text-nowrap" rowspan="2" >Kecamatan</th>
                                                                    <th class="text-nowrap" colspan="2">MARET</th>
                                                                    <th class="text-nowrap" colspan="2">JUNI</th>
                                                                    <th class="text-nowrap" colspan="2">SEPTEMBER</th>
                                                                    <th class="text-nowrap" colspan="2">DESEMBER</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-nowrap" width="3%" style="background:aqua">Sasaran</th>
                                                                    <th class="text-nowrap" width="3%" style="background:#90f50f">Capaian</th>
                                                                    <th class="text-nowrap" width="3%" style="background:aqua">Sasaran</th>
                                                                    <th class="text-nowrap" width="3%" style="background:#90f50f">Capaian</th>
                                                                    <th class="text-nowrap" width="3%" style="background:aqua">Sasaran</th>
                                                                    <th class="text-nowrap" width="3%" style="background:#90f50f">Capaian</th>
                                                                    <th class="text-nowrap" width="3%" style="background:aqua">Sasaran</th>
                                                                    <th class="text-nowrap" width="3%" style="background:#90f50f">Capaian</th>
                                                                    
                                                                </tr>
                                                                
                                                            </thead>
                                                                <tfoot>
                                                                <tr>
                                                                    <th></th>
                                                                    <th> TOTAL</th>
                                                                    <th class="nilai{{$o->id}}_2"></th>
                                                                    <th class="nilai{{$o->id}}_3"></th>
                                                                    <th class="nilai{{$o->id}}_4"></th>
                                                                    <th class="nilai{{$o->id}}_5"></th>
                                                                    <th class="nilai{{$o->id}}_6"></th>
                                                                    <th class="nilai{{$o->id}}_7"></th>
                                                                    <th class="nilai{{$o->id}}_8"></th>
                                                                    <th class="nilai{{$o->id}}_9"></th>
                                                                    
                                                                </tr>
                                                            </tfoot>
                                                        </table>

                                                         @else
                                                         <table id="data-table-fixed-header-{{$o->id}}" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-nowrap" rowspan="2" width="3%">No</th>
                                                                    <th class="text-nowrap" rowspan="2" >Kecamatan</th>
                                                                    <th class="text-nowrap" colspan="2">FEBRUARI</th>
                                                                    <th class="text-nowrap" colspan="2">AGUSTUS</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-nowrap" width="5%" style="background:aqua">Sasaran</th>
                                                                    <th class="text-nowrap" width="5%" style="background:#90f50f">Capaian</th>
                                                                    <th class="text-nowrap" width="5%" style="background:aqua">Sasaran</th>
                                                                    <th class="text-nowrap" width="5%" style="background:#90f50f">Capaian</th>
                                                                    
                                                                </tr>
                                                                
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th></th>
                                                                    <th> TOTAL</th>
                                                                    <th class="nilai{{$o->id}}_2"></th>
                                                                    <th class="nilai{{$o->id}}_3"></th>
                                                                    <th class="nilai{{$o->id}}_4"></th>
                                                                    <th class="nilai{{$o->id}}_5"></th>
                                                                    
                                                                </tr>
                                                            </tfoot>
                                                        </table>

                                                         @endif


                                                    @else
                                                        <table id="data-table-fixed-header-{{$o->id}}" class="table table-striped table-bordered table-td-valign-middle" width="140%">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-nowrap" rowspan="2" width="3%">No</th>
                                                                    <th class="text-nowrap" rowspan="2" width="6%">Kecamatan</th>
                                                                    <th class="text-nowrap" rowspan="2">Kelurahan</th>
                                                                    <th class="text-nowrap" colspan="2">JANUARI</th>
                                                                    <th class="text-nowrap" colspan="2">FEBRUARI</th>
                                                                    <th class="text-nowrap" colspan="2">MARET</th>
                                                                    <th class="text-nowrap" colspan="2">APRIL</th>
                                                                    <th class="text-nowrap" colspan="2">MEI</th>
                                                                    <th class="text-nowrap" colspan="2">JUNI</th>
                                                                    <th class="text-nowrap" colspan="2">JULI</th>
                                                                    <th class="text-nowrap" colspan="2">AGUSTUS</th>
                                                                    <th class="text-nowrap" colspan="2">SEPTEMBER</th>
                                                                    <th class="text-nowrap" colspan="2">OKTOBER</th>
                                                                    <th class="text-nowrap" colspan="2">NOVEMBER</th>
                                                                    <th class="text-nowrap" colspan="2">DESEMBER</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:#90f50f">Cap</th>
                                                                    <th class="text-nowrap" width="2%" style="background:aqua">Sas</th>
                                                                    <th class="text-nowrap" width="2%" style="background:blue">Cap</th>
                                                                </tr>
                                                                
                                                            </thead>
                                                            <tfoot>
                                                                <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th> TOTAL</th>
                                                                <th class="nilai{{$o->id}}_3"></th>
                                                                    <th class="nilai{{$o->id}}_4"></th>
                                                                    <th class="nilai{{$o->id}}_5"></th>
                                                                    <th class="nilai{{$o->id}}_6"></th>
                                                                    <th class="nilai{{$o->id}}_7"></th>
                                                                    <th class="nilai{{$o->id}}_8"></th>
                                                                    <th class="nilai{{$o->id}}_9"></th>
                                                                    <th class="nilai{{$o->id}}_10"></th>
                                                                    <th class="nilai{{$o->id}}_11"></th>
                                                                    <th class="nilai{{$o->id}}_12"></th>
                                                                    <th class="nilai{{$o->id}}_13"></th>
                                                                    <th class="nilai{{$o->id}}_14"></th>
                                                                    <th class="nilai{{$o->id}}_15"></th>
                                                                    <th class="nilai{{$o->id}}_16"></th>
                                                                    <th class="nilai{{$o->id}}_17"></th>
                                                                    <th class="nilai{{$o->id}}_18"></th>
                                                                    <th class="nilai{{$o->id}}_19"></th>
                                                                    <th class="nilai{{$o->id}}_20"></th>
                                                                    <th class="nilai{{$o->id}}_21"></th>
                                                                    <th class="nilai{{$o->id}}_22"></th>
                                                                    <th class="nilai{{$o->id}}_23"></th>
                                                                    <th class="nilai{{$o->id}}_24"></th>
                                                                    <th class="nilai{{$o->id}}_25"></th>
                                                                    <th class="nilai{{$o->id}}_26"></th>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    @endif
                                                </div>
                                            
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- end panel-body -->
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-10 -->
        </div>
        <!-- end row -->
    </div>
@endsection

@section('modal')

        <div id="modal-form" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;">                                               
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Import Data Indikator Intervensi Spesifik</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydataform" method="post" action="{{ url('indikator-gizi/import') }}" enctype="multipart/form-data" >
                            @csrf
                            <!-- <input type="submit"> -->
                            <div class="note note-yellow m-b-15">
                                <div class="note-icon f-s-20">
                                    <i class="fa fa-lightbulb fa-2x"></i>
                                </div>
                                <div class="note-content">
                                    <h4 class="m-t-5 m-b-5 p-b-2">Notes</h4>
                                    <ul class="m-b-5 p-l-25">
                                        <li>Import Data Intervensi Spesifik </li>
                                        <li>File Hanya berformat (xls,xlsx)</li>
                                        <li>Tunggu hingga proses import selesai</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="form-group row m-b-5">
                                <label class="col-form-label col-md-3">Kota</label>
                                <div class="col-md-9">
                                    <input type="hidden"  class="form-control m-b-5" placeholder="" name="kecamatan_id" value="{{$data->id}}" />
                                    <input type="text" disabled class="form-control m-b-5" placeholder="" value="Cilegon" />
                                </div>
                            </div>
                            <div class="form-group row m-b-5">
                                <label class="col-form-label col-md-3">Periode</label>
                                <div class="col-md-3">
                                    <select name="tahun" class="form-control">
                                        <option>.:: Tahun</option>
                                        @for($x=date('Y');$x>=2020;$x--)
                                        <option value="{{$x}}">{{$x}}</option>
                                       @endfor
                                    </select>
                                </div>
                                
                            </div>
                            <div class="form-group row m-b-5">
                                <label class="col-form-label col-md-3">File (xls,xlsx)</label>
                                <div class="col-md-9">
                                    <input type="file"  class="form-control m-b-5" placeholder="" name="file" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Batal</a>
                        <a href="javascript:;" class="btn btn-success" id="save-data">Proses Import</a>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
@endsection


@push('ajax')
    <script>
        function cari(){
            var bulan=$('#bulan').val();
            var tahun=$('#tahun').val();
            location.assign("{{url('indikator-gizi')}}?act="+bulan+"&tahun="+tahun)
        }
        function cari_act(act){
            var bulan=$('#bulan').val();
            var tahun=$('#tahun').val();
            location.assign("{{url('indikator-gizi')}}?act="+act+"&bulan="+bulan+"&tahun="+tahun)
        }
        
        function add_data(id){
            $('#modal-form').modal('show');
            $('#tampil-form').load("{{url('caleg/modal')}}?id="+id);
        }


        function delete_data(id){
            
            swal({
                title: "Yakin menghapus data ini ?",
                text: "data akan hilang dari data  ini",
                type: "warning",
                icon: "error",
                showCancelButton: true,
                align:"center",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then((willDelete) => {
                if (willDelete) {
                     
                        $.ajax({
                            type: 'GET',
                            url: "{{url('caleg/delete')}}",
                            data: "id="+id,
                            success: function(msg){
                                swal("Success! berhasil terhapus!", {
                                    icon: "success",
                                });
                                load_data();
                                var tables=$('#data-table-fixed-header').DataTable();
                                    tables.ajax.url("{{ url('caleg/getdata')}}").load();
                            }
                        });
                    
                } else {
                    
                }
            });
            
        } 

        $('#save-data').on('click', () => {
            var bulan=$('#bulan').val();
            var tahun=$('#tahun').val();
            
            var form=document.getElementById('mydataform');
                $.ajax({
                    type: 'POST',
                    url: "{{ url('indikator-gizi/import') }}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function() {
                        document.getElementById("loadnya").style.width = "100%";
                    },
                    success: function(msg){
                        var bat=msg.split('@');
                        if(bat[1]=='ok'){
                            document.getElementById("loadnya").style.width = "0px";
                            swal("Success! diproses !", {
                                icon: "success",
                            });
                            $('#modal-form').modal('hide');
                            location.assign("{{url('indikator-gizi')}}?act={{$act}}&bulan="+bulan+"&tahun="+tahun)
                        }else{
                            document.getElementById("loadnya").style.width = "0px";
                            swal({
                                title: 'Notifikasi',
                               
                                html:true,
                                text:'ss',
                                icon: 'error',
                                buttons: {
                                    cancel: {
                                        text: 'Tutup',
                                        value: null,
                                        visible: true,
                                        className: 'btn btn-dangers',
                                        closeModal: true,
                                    },
                                    
                                }
                            });
                            $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:1%;text-align:left;font-size:13px">'+msg+'</div>')
                        }
                        
                        
                    }
                });
        });
    </script>
@endpush