@extends('layouts.app')

@section('content')
<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard <small>header small text goes here...</small></h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<!-- <div class="row">
				
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>TOTAL VISITORS</h4>
							<p>3,291,922</p>	
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				
				
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-info">
						<div class="stats-icon"><i class="fa fa-link"></i></div>
						<div class="stats-info">
							<h4>BOUNCE RATE</h4>
							<p>20.44%</p>	
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				
				
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-orange">
						<div class="stats-icon"><i class="fa fa-users"></i></div>
						<div class="stats-info">
							<h4>UNIQUE VISITORS</h4>
							<p>1,291,922</p>	
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				
				
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-red">
						<div class="stats-icon"><i class="fa fa-clock"></i></div>
						<div class="stats-info">
							<h4>AVG TIME ON SITE</h4>
							<p>00:12:23</p>	
						</div>
						<div class="stats-link">
							<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				
			</div> -->
			<!-- end row -->
			<!-- begin row -->
			<div class="row">
				<!-- begin col-8 -->
				<div class="col-xl-8">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-1">
						<div class="panel-heading">
							<h4 class="panel-title">GRAFIK PERSENTASE</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body pr-1">
							<h6 class="page-header" style="font-size:15px;padding:2%;text-align:center;margin-bottom: 3%;text-transform: capitalize;"><b>grafik</b>  persentase stunting </h6>
                    		<div class="row row-space-30" style="margin-top:1%;margin-top: 1%; ">
                
								<div class="col-lg-12" style=" padding-top: 1%;text-align:center;border-bottom: solid 3px #dfdfe9;padding:0px">
									<div class="btn-group">
										<button onclick="pilih_triwulan(1)" class="btn btn-white " @if($bulan==1) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 1</button>
										<button onclick="pilih_triwulan(2)" class="btn btn-white " @if($bulan==2) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 2</button>
										<button onclick="pilih_triwulan(3)" class="btn btn-white " @if($bulan==3) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 3</button>
										<button onclick="pilih_triwulan(4)" class="btn btn-white " @if($bulan==4) style="background:red;color:#fff" @else style="color:#fff;background:#859131;border:solid 1px #fff" @endif>Triwulan 4</button>
										
									</div>
								</div>
							</div>
							<canvas id="bar-chart" data-render="chart-js"></canvas>
						</div>
					</div>
					<!-- end panel -->
					
				</div>
				<!-- end col-8 -->
				<!-- begin col-4 -->
				<div class="col-xl-4">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-6">
						<div class="panel-heading">
							<h4 class="panel-title">TABEL PERSENTASE</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table">
								<tbody> 
									<tr>
										<td width="50%" style="padding: 1%;font-size:11px; background: blue; color: #fff;">NR (Normal)</td>
										<td  style="padding: 1%;font-size:11px; background: red; color: #fff;">PD (Pendek)</td>
									</tr>
									<tr>
										<td  style="padding: 1%;font-size:11px; background: yellow; color: #000;">TG (Tinggi)</td>
										<td  style="padding: 1%;font-size:11px; background: orange; color: #fff;">SP (Sangat Pendek)</td>
									</tr>
								</tbody>
							</table>
							<table class="table table-bordered table-panel mt-4">
								<thead>
									<tr>	
										<th  rowspan="2" class="ttdh">KECAMATAN</th>
										<th  colspan="4" class="ttdh">KATEGORI</th>
									</tr>
									<tr>	
										<th  class="ttdh">NR</th>
										<th  class="ttdh">TG</th>
										<th  class="ttdh">PD</th>
										<th  class="ttdh">SP</th>
									</tr>
								</thead>
								<tbody id="tampil-data">
									
									
								</tbody>
							</table>
						</div>
					</div>
					<!-- end panel -->
					
				</div>
				<!-- end col-4 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
@endsection
@push('ajax')
	<script src="{{url_plug()}}/assets/plugins/chart.js/dist/Chart.min.js"></script>
	<script>
		 $.getJSON("{{ url('getkecamatan')}}?bulan={{$bulan}}&tahun={{$tahun}}").done( function (results){
			var labels=[];
			var data=[];
			var labels=results.map(function (item){
				return item.nama;
			});
			var normal=results.map(function (item){
				return item.normal;
			});
			var pendek=results.map(function (item){
				return item.pendek;
			});
			var sangat_pendek=results.map(function (item){
				return item.sangat_pendek;
			});
			var tinggi=results.map(function (item){
				return item.tinggi;
			});
			createChart(labels,normal,pendek,sangat_pendek,tinggi);
			
		});
		function pilih_triwulan(act){
            location.assign("{{url('home')}}?tahun={{$tahun}}&bulan="+act)
        }
		function get_dta(){
            $.ajax({ 
                type: 'GET', 
                url: "{{ url('getkecamatan')}}?bulan={{$bulan}}&tahun={{$tahun}}", 
                data: { ide: 1 }, 
                dataType: 'json',
                beforeSend: function() {
                    $("#tampil-data").html("");
                },
                success: function (data) {
                    $.each(data, function(i, result){
                        if(result.total>0){
                            var color="red";
                        }else{
                            var color="blueblue";
                        }
                        var tampil='<tr>'
										+'<td class="ttdd" style="text-align:left;background:blue;color:#fff">'+result.nama+'</td>'
										+'<td class="ttdd">'+result.normal+'%</td>'
										+'<td class="ttdd">'+result.tinggi+'%</td>'
										+'<td class="ttdd">'+result.pendek+'%</td>'
										+'<td class="ttdd">'+result.sangat_pendek+'%</td>'
									+'</tr>';
                        $("#tampil-data").append(tampil);
                    });
                  
                }
            });
           
        }
        function createChart(labels,normal,pendek,sangat_pendek,tinggi){
			

			var barChartData = {
				labels: labels,
				datasets: [{
					label: 'PENDEK',
					borderWidth: 2,
					borderColor: "red",
					backgroundColor: "red",
					data: pendek
				},{
					label: 'SENGAT PENDEK',
					borderWidth: 2,
					borderColor: "orange",
					backgroundColor: "orange",
					data: sangat_pendek
				},{
					label: 'TINGGI',
					borderWidth: 2,
					borderColor: "yellow",
					backgroundColor: "yellow",
					data: tinggi
				},{
					label: 'NORMAL',
					borderWidth: 2,
					borderColor: "blue",
					backgroundColor: "blue",
					data: normal
				}]
			};
			
			var ctx2 = document.getElementById('bar-chart').getContext('2d');
			var barChart = new Chart(ctx2, {
				type: 'bar',
				data: barChartData
			});

		}
        

        function cari_act(act){
            location.assign("{{url('home')}}?bulan={{$bulan}}&act="+act+"&tahun={{$tahun}}")
        }
        function pilih_triwulan(act){
            location.assign("{{url('home')}}?tahun={{$tahun}}&bulan="+act)
        }
		$(document).ready(function() {
            get_dta();
			
		});

        
	</script>

@endpush