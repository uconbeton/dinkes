        <div id="sidebar" class="sidebar" style="background-image: linear-gradient(to right top, #6451af, #483ab7, #363197, #3c429f, #4354af, #4a68a7, #3469a9, #296f9b, #4d4ad9, #4c42bd, #80cdd8, #b2dcd9);">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;">
							<div class="cover"></div>
							<div class="image">
								
							<img src="{{url_plug()}}/assets/img/cover/logo.png" alt="" />
									
							</div>
							<div class="info" style="color:#000">
								<small style="color:blue">Administrator</small>
								<b class="caret pull-right"></b>{{Auth::user()->name}}
								
								
							</div>
						</a>
					</li>
					
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav"><li class="nav-header">Navigation</li>
					<li>
						<a href="{{url('/home')}}">
							<i class="fa fa-th-large"></i>
							<span>Dashboard</span> 
						</a>
					</li>
					
					<li class="has-sub expand">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fa fa-building"></i>
							<span>Gizi Anak Kecamatan</span>
						</a>
						<ul class="sub-menu" style="display: block;">
							@foreach(get_kecamatan() as $o)
							<li><a href="{{url('gzkecamatan/'.$o->id)}}">{{$o->nama}}</a></li>
							@endforeach
						</ul>
					</li>
					<li>
						<a href="{{url('/indikator-gizi')}}">
							<i class="fa fa-chart-pie"></i>
							<span>Intervensi Spesifik</span> 
						</a>
					</li>
					<li>
						<a href="{{url('/indikator-sensitif')}}">
							<i class="fa fa-chart-pie"></i>
							<span>Intervensi Sensitif</span> 
						</a>
					</li>
					<!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
					<!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>