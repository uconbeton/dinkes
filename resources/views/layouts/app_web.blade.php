
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Stunting| Kota Cilegon</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/css/blog/app.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/datatables.net-fixedheader-bs4/css/fixedheader.bootstrap4.min.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	<style>
		body{
			background: #f2f2fd;
			font-family: sans-serif;
		}
		.page-title.has-bg .bg-cvr {
			position: absolute;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			background-position: center;
			background-size: cover;
			background-repeat: no-repeat;
		}
		.col-md-3 table tr th {
			border-radius: 10px;
		}
		table.dataTable th {
			-webkit-box-sizing: content-box;
			box-sizing: content-box;
			background: #b1f5a1;
			font-size: 11px;
			padding: 0.4%;
			border: solid 1px #5a5a5a7a;
			text-align:center;
			vertical-align:middle;
		}
		table.dataTable td{
			-webkit-box-sizing: content-box;
			box-sizing: content-box;
			border: solid 1px #5a5a5a7a;
			font-size: 11px;
			padding: 0.4%;
			border: solid 1px #e3e3e9;
		}
		.app-theme-green .app-sidebar .nav>li.active>a, .bg-blueblue {
			background-color: #3251a9c4!important;
		}
		.header .nav.navbar-nav>li>a {
			font-family: sans-serif;
			font-weight: 50 !important;
		}
		@media (max-width: 991.98px){
			.loadnyapage-content img{
				width:30%;
			}
			.navbar-collapse {
				flex-basis: 100%;
				flex-grow: 1;
				background: #2f48cb;
				align-items: center;
			}
			.layout-web{
				display:none;
			}
			.page-title.has-bg{
				padding: 7rem 0 6.25rem;
			}
			.page-title+.content {
				padding-top: 1.875rem;
			}
	    }
		@media (min-width: 992.98px){
			.navbar-brand img{
				height: 90px !important;
			}
			.page-title.has-bg .bg-cvr {
				top: 10%;
			}
			.page-title.has-bg #bg-cvr {
				top: 0%;
			}
			.header.navbar-transparent {
				background: 0 0;
				background: #22226c;
			}
			.header .navbar-brand {
				line-height: 0px;
				padding: 0px;
				max-height: 3.1875rem;
			}
			.loadnyapage-content img{
				width:6%;
			}
			.layout-mobile{
				display:none;
			}
			.page-title.has-bg{
				padding: 7rem 0 6.25rem;
			}
			.page-title+.content {
				padding-top: 1.875rem;
			}
	    }
		.loadnya {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1070;
            top: 0;
            left: 0;
            background-color: rgb(0, 0, 0);
            background-color: rgb(34 31 31 / 81%);
            overflow-x: hidden;
            transition: transform .9s;
        }

        .loadnya-content {
            position: relative;
            top: 25%;
            width: 100%;
            text-align: center;
            margin-top: 30px;
            color: #fff;
            font-size: 20px;
        }
		.loadnyapage {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1070;
            top: 0;
            left: 0;
            background-color: rgb(243 228 228 / 81%);
            background-color: rgb(243 228 228 / 81%);
            overflow-x: hidden;
            transition: transform .9s;
        }

        .loadnyapage-content {
            position: relative;
            top: 25%;
            width: 100%;
            text-align: center;
            margin-top: 30px;
            color: #fff;
            font-size: 20px;
        }
	</style>
</head>
<body>
	<!-- begin #header -->
	<div id="loadnya" class="loadnya">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="loadnya-content">
            <button class="btn btn-light" type="button" disabled>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>
        </div>
    </div>
	<div id="loadnyapage" class="loadnyapage">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="loadnyapage-content">
		<img src="{{url_plug()}}/assets/img/cover/logo.png?v={{date('ymdhis')}}" ><br>
		<h5 style="color:#000">DINKES KOTA CILEGON</h5>
            <button class="btn btn-light" type="button" disabled>
			
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>
        </div>
    </div>
	<div id="header" class="header navbar navbar-transparent navbar-expand-lg navbar-fixed-top">
		<!-- begin container -->
		<div class="container">
			<!-- begin navbar-brand -->
			<a href="http://taring.cilegon.go.id" class="navbar-brand">
				<img src="{{url_plug()}}/assets/img/cover/logo.png?v={{date('ymdhis')}}">
				
			</a>
			<!-- end navbar-brand -->
			<!-- begin navbar-toggle -->
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- end navbar-toggle -->
			<!-- begin navbar-collapse -->
			<div class="collapse navbar-collapse" id="header-navbar">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="{{url('/')}}" >Beranda </a>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Stunting <b class="caret"></b></a>
						<div class="dropdown-menu">
							@foreach(get_kecamatan() as $o)
							<a class="dropdown-item" href="{{url('stunting/'.$o->id)}}">{{$o->nama}}</a>
							
							@endforeach
							
						</div>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Intervensi Spesifik<b class="caret"></b></a>
						<div class="dropdown-menu">
							@foreach(get_kecamatan() as $o)
							<a class="dropdown-item" href="{{url('intervensi/'.$o->id)}}">{{$o->nama}}</a>
							
							@endforeach
							
						</div>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Intervensi Sensitif<b class="caret"></b></a>
						<div class="dropdown-menu">
							@foreach(get_kecamatan() as $o)
							<a class="dropdown-item" href="{{url('intervensi-sensitif/'.$o->id)}}">{{$o->nama}}</a>
							
							@endforeach
							
						</div>
					</li>
					<li><a href="https://dinkes.cilegon.go.id/">Hubungi Kami</a></li>
				</ul>
			</div>
			<!-- end navbar-collapse -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #header -->
	
	<!-- begin #page-title -->
	
	<!-- end #page-title -->
	
	<!-- begin #content -->
	@yield('content')
	<!-- end #content -->
    
	<!-- begin #footer -->
	<div id="footer" class="footer" style="-webkit-box-shadow: none;background: url({{url_plug()}}/assets/img/cover/cover-dinas2.jpg?v={{date('ymdhis')}}) center 0px / cover no-repeat">
		<!-- begin container -->
		<div class="container">
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				
				<div class="col-md-4 col-6">
					<!-- begin section-container -->
					<img src="{{url_plug()}}/assets/img/cover/logo.png?v={{date('ymdhis')}}" width="40%">
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-2 layout-web">

				</div>
				<div class="col-md-4 col-6">
					<div class="section-container" >
						<h4 class="footer-title">DINAS KESEHATAN KOTA CILEGON</h4>
						<address style="color:#000 !important">
							<strong>Alamat.</strong> 
							Jl. Pangeran Jayakarta No.47, Masigit, Kec. Jombang, Kota Cilegon, Banten 42411<br />
							<br />
							<strong>Link Aktif</strong><br />
							<a href="http://taring.cilegon.go.id">taring.cilegon.go.id</a>
						</address>
					</div>
					<!-- end section-container -->
				</div>
				<!-- end col-3 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #footer -->
	
	<!-- begin #footer-copyright -->
	<div id="footer-copyright" class="footer-copyright" style="background: #f8fafd14;">
		<!-- begin container -->
		<div class="container">
			<span class="copyright">&copy; 2023 TARING </span>
			<ul class="social-media-list mt-2 mt-sm-0 float-none float-sm-right">
				<li><a href="https://www.facebook.com/dinas.kesehatan.kota.cilegon?mibextid=ZbWKwL"><i class="fab fa-facebook"></i></a></li>
				<li><a href="https://instagram.com/dinkes_cilegon?igshid=OGQ5ZDc2ODk2ZA=="><i class="fab fa-instagram"></i></a></li>
				<!-- <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
				
				<li><a href="#"><i class="fab fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-rss"></i></a></li> -->
			</ul>
		</div>
		<!-- end container -->
	</div>
	<!-- end #footer-copyright -->
	
	<!-- begin theme-panel -->
	<div class="theme-panel">
		<!-- <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a> -->
		<div class="theme-panel-content">
			<ul class="theme-list clearfix">
				<li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="{{url_plug()}}/assets/css/blog/theme/red.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-pink" data-theme="pink" data-theme-file="{{url_plug()}}/assets/css/blog/theme/pink.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Pink" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="{{url_plug()}}/assets/css/blog/theme/orange.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-yellow" data-theme="yellow" data-theme-file="{{url_plug()}}/assets/css/blog/theme/yellow.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Yellow" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-lime" data-theme="lime" data-theme-file="{{url_plug()}}/assets/css/blog/theme/lime.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Lime" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-green" data-theme="green" data-theme-file="{{url_plug()}}/assets/css/blog/theme/green.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Green" data-original-title="" title="">&nbsp;</a></li>
				<li class="active"><a href="javascript:;" class="bg-teal" data-theme-file="" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-aqua" data-theme="aqua" data-theme-file="{{url_plug()}}/assets/css/blog/theme/aqua.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Aqua" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="{{url_plug()}}/assets/css/blog/theme/blue.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="{{url_plug()}}/assets/css/blog/theme/purple.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-indigo" data-theme="indigo" data-theme-file="{{url_plug()}}/assets/css/blog/theme/indigo.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Indigo" data-original-title="" title="">&nbsp;</a></li>
				<li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="{{url_plug()}}/assets/css/blog/theme/black.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black" data-original-title="" title="">&nbsp;</a></li>
			</ul>
		</div>
	</div>
	<!-- end theme-panel -->
    
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{url_plug()}}/assets/js/blog/app.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-fixedheader/js/dataTables.fixedheader.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-fixedheader-bs4/js/fixedheader.bootstrap4.min.js"></script>
	<script>
		document.getElementById("loadnyapage").style.width = "100%";
		setTimeout(()=> {
			document.getElementById("loadnyapage").style.width = "0%";
        }, 2000);
	</script>
	@stack('ajax')
	<!-- ================== END BASE JS ================== -->
</body>
</html>