@extends('layouts.app')

@section('content')
<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard Lokasi Kecamatan - {{$nama}}</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			
			
			<div class="row">
				<!-- begin col-8 -->
				<div class="col-xl-12">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-1">
						<div class="panel-heading">
							<h4 class="panel-title">GRAFIK HASIL TARGET DAN REALISASI </h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body p-0 row" style="min-height:300px">
							<div class="col-md-8">
								<canvas id="bar-chart" data-render="chart-js"></canvas>
							</div>
							<div class="col-md-4">
								<div class="table-responsive">
									<table  class="table table-striped m-b-0 table-bordered">
										<thead>
											<tr>
												<th>KECAMATAN</th>
												<th width="30%">TARGET</th>
												<th width="30%">REALISASI</th>
											</tr>
											
										</thead>
										<tbody id="tabelnya">
										</tbody>
									</table>
								</div>
							</div>
							
						</div>
					</div>
					
					
					
                    
					
				</div>
				
			</div>
			
		</div>
		<audio id="myAudio">
			<source src="{{url_plug()}}/img/pingpong.mp3" type="audio/mp3">
		</audio>
		<!-- end #content -->
@endsection
@push('ajax')
	<script src="{{url_plug()}}/assets/plugins/chart.js/dist/Chart.min.js"></script>
	<script src="https://js.pusher.com/7.2/pusher.min.js"></script>
	<script>
		/*
		Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
		Version: 4.6.0
		Author: Sean Ngu
		Website: http://www.seantheme.com/color-admin/admin/
		*/

		Pusher.logToConsole = false;

        var pusher = new Pusher('017380cd2cb9f230df7a', {
            cluster: 'ap1',
            // forceTLS: true
        });

        var channel = pusher.subscribe('my-chanel');
        channel.bind('kirim-created', function(data) {
			get_dta();
			var adu = document.getElementById("myAudio");
            var pesan = data.message;
            var bat = pesan.split('@');
			adu.play();
			
            
        });
		function get_dta(){
			$.getJSON("{{ url('area/getkecamatan')}}?kota_id={{Auth::user()->kota_id}}").done( function (results){
				var labels=[];
				var data=[];
				var labels=results.map(function (item){
					return item.nama;
				});
				var data=results.map(function (item){
					return item.total_rencana;
				});
				var data2=results.map(function (item){
					return item.total_actual;
				});
				createChart(labels,data,data2);
				
			});
			load_table();
		}
		
		function load_table(){
			$.ajax({ 
                type: 'GET', 
                url: "{{ url('area/getkecamatan')}}?kota_id={{Auth::user()->kota_id}}", 
                data: { ide: 1 }, 
                dataType: 'json',
                beforeSend: function() {
                    $('#tabelnya').html('<tr><td colspan="3">Loading............</td></tr>')
                    
                },
                success: function (data) {
					$('#tabelnya').html('')
					$.each(data, function(i, result){
						$("#tabelnya").append('<tr><td>'+result.nama+'</td><td>'+result.total_rencana+'</td><td>'+result.total_actual+'</td></tr>');
					});
				}
			});

		}
		function createChart(labels,data,data2){
			

			var barChartData = {
				labels: labels,
				datasets: [{
					label: 'Target',
					borderWidth: 2,
					borderColor: COLOR_INDIGO,
					backgroundColor: COLOR_INDIGO_TRANSPARENT_3,
					data: data
				},{
					label: 'Realisasi',
					borderWidth: 2,
					borderColor: COLOR_INDIGO,
					backgroundColor: COLOR_INDIGO_TRANSPARENT_3,
					data: data2
				}]
			};
			
			var ctx2 = document.getElementById('bar-chart').getContext('2d');
			var barChart = new Chart(ctx2, {
				type: 'bar',
				data: barChartData
			});

		}
		$(document).ready(function() {
			get_dta();
            
		});
	</script>

@endpush